<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package VMA-Main
 */
get_header(); ?>

<!-- HERO SECTION -->
<div id="parallax" class="section section-banner section-parallax" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri(); ?>/library/images/innerpage-bg.jpg" data-bleed="0" data-position="center">
	<div class="container">
		<div class="col-xs-12">
			<div class="banner-block text-center">
				<h1 class="h1 text-bold color-white fade-scroll"><?php single_post_title( '' ); ?></h1>
			</div>
		</div>
	</div>
</div>
<!-- HERO SECTION -->


<!-- CONTENT SECTION -->
<div class="section section-programs bg-grey" >
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8">
				<div class="program-group">
					<div class="content-block">
						<div class="paragraph-block">
							<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
							<?php endwhile; ?>
						</div>					
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4">			
				<?php get_sidebar(); ?>
			</div>

			<div class="clear"></div>
		</div>
	</div>
</div>
<!-- CONTENT SECTION -->

<?php get_footer(); ?>