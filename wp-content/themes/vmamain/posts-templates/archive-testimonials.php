<?php

/**

 * Testimonials archive template

 *

 * Template Name: Testimonials_Archive

 *

 * @package VMA-Main

 */

get_header();

?>



<!-- HERO SECTION -->

<div id="parallax" class="section section-banner section-parallax" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri(); ?>/library/images/hero-thumbsup.jpg" data-bleed="0" data-position="center">

	<div class="container">

		<div class="row">

			<div class="col-xs-12">

				<div class="banner-block text-center">

					<h1 class="h1 text-bold color-white fade-scroll">Testimonials</h1>

				</div>

			</div>

			<div class="clear" ></div>

        </div>

	</div>

</div>

<!-- HERO SECTION -->



<!-- TESTIMONIALS SECTION -->

<div class="section page-section-testimonials bg-grey" >

	<div class="container">

		<div class="row">

			<div class="col-xs-12 col-sm-8">

				<div class="title-block">

					<h2 class="section-title">Testimonials</h2>

				</div>

	<?php

	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	$args = array(

	'post_type' => 'testimonial',

	'post_status' => 'publish',

	'order'    => 'DESC',

	'posts_per_page' => 10,
	
	'paged' => $paged

	);

	$the_query = new WP_query( $args );

	if ($the_query->have_posts()):

	?>

				<div class="testimonials-group">

	<?php while ($the_query->have_posts()) : $the_query->the_post();?>

					<div class="testimonial-block">

						<div class="row">

	<?php if (get_field('image')): $image = get_field('image');  ?>

							<div class="col-xs-3 col-sm-3">						

								<div class="author_icon"><img src="<?php echo $image['sizes']['thumbnail']; ?>" class="img-responsive" alt=""/></div>

							</div>

	<?php endif; ?>

							<div class="<?php echo get_field('image') ? 'col-xs-9 col-sm-9' : 'col-xs-12 col-sm-12'; ?>">

								<div class="author_content">

									<!--<div class="name"><?php the_title(); ?></div>-->

									"<?php echo get_field('testimonial_description'); ?>"

									<div class="author_name" style="padding-top: 8px;"><span class="name text-semibold" style="text-transform: uppercase;"><?php echo get_field('client_name');?></span><br>

<?php echo get_field('designation');?><br>
<span class="company color-orange"><a href="<?php echo get_field('company_website');?>" target="_blank"><?php echo get_field('company_name');?></a></span> </div>

									

								</div>

							</div>

						</div>

					</div>

	<?php endwhile;?>

				</div>

				

				<div class="pagination">

 	<?php 

// 	$big = 999999999; // need an unlikely integer

// 	echo paginate_links(array(

// 	'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),

//     'format' => '?paged=%#%',

// 	'current' => max(1, get_query_var('paged')),

// 	'total' => $the_query->max_num_pages

// 	));

	?>
	<?php 

	$big = 999999999; // need an unlikely integer

	echo paginate_links(array(

	'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),

    'format' => '?paged=%#%',

	'current' => max(1, absint(get_query_var('paged'))),

	'total' => $the_query->max_num_pages

	));

	?>

				</div>

	<?php

	wp_reset_query();

	endif;

	?> 			

			</div>



			<div class="col-xs-12 col-sm-4">

				<?php get_sidebar(); ?>

			</div>

		</div>

	</div>

</div>

<!-- TESTIMONIALS SECTION -->



<?php get_footer(); ?>