<?php get_header();?>

<!-- HERO SECTION -->
<?php $default = get_template_directory_uri().'/library/images/membership-section-bg.jpg';?>
<div class="section section-banner section-parallax" data-parallax="scroll" data-image-src="<?php echo $default;?>" data-bleed="0" data-position="center">
    <div class="container">
		<div class="row">
	        <div class="col-xs-12">        
				<div class="hero-block text-center">
					<h1 class="h1 text-bold color-white fade-scroll">Blog</h1>
				</div>
	        </div>
			<div class="clear" ></div>
		</div>
    </div>
</div>
<!-- HERO SECTION -->

<!-- BLOG DESCRIPTION SECTION -->
<?php 
$output =  apply_filters( 'the_content', $post->post_content );
setup_postdata($post);
 ?>
<div class="section section-blog-details bg-grey" >
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8">
				<div class="content-block">
					<div class="blog-content">
						<h2 class="text-bold"><?php the_title();?></h2>
						<div class="heading-block">
							<div class="post-info">
								<span>Posted by:</span> <span class="color-orange"><?php echo get_field('displayed_author_name') ? get_field('displayed_author_name') : get_the_author();  ?></span> | <?php echo get_the_date('M d, Y', get_the_ID() );?>
							</div>
						</div>
					</div>
					<div class="paragraph-block">
						<p><?php if(get_field('description')) { echo get_field('description'); } else { echo $output;}?><p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="nav-block">
							<div class="col-xs-6 col-sm-4 text-left no-padding">
								<?php previous_post_link( '%link', '<i class="icon icon-angle-left"></i> Previous Post' ); ?>
							</div>
							<div class="col-xs-4 col-sm-4 text-center no-padding hidden-xs ">
								<a class="button button-normal color-orange text-semibold capitalize animated-normal" href="<?php echo get_site_url(); ?>/blog-articles/">All Posts</a>	
							</div>
							<div class="col-xs-6 col-sm-4 text-right no-padding">
								<?php next_post_link( '%link', 'Next Post <i class="icon icon-angle-right"></i>' ); ?>
							</div>
							<div class="col-xs-12 text-center no-padding visible-xs">
								<a class="button button-normal color-orange text-semibold capitalize animated-normal" href="<?php echo get_site_url(); ?>/blog-articles/">All Posts</a>	
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</div>
<!-- BLOG DESCRIPTION SECTION -->
<?php get_footer(); ?>