<?php
get_header();
?>
<!-- HERO SECTION -->
<div id="parallax" class="section section-banner section-parallax" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri(); ?>/library/images/photoshop-hero-bg.jpg" data-bleed="0" data-position="center" data-natural-width="1315" data-natural-height="450">
	<div class="container banner-container">
		<div class="col-xs-12">
			<div class="banner-block">
				<div class="col-xs-12 text-center">
					<h1 class="page-title text-bold color-white fade-scroll">Photoshop</h1>
				</div>
				<div class="clear" ></div>
			</div>
		</div>
	</div>
</div>
<!-- HERO SECTION -->

<!-- SEARCH DETAILS -->
<div class="section section-course-details" >
	<div class="container course-details-container">
		<div class="col-xs-12 col-sm-7">
			<div class="title-block">
				<h2 class="section-title">Photoshop Advanced: Practical Application Techniques</h2>
			</div>
			<div class="paragraph-block">
				<p>In this two-day project-driven training class, you will learn from an Adobe Certified Instructor a wide range of advanced Photoshop techniques as you work through challenging real-world projects, including how to use vector tools to create masks, shapes and selections, advanced selection techniques, sharpening and compositing images, automating actions, the Brush tool and vanishing point filter and layer comps.<br /><br />
In this two-day project-driven training class, you will learn from an Adobe Certified Instructor a wide range of advanced Photoshop techniques as you work through challenging real-world projects, including how to use vector tools to create masks, shapes and selections, advanced selection techniques, sharpening and compositing images, automating actions, the Brush tool and vanishing point filter and layer comps. In this two-day project-driven training class, you will learn from an Adobe Certified Instructor a wide range of advanced Photoshop techniques as you work through challenging real-world projects, including how to use vector tools to create masks, shapes and selections, advanced selection techniques, sharpening and compositing images, automating actions, the Brush tool and vanishing point filter and layer comps.</p>
			</div>
		</div>
		<div class="col-xs-12 col-sm-5">
			<div class="sidebar-right">
				<div class="class-info-block">
					<div class="form-header">
						<h3 class="text-bold">Class Details</h3>
					</div>
					<div class="form-body">
						<form action="<?php echo get_site_url(); ?>/registration">
							<div class="form-row row">
								<div class="col-xs-12">
									<select id="cd-dropdown" name="cd-dropdown" class="cd-select">										
										<option value="-1" selected>Choose Date</option>
										<option value="Feb 15, 2016">Feb 15, 2016</option>
										<option value="Feb 16, 2016">Feb 16, 2016</option>
										<option value="Feb 17, 2016">Feb 17, 2016</option>
										<option value="Feb 18, 2016">Feb 18, 2016</option>
										<option value="Feb 19, 2016">Feb 19, 2016</option>
										<option value="Feb 22, 2016">Feb 22, 2016</option>
									</select>
								</div>
							</div>
							<div class="form-row row">
								<div class="col-xs-5 col-sm-4">
									<label>Duration:</label>
								</div>
								<div class="col-xs-7 col-sm-8">
									<span>2 day(s)</span>
								</div>
							</div>
							<div class="form-row row">
								<div class="col-xs-5 col-sm-4">
									<label>Time:</label>
								</div>
								<div class="col-xs-7 col-sm-8">
									<span>9 am - 4 pm</span>
								</div>
							</div>
							<div class="form-row row">
								<div class="col-xs-5 col-sm-4">
									<label>Location:</label>
								</div>
								<div class="col-xs-7 col-sm-8">
									<span>Online</span>
								</div>
							</div>
							<div class="form-row row">
								<div class="col-xs-5 col-sm-4">
									<label>Price:</label>
								</div>
								<div class="col-xs-7 col-sm-8">
									<div>Members $591</div>
									<div>Non-Member $695</div>
								</div>
							</div>
							<div class="form-row button-row row">
								<div class="col-xs-12">
									<button type="submit" class="button bg-red button-primary button-full">Register <i class="icon icon-angle-right"></i></button>
								</div>
							</div>
							<div class="clear"></div>
						</form>
					</div>
				</div>
				
				<div class="have-a-question">
					<div class="form-header">
						<h3 class="text-bold">Have a Question?</h3>
					</div>
					<div class="form-body"><p>Contact VMA at <a href="tel:800.659.3363">800.659.3363</a> or <a href="mailto:info@vma.bz">info@vma.bz</a></p></div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- SEARCH DETAILS -->

<?php get_footer(); ?> 
