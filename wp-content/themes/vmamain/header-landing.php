<?php
/**
 *
 * @package VMA-Insurance
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
<meta name="msapplication-square144x144logo" content="<?php echo get_template_directory_uri(); ?>/win8-tile-icon.png" />
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-icon-touch.png"/>
<link rel="icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
<title><?php
			if( is_home() ) {
				echo get_bloginfo( 'name' ) . ' | ' . get_bloginfo( 'description' );
			} else {
				if( wp_title( '', false ) === '' ) {
					echo get_bloginfo( 'name' );
				} else {
					echo wp_title( '', false ) . ' | ' .  get_bloginfo( 'name' );
				}
			}
		?></title>
<?php wp_head(); ?>
<style>
body.page-template {background: #f2f2f2; color: #333433;}
body.page-template h1,
body.page-template h2,
body.page-template h3{font-weight: bold;}
body.page-template p{color: #333433;}
body.page-template h2{color: #555555;}
</style>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MFJ4NLC');</script>
<!-- End Google Tag Manager -->
</head>
<body <?php body_class( grunt_wordpress_page_name() ); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MFJ4NLC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--[if lt IE 9]><p class="browse-happy">You are using an <strong>outdated</strong> browser.<br/>Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a>.</p><div class="lt-ie9"><![endif]-->
<div class="xanimsition">

<!-- Header -->
<header id="insurance-header" class="landing-header">

	<!-- Logo & Nav Banner -->
	<div class="logonav">
		<div class="container relative">
			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<a href="<?php echo get_site_url(); ?>/" class="logo-link">
						<div class="logo"></div>
					</a>
					<div class="clear"></div>
				</div>
				<div class="col-xs-12 col-sm-8" >
					<div class="cta-block landing-cta">
						<span class="call-agent">Offering coverage for 35 years</span>
						<div class="quote-text">Get a Free Quote Today! <a href="tel:800.659.3363">800-659-3363</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Logo & Nav Banner -->
</header>
<div class="display-none"><header id="header" class="hide-header cloned"></header></div>
<!-- Header -->
<!-- Main Container -->
<div class="main-container insurance-container no-padding">