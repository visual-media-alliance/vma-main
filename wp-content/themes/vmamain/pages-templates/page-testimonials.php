<?php

/**
 * Testimonials page template
 *
 * Template Name: Testimonials
 * @package VMA-Main
 */
require get_stylesheet_directory() . '/posts-templates/archive-testimonials.php';