<?php
/**
 * Membershipland page template
 *
 * Template Name: Membershipland
 *
 * @package VMA
 */
get_header('chronicles');
?>
<!-- FB SPLASH SECTION -->
<div class="section section-splash fill fill-top" style="background-image: url(<?php echo get_template_directory_uri(); ?>/library/images/main-bg.jpg)">
    <div class="container">
	    <div class="row">
	        <div class="col-xs-12">
	            <div class="title-block text-center">
	            	<h1 class="h1 text-bold uppercase">VMA Membership</h1>
	            	<p class="text-semibold">Visual Media Alliance is made up of Printers, Graphic Artist, Web Designers and Creative minded folks just like you.  We are dedicated to support you and your business so you can keep creating.</p>
	            </div>
				
				<div class="splash-form-block">
					<div class="row">					
						<div class="message-block text-center col-xs-12">
							<h2 class="h2 color-primary text-regular">Please give us your contact info and we'll call you quickly!</h2>
						</div>
						<?php echo do_shortcode('[contact-form-7 id="4673" title="Membershipland Form"]'); ?>
						<div class="link-block col-xs-12 text-center">
							<a href="#">Terms</a> | <a href="#">Privacy Policy</a>
						</div>
					</div>
				</div>		
	        </div>
	        <div class="col-xs-12">
	        	<div class="footer text-center">
	        		<div class="footer-logo"><a href="http://main.vma.bz/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/vma-insurance-logo.png" class="img-responsive"></a></div>
	        		<div class="address">
	        			<p>665 Third Street, Suite 500, San Francisco, CA 94107</p>
	        			<p>Phone <a href="tel:8006593363">800.659.3363</a> | Fax 800.824.1911 | <br class="visible-xs"><a href="mailto:shannon@vma.bz">info@vma.bz</a></p>
	        		</div>
	        	</div>
	        </div>
	    </div>
    </div>
</div>
<!-- FB SPLASH SECTION -->
<?php get_footer('landingpage'); ?>