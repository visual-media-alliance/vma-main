<?php
/**
 * Membership page template
 *
 * Template Name: Membership
 *
 * @package VMA-Main
 */
get_header();?>
<!-- HERO SECTION -->
<?php
if (have_rows('hero_section')):
while (have_rows('hero_section')): the_row();
$banner_image = get_sub_field('background_image');
?>
<div id="parallax" class="section section-banner section-parallax" data-parallax="scroll" data-image-src="<?php echo $banner_image; ?>" data-bleed="0" data-position="center">
	<div class="container">
		<div class="col-xs-12">
			<div class="banner-block text-center">
				<h1 class="h1 text-bold color-white fade-scroll"><?php echo get_sub_field('title'); ?></h1>
			</div>
		</div>
	</div>
</div>
<?php endwhile; endif;?>
<!-- HERO SECTION -->
<!-- CONTENT SECTION -->
<div class="section section-programs bg-grey" >
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8">	
				<div class="program-group">
					<?php
					if (have_rows('intro_section')):
					while (have_rows('intro_section')): the_row();
					?>
					<div class="intro-block">
						<h2 class="h2 text-light color-orange"><?php echo get_sub_field('heading_text'); ?></h2>
						<div class="paragraph-block">
							<?php echo get_sub_field('description_text'); ?>
						</div>
					</div>
					<?php endwhile; endif;?>
					<?php
					if (have_rows('content_section')):
					while (have_rows('content_section')): the_row();
					?>
					<div class="content-block">
						<?php
						if (have_rows('content_group')):
						while (have_rows('content_group')): the_row();
						?>
						<h3 class="h3 text-light border-title color-orange"><?php echo get_sub_field('title'); ?></h3>
						<?php
						if (have_rows('paragraph_block')):
						while (have_rows('paragraph_block')): the_row();
						?>
						<div class="paragraph-block">
							<p><b><?php echo get_sub_field('heading'); ?></b><br>
							<?php echo get_sub_field('paragraph_text'); ?>
						</div>	
						<?php endwhile; endif;?>
						<!-- <div class="paragraph-block">							
							<p><b>Lunch & Learn</b><br>
							Listen. You gotta eat. So if you’re gonna to take a break from work to have lunch, why not add a side of learning while you’re at it? At Lunch & Learns, our members do just that. Gain insightful information from special guest speakers presenting on the hottest topics, then network with peers and potential clients. For VMA members, these events are complimentary. (Whoever said there’s no such thing as a free lunch?)</p>
						</div>
						<div class="paragraph-block">							
							<p><b>Constructive Cocktails</b><br>
							When your work day ends, the schmoozing begins. And what better way to meet prospective clients, make new friends, and learn something new than over cocktails? These popular events are free to VMA members and are held during the evenings at convenient locations. Cheers to networking!</p>
						</div>
						<div class="paragraph-block">							
							<p><b>Dinner Meetings</b><br>
							Take your career to the next level... over dinner. Held several times throughout the year at some of the Bay Area's most popular restaurants, VMA Dinner Meetings include cocktails, a main course, dessert - and, oh yeah - inspiring guest speakers. Topics range from upping your sales game to the latest and greatest in digital print and new media marketing, and much more.</p>
						</div> -->
						<?php endwhile; endif;?>
					</div>
					<?php endwhile; endif;?>
					<!-- <div class="content-block">
						<h3 class="h2 text-light border-title color-orange">Industry Events</h3>
						<div class="paragraph-block">
							<p><b>Java & Jabber</b><br>
							An always-popular event with our members, these monthly round-table breakfast events feature industry experts who share deep insights into a variety of relevant topics. Bring your business cards and get ready to network. We’ll take care of the rest.</p>
						</div>
						<div class="paragraph-block">							
							<p><b>Lunch & Learn</b><br>
							Listen. You gotta eat. So if you’re gonna to take a break from work to have lunch, why not add a side of learning while you’re at it? At Lunch & Learns, our members do just that. Gain insightful information from special guest speakers presenting on the hottest topics, then network with peers and potential clients. For VMA members, these events are complimentary. (Whoever said there’s no such thing as a free lunch?)</p>
						</div>
						<div class="paragraph-block">							
							<p><b>Constructive Cocktails</b><br>
							When your work day ends, the schmoozing begins. And what better way to meet prospective clients, make new friends, and learn something new than over cocktails? These popular events are free to VMA members and are held during the evenings at convenient locations. Cheers to networking!</p>
						</div>
						<div class="paragraph-block">							
							<p><b>Dinner Meetings</b><br>
							Take your career to the next level... over dinner. Held several times throughout the year at some of the Bay Area's most popular restaurants, VMA Dinner Meetings include cocktails, a main course, dessert - and, oh yeah - inspiring guest speakers. Topics range from upping your sales game to the latest and greatest in digital print and new media marketing, and much more.</p>
						</div>
					
					</div> -->
				</div>
			</div>
			<div class="col-xs-12 col-sm-4">			
				<?php get_sidebar(); ?>
			</div>
			<div class="clear"></div>
			
		</div>
	</div>
</div>
<!-- CONTENT SECTION -->
<?php get_footer(); ?>