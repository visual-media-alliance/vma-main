<?php
/**
 * Networking page template
 *
 * Template Name: Networking
 *
 * @package VMA-Main
 */
get_header();
?>
<!-- HERO SECTION -->
<?php
if (have_rows('hero_section')):
while (have_rows('hero_section')): the_row();
$banner_image = get_sub_field('background_image');
?>
<div id="parallax" class="section section-banner section-parallax" data-parallax="scroll" data-image-src="<?php echo $banner_image; ?>" data-bleed="0" data-position="center">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="banner-block text-center">
					<h1 class="h1 text-bold color-white fade-scroll"><?php echo get_sub_field('title'); ?></h1>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endwhile; endif;?>
<!-- HERO SECTION -->
<!-- CONTENT SECTION -->
<div class="section section-programs bg-grey" >
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8">	
				<div class="program-group">
<?php
if (have_rows('intro_section')):
while (have_rows('intro_section')): the_row();
?>
<?php if(get_sub_field('description_text')) { ?>
					<div class="intro-block">
						<?php if(get_sub_field('heading_text')) { ?><h2 class="h2 text-light color-orange"><?php echo get_sub_field('heading_text'); ?></h2><?php }?>
						<div class="paragraph-block">
							<?php echo get_sub_field('description_text'); ?>
						</div>
					</div>
<?php }?>
					<?php endwhile; endif;?>
<?php
if (have_rows('content_section')):
while (have_rows('content_section')): the_row();
?>
<?php
if (have_rows('content_group')):
while (have_rows('content_group')): the_row();
?>
					<div class="content-block">
						<?php if(get_sub_field('title')) { ?><h3 class="h3 text-light border-title color-orange"><?php echo get_sub_field('title'); ?></h3><?php }?>
<?php
if (have_rows('paragraph_block')):
while (have_rows('paragraph_block')): the_row();
?>
						<div class="paragraph-block">
							<?php if(get_sub_field('heading')) { ?><p><b><?php echo get_sub_field('heading'); ?></b></p><?php }?>
							<?php echo get_sub_field('paragraph_text'); ?>
						</div>	
<?php endwhile; endif;?>
					</div>
<?php endwhile; endif;?>
<?php endwhile; endif;?>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4">			
				<?php get_sidebar(); ?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
<!-- CONTENT SECTION -->
<?php get_footer(); ?>