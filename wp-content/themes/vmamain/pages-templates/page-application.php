<?php
/**
 * Join page template
 *
 * Template Name: Application
 *
 * @package VMA-Main
 */
get_header(); ?>

<!-- HERO SECTION -->
<?php

?>
<div class="section section-hero section-join section-parallax" data-image-src="/wp-content/themes/vmamain/library/images/membership-section-bg.jpg" data-parallax="scroll" data-bleed="0" data-position="center" draggable="disable">
    <div class="container">
        <div class="col-xs-12 no-padding">            
			<div class="join-block">
				<h1 class="h1 text-bold"><div>Join the Alliance.</div> Together. We're Better.</h1>
				<h4>Discover the benefits of becoming a VMA Member!</h4>
				<div class="button-block"><a class="button button-normal button-orange uppercase animated-normal" href="#signup">BECOME A MEMBER<i class="icon icon-angle-right text-bold"></i></a></div>
			</div>
        </div>
		<div class="clear"></div>
    </div>
</div>
<!-- HERO SECTION -->


<!-- CONTENT SECTION -->
<div class="section section-contact bg-grey" >
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8">
				<div class="program-group">
					<div class="content-block">
						<div class="paragraph-block">
							<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
							<h4 id="signup" class="text-light color-orange" style="border-bottom: 1px solid #dfdfdf; padding-bottom: 20px; margin-bottom: 15px;">What are you waiting for? Become a member today!</h4>
<?php echo do_shortcode('[contact-form-7 id="6607" title="Join VMA"]'); ?>
							<?php endwhile; ?>
						</div>					
					</div>
				</div>
			</div>
		

			<div class="col-xs-12 col-sm-4">			
				<?php get_sidebar(); ?>
			</div>

			<div class="clear"></div>
		</div>
	</div>
</div>
<!-- CONTENT SECTION -->
<div class="section section-contact bg-grey">
	<div class="container">
		<div class="row">
		
	</div>
</div>

<?php get_footer(); ?>