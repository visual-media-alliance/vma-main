<?php
/**
 * Chronicles Blog page template
 *
 * Template Name: Chronicles Blog
 *
 * @package VMA
 */
get_header('chronicles');
?>
<!-- HERO SECTION -->
<div class="section section-cbanner fill fill-top" style="background-image: url(<?php echo get_template_directory_uri(); ?>/library/images/blog-header.jpg)">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="logo-block fade-scroll">
					<a href="http://main.vma.bz" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/vma-blog-logo.png" class="img-responsive"></a>
				</div>
				<div class="title-block text-center fade-scroll">
					<h1 class="h1 text-semibold color-white">Cross-Media Chronicles</h1>
					<div class="dotted-line color-white">..................</div>
					<p class="tagline color-white" >Support for Creative, Web Media, Marketing and Print Businesses</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- HERO SECTION -->

<!-- BLOG ARCHIVE SECTION -->
<?php $args = 'category_name=cross-media-chronicles&orderby=date&order=DESC&posts_per_page=-1';
$postslist = get_posts( $args );
?>
<div class="section section-cblog-list">
	<div class="container blog-container" style="padding-left: 15px; padding-right: 15px;">
		
		<div class="row">
			<div class="col-xs-12 col-sm-8">
				<!-- <div class="blog-group">
	<?php foreach($postslist as $post): setup_postdata($post); ?>
						<div class="blog-block" id="content_<?php the_ID();?>">
							<div class="blog-content">
								<h3 class="h3 text-bold"><?php the_title();?></h3>
								<?php if(has_post_thumbnail()):
									the_post_thumbnail(array(640,420), array('class' => 'img-responsive pull-left'));
								endif;?>
								<p><?php the_content();?></p>
							</div>
						</div>
	<?php endforeach;?>
				</div> -->
				<!-- CHRONICLES BLOG SECTION -->
				<div class="blog-group">
					<?php 
					$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
					$args = array(
						'post_type' 		=> 'post',
						'category_name' 	=> 'cross-media-chronicles',
						'posts_per_page' 	=> 7,
						'order'   			=> 'DESC',
						'paged'				=> $paged
					);
					$the_query = new WP_Query($args);
					if ($the_query->have_posts()) :
						while ($the_query->have_posts()): $the_query->the_post();
							$featured_image = get_field('main_image',get_the_ID());
					?>
						<div class="blog-block" id="content_<?php the_ID();?>">
							<div class="blog-content">
								<h3 class="h3 text-bold"><?php the_title();?></h3>
								<img src="<?php echo $featured_image; ?>" class="img-responsive" alt=""/>
								<p><?php $short_details = get_the_content();
										$str = strip_tags($short_details);
										echo substr($str, 0, 172);?>...</p>
								<a href="<?php the_permalink(); ?>" class="button button-normal color-orange text-semibold capitalize animated-normal" >Read More <i class="icon icon-angle-right"></i></a>
							</div>
						</div>
					<?php endwhile;?>
							<div class="pagination">
					        <?php
		                    $big = 999999999; // need an unlikely integer
		                    echo paginate_links( array(
			                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			                        'format' => '?paged=%#%',
			                        'current' => max( 1, get_query_var('paged') ),
			                        'total' => $the_query->max_num_pages,                        
								    'prev_text'    => '«',
								    'next_text'    => '»'
				                    ) );
				            ?>
			                </div>
					<?php wp_reset_postdata();?>
					<?php else: ?>
						No blog found.
					<?php endif; ?>				
				</div>
			</div>
<!-- CHRONICLES BLOG SECTION -->
			<div class="col-xs-12 col-sm-4">
				<div class="sidebar-right">
					<div class="blog-posts-group">
						<div class="link-header">
							<h3 class="text-bold">Articles</h3>
						</div>
						<div class="blog-links">
							<ul>
	<?php foreach($postslist as $post): setup_postdata($post); ?>
								<li><a href="<?php the_permalink(); ?>" target="_blank" class="link text-semibold"><?php the_title();?></a></li>
	<?php endforeach;?>
							</ul>
						</div>
					</div>
					<div class="benefit-block text-center">
						<p class="text-bold">Would you like to know the benefits of becoming a VMA Member?</p>
						<a href="http://submain.vma.bz/Default.aspx?pageid=221" target="_blank" class="button button-primary button-orange link capitalize text-semibold">Learn More</a>
					</div>
					<div class="benefit-block text-center">
						<p class="text-bold">Are you interested in receiving a free business insurance quote?</p>
						<a href="http://main.vma.bz/insuranceland" target="_blank" class="button   button-blue link capitalize text-semibold">Get A Quote</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- BLOG ARCHIVE SECTION -->
<a href="#0" class="cd-top">Top</a>
<!-- HERO SECTION -->
<div class="section section-cfooter">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="title-block text-center">
					<h1 class="h2 text-semibold color-orange">Cross-Media Chronicles</h1>
					<p class="tagline" >Insightful Design, Printing, Marketing and Communications Resources Delivered Monthly</p>
				</div>
				<div class="logo-block">
					<a href="http://main.vma.bz" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/vma-blog-logo-footer.png" class="img-responsive"></a>					
				</div>
			</div>
		</div>
	</div>
</div>
<!-- HERO SECTION -->
<style>
.cd-top {
  display: inline-block;
  height: 40px;
  width: 40px;
  position: fixed;
  bottom: 20px;
  right: 20px;
  z-index: 10;
  border-radius: 50%;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.05);
  /* image replacement properties */
  overflow: hidden;
  text-indent: 100%;
  white-space: nowrap;
  background: rgba(243, 127, 51, 0.8) url(<?php echo get_template_directory_uri(); ?>/library/images/cd-top-arrow.svg) no-repeat center 50%;
  visibility: hidden;
  opacity: 0;
  -webkit-transition: opacity .3s 0s, visibility 0s .3s;
  -moz-transition: opacity .3s 0s, visibility 0s .3s;
  transition: opacity .3s 0s, visibility 0s .3s;
}
.cd-top.cd-is-visible, .cd-top.cd-fade-out, .no-touch .cd-top:hover {
  -webkit-transition: opacity .3s 0s, visibility 0s 0s;
  -moz-transition: opacity .3s 0s, visibility 0s 0s;
  transition: opacity .3s 0s, visibility 0s 0s;
}
.cd-top.cd-is-visible {
  /* the button becomes visible */
  visibility: visible;
  opacity: 1;
}
.cd-top.cd-fade-out {
  /* if the user keeps scrolling down, the button is out of focus and becomes less visible */
  opacity: .5;
}
.cd-top.cd-fade-out:hover {
  opacity: 1;
}
.no-touch .cd-top:hover {
  background-color: #e86256;
  opacity: 1;
}
@media only screen and (min-width: 768px) {
  .cd-top {
    right: 20px;
    bottom: 20px;
  }
}
@media only screen and (min-width: 1024px) {
  .cd-top {
    height: 60px;
    width: 60px;
    right: 30px;
    bottom: 30px;
  }
}
</style>

<?php get_footer('landingpage'); ?>
<script>
jQuery(document).ready(function($){
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');
	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});
	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});
	function scrollToAnchor(aid){
		var aTag = $(aid);
		$('html,body').animate({scrollTop: aTag.offset().top},'slow');
	}
	jQuery(".link").click(function(e) {
		var id=jQuery(this).attr('href');
		scrollToAnchor(id);
		e.preventDefault();
	});
});
</script>