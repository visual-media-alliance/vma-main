<?php
/**
 * Contact page template
 *
 * Template Name: Contact
 *
 * @package VMA-Main
 */
get_header();
?>
<!-- HERO SECTION -->
<?php
if (have_rows('hero_section')):
while (have_rows('hero_section')): the_row();
$banner_image = get_sub_field('background_image');
?>
<div class="section section-banner section-parallax" data-parallax="scroll" data-image-src="<?php echo $banner_image; ?>" data-bleed="0" data-position="center">
	<div class="container">
		<div class="col-xs-12">
			<div class="banner-block">
				<div class="col-xs-12 text-center">
					<h1 class="h1 text-bold color-white fade-scroll"><?php echo get_sub_field('title'); ?></h1>
				</div>
				<div class="clear" ></div>
			</div>
		</div>
	</div>
</div>
<?php endwhile; endif;?>
<!-- HERO SECTION -->
<!-- Intro SECTION -->
<?php
if (have_rows('description_section')):
while (have_rows('description_section')): the_row();
?>
<div class="section section-intro section-contact-intro">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1">
				<div class="content-block text-center">
					<h2 class="h4 text-bold color-orange"><?php echo get_sub_field('title'); ?></h2>
					<p><?php echo get_sub_field('text'); ?></p>
				</div>
			</div>
		</div>				
	</div>
</div>
<?php endwhile; endif;?>
<!-- Intro SECTION -->
<!-- Intro SECTION -->
<div class="section section-contact bg-grey">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8">
				<?php //echo do_shortcode('[contact-form-7 id="4948" title="Contact Form"]');?>
				<?php echo do_shortcode('[contact-form-7 id="5311" title="Primary Contact Form" ajax]');?>
			</div>
			<div class="col-xs-12 col-sm-4">
				<div class="contact-details-block">
					<div class="paragraph-block title-block">
						<h2 class="h4 text-bold">&nbsp;</h2>
					</div>
<?php
if (have_rows('contact_details')):
while (have_rows('contact_details')): the_row();
?>
					<div class="paragraph-block">
						<h4 class="h6 color-orange text-bold"><?php echo get_sub_field('title'); ?></h4>
						<p><?php echo get_sub_field('text'); ?></p>
					</div>					
<?php endwhile; endif;?>
				</div>
			</div>
		</div>				
	</div>
</div>
<!-- Intro SECTION -->
<!-- BECOME MEMBER SECTION -->
<?php
if (have_rows('membership_section')):
while (have_rows('membership_section')): the_row();
$background_image = get_sub_field('background_image');
?>
<div class="section section-membership relative section-parallax" data-parallax="scroll" data-image-src="<?php echo $background_image; ?>" data-bleed="0" data-position="center">	
    <div class="container">
    	<div class="row">
			<div class="col-xs-12">
			    <div class="membership-block text-center">
		            <h2 class="text-bold uppercase color-white"><?php echo get_sub_field('title'); ?></h2>
					<p class="color-white"><?php echo get_sub_field('text'); ?></p>
					<a href="<?php echo get_sub_field('button_url'); ?>" class="button button-primary button-orange" ><?php echo get_sub_field('button_name'); ?> <i class="icon icon-angle-right"></i></a> 			        
			    </div>
			</div>
		</div>
    </div>
</div>
<?php endwhile; endif;?>
<!-- BECOME MEMBER SECTION -->
<?php get_footer(); ?>