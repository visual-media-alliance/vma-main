﻿<?php
/**
 * Scholarship page template
 *
 * Template Name: Scholarship
 *
 * @package VMA-Main
 */
get_header();
?>

<!-- HERO SECTION -->
<div id="parallax" class="section section-banner section-parallax" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri(); ?>/library/images/scholarship-bg.jpg" data-bleed="0" data-position="center">
	<div class="container">
		<div class="col-xs-12">
			<div class="banner-block text-center">
				<h1 class="h1 text-bold color-white fade-scroll">Scholarship</h1>
			</div>
		</div>
	</div>
</div>
<!-- HERO SECTION -->

<!-- CONTENT SECTION -->
<div class="section section-programs bg-grey" >
	<div class="container">

		<div class="row">
			<div class="col-xs-12 col-sm-8">

				<div class="program-group">
					<div class="intro-block">
						<h2 class="h2 text-light color-orange">Scholarships to Help You Succeed</h2>
						<div class="paragraph-block">
							<p>Visual Media Alliance offers scholarships for education in graphic arts for the purpose of providing funding to attract and train new talent to our industry as well as improving the skills of our existing workforce. The general fund is supported by the Guy and Louise Condrott Scholarship Fund, Inc. and the VMA Scholarship Fund through various fundraising events throughout the year.<br><br></p>

<h3>Target</h3>
<p>For students attending graphic communications programs in community college or other vocational schools. Priority is given to those pursuing careers in prepress, production and management in the printing industry.<br><br></p>

<h3>Requirements</h3>
<p>1. Submit scholarship application by downloading and filling it out <a href="http://submain.vma.bz/scholarship/vma_scholarship_application.pdf" target="_blank">here</a>.<br>

2. Application must be accompanied with proof of student status.<br>

3. Submit letter of recommendation from your instructor on school letterhead.<br><br></p>

<h3>Award</h3>
<p>Up to 10 scholarships of $1,000 will be awarded per calendar year.<br><br></p>

<h3>Application Guidelines</h3>
<p>To be eligible for a scholarship for a given training period, applications and required letters must be turned in by these due dates:

<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Fall Semester: July 1
<br>
&nbsp;&nbsp;&nbsp;&nbsp;Spring Semester: December 1
<br>
&nbsp;&nbsp;&nbsp;&nbsp;Summer Session: April 1<br><br>

<strong>MAIL TO</strong><br>
VMA Educational Scholarship Fund<br>
665 3rd Street, Suite 500<br>
San Francisco, CA 94107<br><br>

<strong>FAX TO</strong>
(800) 824-1911<br></p>
						</div>
					</div>

				

				</div>
			</div>

			<div class="col-xs-12 col-sm-4">			
				<?php get_sidebar(); ?>
			</div>

			<div class="clear"></div>
			
		</div>
	</div>
</div>
<!-- CONTENT SECTION -->

<?php get_footer(); ?>