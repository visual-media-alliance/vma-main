<?php
/**
 * Business Services page template
 *
 * Template Name: Business 
 *
 * @package VMA-Main
 */
get_header();?>

<!-- HERO SECTION -->
<div id="parallax" class="section section-banner section-parallax" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri(); ?>/library/images/networking-bg.jpg" data-bleed="0" data-position="center">
	<div class="container">
		<div class="col-xs-12">
			<div class="banner-block text-center">
				<h1 class="h1 text-bold color-white fade-scroll">Business Services</h1>
			</div>
		</div>
	</div>
</div>
<!-- HERO SECTION -->

<!-- CONTENT SECTION -->
<div class="section section-programs bg-grey" >
	<div class="container">

		<div class="row">
			<div class="col-xs-12 col-sm-8">

				<div class="program-group">
					<div class="intro-block">
						<h2 class="h2 text-light color-orange">Networking is the key to success</h2>
						<div class="paragraph-block">
							<p>Business shouldn't happen in a vacuum. What's more, when it comes to doing creative stuff, you’ve got to be out there. You know, mixing it up. Meeting, greeting, and diving into experiences with others just like you who find inspiration in the everyday.</p>

							<p>Lucky for you, we here at VMA are kind of obsessed with organizing events that give you all kinds of opportunities to do just that.</p>

							<p>Whether social, professional, or educational, when you attend a VMA event, you'll meet cool, like-minded members, make connections with awesome professionals in your field, and come away with the skills, tools, and leading-edge best practices, tips, and techniques you need to get more out of your career.</p>
						</div>
					</div>

					<div class="content-block">
						<h3 class="h3 text-light border-title color-orange">Industry Events</h3>
						<div class="paragraph-block">
							<p><b>Java & Jabber</b><br>
							An always-popular event with our members, these monthly round-table breakfast events feature industry experts who share deep insights into a variety of relevant topics. Bring your business cards and get ready to network. We’ll take care of the rest.</p>
						</div>
						<div class="paragraph-block">							
							<p><b>Lunch & Learn</b><br>
							Listen. You gotta eat. So if you’re gonna to take a break from work to have lunch, why not add a side of learning while you’re at it? At Lunch & Learns, our members do just that. Gain insightful information from special guest speakers presenting on the hottest topics, then network with peers and potential clients. For VMA members, these events are complimentary. (Whoever said there’s no such thing as a free lunch?)</p>
						</div>
						<div class="paragraph-block">							
							<p><b>Constructive Cocktails</b><br>
							When your work day ends, the schmoozing begins. And what better way to meet prospective clients, make new friends, and learn something new than over cocktails? These popular events are free to VMA members and are held during the evenings at convenient locations. Cheers to networking!</p>
						</div>
						<div class="paragraph-block">							
							<p><b>Dinner Meetings</b><br>
							Take your career to the next level... over dinner. Held several times throughout the year at some of the Bay Area's most popular restaurants, VMA Dinner Meetings include cocktails, a main course, dessert - and, oh yeah - inspiring guest speakers. Topics range from upping your sales game to the latest and greatest in digital print and new media marketing, and much more.</p>
						</div>
					
					</div>

					<div class="content-block">
						<h3 class="h2 text-light border-title color-orange">Industry Events</h3>
						<div class="paragraph-block">
							<p><b>Java & Jabber</b><br>
							An always-popular event with our members, these monthly round-table breakfast events feature industry experts who share deep insights into a variety of relevant topics. Bring your business cards and get ready to network. We’ll take care of the rest.</p>
						</div>
						<div class="paragraph-block">							
							<p><b>Lunch & Learn</b><br>
							Listen. You gotta eat. So if you’re gonna to take a break from work to have lunch, why not add a side of learning while you’re at it? At Lunch & Learns, our members do just that. Gain insightful information from special guest speakers presenting on the hottest topics, then network with peers and potential clients. For VMA members, these events are complimentary. (Whoever said there’s no such thing as a free lunch?)</p>
						</div>
						<div class="paragraph-block">							
							<p><b>Constructive Cocktails</b><br>
							When your work day ends, the schmoozing begins. And what better way to meet prospective clients, make new friends, and learn something new than over cocktails? These popular events are free to VMA members and are held during the evenings at convenient locations. Cheers to networking!</p>
						</div>
						<div class="paragraph-block">							
							<p><b>Dinner Meetings</b><br>
							Take your career to the next level... over dinner. Held several times throughout the year at some of the Bay Area's most popular restaurants, VMA Dinner Meetings include cocktails, a main course, dessert - and, oh yeah - inspiring guest speakers. Topics range from upping your sales game to the latest and greatest in digital print and new media marketing, and much more.</p>
						</div>
					
					</div>


				</div>
			</div>

			<div class="col-xs-12 col-sm-4">			
				<?php get_sidebar(); ?>
			</div>

			<div class="clear"></div>
			
		</div>
	</div>
</div>
<!-- CONTENT SECTION -->

<?php get_footer(); ?>