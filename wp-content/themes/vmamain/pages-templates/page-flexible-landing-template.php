<?php
/**
 * Flexible Landing page template
 *
 * Template Name: Flexible Landing Page
 *
 * @package VMA-Insurance
 */
get_header('landing');
?>

<?php if(get_field('layout_status') == 'activate'){ ?>
<!-- Section Landing Content -->
<div class="section section-landing-content">
    <div class="container">

        <div class="column-container">
<?php if(get_field('primary_content')):?>
            <div class="main-column">
                <div class="content-block">
                    <?php the_field('primary_content');?>
                </div>
            </div>
<?php endif;?>

<?php  if(have_rows('formscontact_box')): while(have_rows('formscontact_box')): the_row();?>
            <div class="rightbar">
                <div class="landing-form-block">
<?php if(get_sub_field('title')):?>
                    <div class="form-header"><?php the_sub_field('title');?></div>
<?php endif;?>

<?php if(get_sub_field('content_type') == 'form'):?>
<?php if(get_sub_field('contact_form')):?>
                    <div class="form-content">                        
                        <?php the_sub_field('contact_form');?>
                    </div>
<?php endif;?>
<?php endif;?>

<?php if(get_sub_field('content_type') == 'contact_box'):?>
<?php  if(have_rows('contact_box')): while(have_rows('contact_box')): the_row();?>
                    <div class="form-content">
                        <div class="contact-info-block">
                            <div class="info-block">
                                <h3><?php the_sub_field('contact_text');?></h3>
                                <p><?php the_sub_field('name');?><br>
                                    <?php the_sub_field('phone');?><br>
                                    <a href="mailto:<?php the_sub_field('email');?>?subject=Insurance%20Inquiry"><?php the_sub_field('email');?></a>
                                </p>
                            </div>
                            <div class="img-block">
                                <img src="<?php the_sub_field('image');?>" class="img-responsive">
                            </div>
                        </div>
                        <a href="<?php the_sub_field('button_link');?>" class="button-quote"><?php the_sub_field('button_text');?></a>
                    </div>
<?php endwhile;endif;?>
<?php endif;?>

                </div>
            </div>
<?php endwhile;endif;?>
        </div>
        
    </div>
</div>
<!-- Section Landing Content -->
<?php } ?>

<?php if( have_rows('full_width_content') ): while ( have_rows('full_width_content') ) : the_row(); ?>
<?php if( get_row_layout() == 'infobox_section' ) {?>
<!-- Section Infobox -->
<div class="section section-infobox">
    <div class="container">

        <div class="row">
            <div class="col-xs-12">
                <div class="title-block m-bottom-30">
                    <?php if(get_sub_field('section_title')){ ?><h2><?php the_sub_field('section_title');?></h2><?php } ?>
                    <?php if(get_sub_field('section_subtitle')){ ?><h4 class="color-orange text-bold"><?php the_sub_field('section_subtitle');?></h4><?php } ?>
                </div>
            </div>
        </div>

        <div class="row">
<?php if(have_rows('infobox')): while (have_rows('infobox')) : the_row();?>
            <div class="col-xs-12 col-sm-3">
                <div class="infobox-block">
                    <div class="img-block">
<?php if(get_sub_field('url')) { ?>
                        <a href="<?php the_sub_field('url');?>">
<?php if(get_sub_field('image')){ ?>
                            <img src="<?php the_sub_field('image');?>" class="img-responsive">
<?php } ?>
                        </a>
<?php } else {?>
<?php if(get_sub_field('image')){ ?>
                        <img src="<?php the_sub_field('image');?>" class="img-responsive">
<?php } ?>
<?php } ?>
                    </div>
                    <div class="info-block">
<?php if(get_sub_field('url')) { ?>
                        <a href="<?php the_sub_field('url');?>"><h3><?php the_sub_field('title');?></h3></a>
<?php } else {?>
                        <h3><?php the_sub_field('title');?></h3>
<?php } ?>
<?php if(get_sub_field('text')){ ?>
                        <p><?php the_sub_field('text');?></p>
<?php } ?>
                    </div>
                </div>
            </div>
<?php endwhile; endif;?>
        </div>
    </div>
</div>
<!-- Section Infobox -->
<?php }?>
<?php if( get_row_layout() == 'quote_section' ) {?>
<?php if(get_sub_field('quote')){ ?>
<!-- Section Quote -->
<div class="section section-quote">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="quote-block text-center">
                    <h2><?php the_sub_field('quote');?></h2>
                </div>
            </div>
        </div>        
    </div>
</div>
<!-- Section Quote -->
<?php } ?>
<?php }?>
<?php if( get_row_layout() == 'statistic_section' ) {?>
<!-- Section Static -->
<div class="section section-static">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="static-block">
<?php if(get_sub_field('left_text')){ ?>
                    <div class="title"><h2><?php the_sub_field('left_text');?></h2></div>
<?php }?>
<?php if(get_sub_field('right_text')){ ?>
                    <div class="text"><p><?php the_sub_field('right_text');?></p></div>
<?php }?>
                </div>
            </div>
        </div>        
    </div>
</div>
<!-- Section Static -->
<?php }?>
<?php if( get_row_layout() == 'testimonial_slider_section' ) {?>
<!-- Section Testimonials Slider -->
<div class="section section-tslider">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

<?php if(have_rows('testimonials')): ?>
                <div id="owl-tslider" class="owl-carousel testimonials-slider">
<?php while (have_rows('testimonials')) : the_row();?>
                    <div class="owl_slider">
                        <div class="slider-block">
<?php if(get_sub_field('image')){ ?>
                            <div class="img-block">
                                <img src="<?php the_sub_field('image');?>" class="img-responsive">
                            </div>
<?php }?>
                            <div class="content-block">
<?php if(get_sub_field('testimonial_text')){ ?>
                                <p><?php the_sub_field('testimonial_text');?></p>
<?php }?>
<?php if(get_sub_field('name')){ ?>
                                <h4><?php the_sub_field('name');?></h4>
<?php }?>
<?php if(get_sub_field('company_url')) { ?>
                                <div class="company"><a href="<?php the_sub_field('company_url');?>"><?php the_sub_field('company');?></a></div>
<?php } else {?>
                                <div class="company"><?php the_sub_field('company');?></div>
<?php } ?>
                            </div>
                        </div>
                    </div>
<?php endwhile;?>
                    
                </div>
<?php endif;?>

            </div>
        </div>        
    </div>
</div>
<!-- Section Testimonials Slider -->
<?php }?>
<?php if( get_row_layout() == 'logo_section' ) {?>
<!-- Section Icon -->
<div class="section section-icon">
    <div class="container">

<?php if(have_rows('logo_group')): while (have_rows('logo_group')) : the_row();?>
<?php if(get_sub_field('section_title')){ ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="title-block">
                    <h2><?php the_sub_field('section_title');?></h2>
                </div>
            </div>
        </div>
<?php }?>

<?php if(have_rows('logos')): ?>
        <div class="row">
<?php while (have_rows('logos')) : the_row();?>
            <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="icon-block">
<?php if(get_sub_field('company_url')) { ?>
                    <a href="<?php the_sub_field('company_url');?>" target="_blank"><img src="<?php the_sub_field('company_logo');?>" class="img-responsive" border="0"/></a>
<?php } else {?>
                    <img src="<?php the_sub_field('company_logo');?>" class="img-responsive" border="0"/>
<?php } ?>
                </div>
            </div>
<?php endwhile;?>
        </div>
<?php endif;?>

<?php endwhile; endif;?>
        
    </div>
</div>
<!-- Section Icon -->
<?php }?>
<?php if( get_row_layout() == 'intro_section' ) {?>
<?php if(get_sub_field('intro_text')){ ?>
<!-- Section Intro -->
<div class="section section-landing-intro section-landing-blue">
    <div class="container">

        <div class="row">
            <div class="col-xs-12">
                <div class="content-block">
                    <?php the_sub_field('intro_text');?>
                </div>
            </div>
        </div>
        
    </div>
</div>
<!-- Section Intro -->
<?php }?>
<?php }?>
<?php if( get_row_layout() == 'content_section' ) {?>
<?php if(get_sub_field('contents')){ ?>
<!-- Section Content -->
<div class="section section-landing-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="content-block">
                    <?php the_sub_field('contents');?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Section Content -->
<?php }?>
<?php }?>
<?php endwhile; wp_reset_postdata(); endif;?> 
<?php get_footer('landing'); ?>