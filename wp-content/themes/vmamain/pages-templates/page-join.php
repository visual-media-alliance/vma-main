<?php
/**
 * Join page template
 *
 * Template Name: Join
 *
 * @package VMA-Main
 */
get_header();
?>

<!-- HERO SECTION -->
<?php
if (have_rows('hero_section')):
while (have_rows('hero_section')): the_row();
$banner_image = get_sub_field('background_image');
?>
<div class="section section-hero section-join section-parallax" data-image-src="<?php echo $banner_image; ?>" data-parallax="scroll" data-bleed="0" data-position="center" draggable="disable">
    <div class="container">
        <div class="col-xs-12 no-padding">            
			<div class="join-block">
				<h1 class="h1 text-bold"><?php echo get_sub_field('title'); ?></h1>
				<h4><?php echo get_sub_field('text'); ?></h4>
				<div class="button-block"><a class="button button-normal button-orange uppercase animated-normal" href="<?php echo get_sub_field('button_url'); ?>" target="_blank"><?php echo get_sub_field('button_text'); ?> <i class="icon icon-angle-right text-bold"></i></a></div>
			</div>
        </div>
		<div class="clear"></div>
    </div>
</div>
<?php endwhile; endif;?>
<!-- HERO SECTION -->

<!-- INTRO SECTION -->
<?php
if (have_rows('intro_section')):
while (have_rows('intro_section')): the_row();
?>
<div class="section section-intro bg-grey">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1">
				<div class="content-block text-center">
					<h2 class="h3 text-bold color-orange text-center"><?php echo get_sub_field('intro_title'); ?></h2>
					<?php echo get_sub_field('intro_details'); ?>
				</div>
			</div>
		</div>				
	</div>
</div>
<?php endwhile; endif;?>
<!-- INTRO SECTION -->

<!-- CONTENT SECTION -->
<div class="section section-howwedo" >
	<div class="container">
		<div class="row">
			<?php if (have_rows('how_we_do')):
			while (have_rows('how_we_do')): the_row();?>
			<div class="col-xs-12 col-sm-6">
				<div class="content-block">
					<div class="title-block">
						<h2 class="h3 border-title color-orange"><?php echo get_sub_field('title'); ?></h2>
					</div>
					<div class="text-block">
						<?php echo get_sub_field('details'); ?>	
					</div>
				</div>
			</div>
			<?php endwhile; endif;?>
			<?php if (have_rows('whos_eligible_to_join')):
			while (have_rows('whos_eligible_to_join')): the_row();?>
			<div class="col-xs-12 col-sm-6 ">
				<div class="content-block">
					<div class="title-block">
						<h2 class="h3 border-title color-orange"><?php echo get_sub_field('title'); ?></h2>
					</div>
					<div class="text-block">
						<?php echo get_sub_field('details'); ?>
					</div>
				</div>
			</div>
			<?php endwhile; endif;?>
		</div>
	</div>
</div>
<!-- CONTENT SECTION -->

<!-- CONTENT SECTION -->
<?php if (have_rows('membership_offers')):
while (have_rows('membership_offers')): the_row();?>
<div class="section section-offers bg-grey" >
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="title-block">
					<h2 class="h3 color-orange text-center"><?php echo get_sub_field('title'); ?></h2>
				</div>
				<div class="content-block">
					<table class="custom_table">
						<thead>
							<tr>
								<th>Key Member Benefits</th>
								<th>Companies</th>
								<!-- <th>Individuals</th> -->
							</tr>
						</thead>
						<tbody>
							<?php
							if (have_rows('member_benefits_table')):
							while (have_rows('member_benefits_table')): the_row();
							?>

							<tr>
								<th colspan="2" align="center" class="tr-head"><?php echo get_sub_field('title'); ?></th>
							</tr>
							<?php
						    if (have_rows('members_benefits')):
						        $index = 1;
						    while (have_rows('members_benefits')): the_row();
						    ?>
	    					<tr <?php echo (($index++)%2==0)? ' class="grey-tr"':'' ?>>
								<th><?php echo get_sub_field('benefit'); ?></th>
								<th><span class="<?php echo get_sub_field('for_companies'); ?>">&nbsp;</span></th>
								<!-- <th><span class="<?php echo get_sub_field('for_individuals'); ?>">&nbsp;</span></th> -->
							</tr>
							<?php endwhile; endif;?>
							<?php endwhile; endif;?>
						</tbody>						
					</table>
				</div>
				<div class="content-block">
					<p>&nbsp;</p>
					<?php echo get_sub_field('contents'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endwhile; endif;?>
<!-- CONTENT SECTION -->

<!-- BECOME MEMBER SECTION -->
<?php
if (have_rows('become_a_member')):
while (have_rows('become_a_member')): the_row();
$banner_image = get_sub_field('background_image');
?>
<div class="section section-membership relative section-parallax" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri(); ?>/library/images/membership-section-bg.jpg" data-bleed="0" data-position="center">	
    <div class="container">
    	<div class="row">
			<div class="col-xs-12">
			    <div class="membership-block text-center">
		            <h2 class="text-bold uppercase color-white"><?php echo get_sub_field('title'); ?></h2>
					<a href="<?php echo get_sub_field('button_url'); ?>" target="_blank" class="button button-primary button-orange" ><?php echo get_sub_field('button_text'); ?> <i class="icon icon-angle-right"></i></a> 			        
			    </div>
			</div>
		</div>
    </div>
</div>
<?php endwhile; endif;?>
<!-- BECOME MEMBER SECTION -->

<?php get_footer(); ?>