<?php
/**
 * Design page template
 *
 * Template Name: Registration Template
 *
 * @package VMA-Main
 */
get_header();
?>

<!-- HERO SECTION -->
<div id="parallax" class="section section-banner section-single-banner section-parallax" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri(); ?>/library/images/events-details-banner.jpg" data-bleed="0" data-position="top" >
	<div class="container banner-container">
		<div class="col-xs-12">
			<div class="banner-block">
				<div class="col-xs-12 text-center">
					<h1 class="h1 text-bold color-white fade-scroll">Event Registration</h1>
				</div>
				<div class="clear" ></div>
			</div>
		</div>
	</div>
</div>
<!-- HERO SECTION -->


<!-- REGISTRATION SECTION -->
<div class="section section-registration" >
	<div class="container registration-container">
		<div class="col-xs-12 col-sm-8">			
			<div class="registration-form">
				<form action="<?php echo get_site_url(); ?>/confirmation"> 
					<div class="form-block your-information">
						<div class="title-block"><h3 class="pull-left"><span>1</span> Your Information</h3><span class="pull-right warning color-orange">*Field required</span></div>
						<div class="row">
							<div class="form-group col-xs-12 col-sm-6">
								<input class="form-control" type="text" placeholder="Full Name*" />
							</div>
							<div class="form-group col-xs-12 col-sm-6">
								<input class="form-control" type="text" placeholder="Last Name*" />
							</div>
						</div>
						<div class="row">
							<div class="form-group col-xs-12">
								<input class="form-control" type="text" placeholder="Email Address*" />
							</div>
						</div>
						<div class="row">
							<div class="form-group col-xs-12">
								<input class="form-control" type="text" placeholder="Phone Number*" />
							</div>
						</div>
						<div class="row">
							<div class="form-group col-xs-12">
								<input class="form-control" type="text" placeholder="Company" />
							</div>
						</div>
						<div class="row">
							<div class="form-group col-xs-12">
								<span class="text-semibold" style="margin-right: 10px;">Are you a member of VMA?</span>
								<label class="text-regular radio-inline">
									<input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> No
								</label>
								<label class="text-regular radio-inline">
									<input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Yes
								</label>
							</div>
						</div>
					</div>

					<div class="form-block address">
						<div class="title-block"><h3><span>2</span> Billing Address</h3></div>
						<div class="row">
							<div class="form-group col-xs-12">
								<input class="form-control" type="text" placeholder="Address*" />
							</div>
						</div> 
						<div class="row">
							<div class="form-group col-xs-12 col-sm-6">
								<input class="form-control" type="text" placeholder="City*" />
							</div>
							<div class="form-group col-xs-12 col-sm-6">
								<select class="form-control">
									<option>State*</option>
									<?php 
									for($i = 1; $i <= 5; $i++){
										echo '<option value="'.$i.'">Option '.$i.'</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-xs-12 col-sm-6">
								<input class="form-control" type="text" name="billing_zip" placeholder="Zip*" value="">
							</div>
							<div class="form-group col-xs-12 col-sm-6">
								<select class="form-control">
									<option>Country*</option>
									<?php 
									for($i = 1; $i <= 5; $i++){
										echo '<option value="'.$i.'">Option '.$i.'</option>';
									}
									?>
								</select>
							</div>
						</div>
					</div>

					<div class="form-block payment-information">
						<div class="title-block"><h3><span>3</span> Payment Information</h3></div>
						<div class="row">
							<div class="form-group col-xs-12">
								<span class="text-large">Accepted Payments:</span>
								<div class="radio-inline" >
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/cc-options.png" class="img-responsive" style="max-width: 157px;" />
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="form-group col-xs-12">
								<input class="form-control" type="text" placeholder="Cardholder Name (as it appears on card)*" />
							</div>
						</div>
						<div class="row">
							<div class="form-group col-xs-12">
								<input class="form-control" type="text" placeholder="Card Number*" />
							</div>
						</div>
						
						<div class="row">
							<div class="form-group col-xs-12 col-sm-6">
								<select class="form-control">
									<option>Month*</option>
									<?php 
									for($i = 1; $i <= 5; $i++){
										echo '<option value="'.$i.'">Option '.$i.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-xs-12 col-sm-6">
								<select class="form-control">
									<option>Year*</option>
									<?php 
									for($i = 1; $i <= 5; $i++){
										echo '<option value="'.$i.'">Option '.$i.'</option>';
									}
									?>
								</select>
							</div>
						</div>
						
						<div class="row row-7">
							<div class="form-group col-xs-12 col-sm-6">
								<input class="form-control" type="text" placeholder="CVV*" />
							</div>
						</div>
						
						<div class="row">
							<div class="form-group form-group-margin-top col-xs-12">
								<span>All transactions are secure with</span>
								<div class="radio-inline no-padding">
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/geocerts-ssl.png" class="img-responsive" style="max-width: 129px; margin-left: 5px;"/>
								</div>
							</div>
						</div>
						
					</div>

					<div class="form-block total-amount">
						<div class="row">
							<div class="form-group">
								<div class="col-xs-6">Total</div>
								<div class="col-xs-6 text-right">$7,092</div>
							</div>
						</div>
					</div>
					<div class="form-block privacy-policy">
						<div class="form-group">
							<label class="text-regular">
								<input type="checkbox"> By checking this box you have reviewed and agree to VMA's <a href="#">Terms of Service</a>, <a href="#">Refund Policy</a> and our <a href="#">Privacy Policy</a>.
							</label>
						</div>
					</div>
					<div class="form-block registration-button">
						<div class="form-group">
							<button class="button bg-grey animated-normal btn-cancel" onclick='backToEvent()'>Cancel</button>
							<button type="submit" class="button button-primary button-orange" name="form-name" value="register">Register <i class="icon icon-angle-right"></i></button>
						</div>
					</div>

				</form>
			</div>
		</div>
		
		<div class="col-xs-12 col-sm-4">
			<div class="sidebar-right registration-right">
				<div class="class-info-block">
					<div class="form-header">
						<h3 class="text-bold">Event Details</h3>
					</div>
					<div class="form-body">
						<form>
							<div class="form-row row">
								<div class="col-xs-5 col-sm-4">
									<label>Date:</label>
								</div>
								<div class="col-xs-7 col-sm-8">
									<span>Nov 16, 2016</span>
								</div>
							</div>

							<div class="form-row row">
								<div class="col-xs-5 col-sm-4">
									<label>Time:</label>
								</div>
								<div class="col-xs-7 col-sm-8">
									<span>6 PM - 9 PM</span>
								</div>
							</div>

							<div class="form-row row">
								<div class="col-xs-5 col-sm-4">
									<label>Location:</label>
								</div>
								<div class="col-xs-7 col-sm-8">
									<span>Santa Clara</span>
								</div>
							</div>
							<div class="form-row row">
								<div class="col-xs-5 col-sm-4">
									<label>Price:</label>
								</div>
								<div class="col-xs-7 col-sm-8">
									<span>$25</span>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="have-a-question">
					<div class="form-header">
						<h3 class="text-bold">Have a Question?</h3>
					</div>
					<div class="form-body"><p>Contact VMA at <a href="tel:800.659.3363">800.659.3363</a> or <a href="mailto:info@vma.bz">info@vma.bz</a></p>
	</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- REGISTRATION SECTION -->

<?php get_footer(); ?>