<?php
/**
 * Blog Articles page template
 *
 * Template Name: Blog Articles
 *
 * @package VMA-Main
 */
get_header();
?>
<!-- HERO SECTION -->
<?php
if (have_rows('hero_section')):
while (have_rows('hero_section')): the_row();
$banner_image = get_sub_field('background_image');
?>
<div class="section section-banner section-parallax" data-parallax="scroll" data-image-src="<?php echo $banner_image;?>" data-bleed="0" data-position="center">
    <div class="container">
		<div class="row">
	        <div class="col-xs-12">
				<div class="banner-block text-center">
					<h1 class="h1 text-bold color-white fade-scroll"><?php the_sub_field('title');?></h1>
				</div>
	        </div>
			<div class="clear" ></div>
        </div>
    </div>
</div>
<?php endwhile; endif;?>
<!-- HERO SECTION -->
<!-- BLOG ARCHIVE SECTION -->
<div class="section section-blog-list bg-grey" >
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8">
				<div class="blog-group">
	<?php $paged= get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	$args = array(
	'post_type' => 'post',
	'posts_per_page' => 10,
	'order'    => 'DESC',
	'paged'=>$paged
	);
	$the_query = new WP_Query($args);
	if ($the_query->have_posts()) :?>
	<?php
	while ($the_query->have_posts()): $the_query->the_post();
	$featured_image = get_field('main_image',get_the_ID());
	?>
					<div class="blog-block">
						<div class="img-block fill" style="background-image: url(<?php echo get_field('main_image',get_the_ID());?>);">
							<a href="<?php the_permalink();?>"><img src="<?php echo get_template_directory_uri(); ?>/library/images/blog-pholder.png" class="img-responsive" alt=""/></a>
						</div>
						<div class="blog-content">
							<a href="<?php the_permalink(); ?>" class="animated-normal"><h3 class="text-bold"><?php the_title(); ?></h3></a>
							<span class="date block"><span>Posted by:</span> <span class="color-orange"><?php echo get_field('displayed_author_name', get_the_ID()) ? get_field('displayed_author_name', get_the_ID()) : get_the_author();?></span> | <?php echo get_the_date('M d, Y', get_the_ID() );?></span>
							<p><?php echo substr(strip_tags(get_the_content()), 0, 172); ?>...</p>
							<a href="<?php the_permalink(); ?>" class="button button-normal color-orange text-semibold capitalize animated-normal" >Read More <i class="icon icon-angle-right"></i></a>
						</div>
					</div>
	<?php endwhile;?>
					 <div class="pagination">
	                    <?php
	                    $big = 999999999; // need an unlikely integer
	                    echo paginate_links( array(
	                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	                        'format' => '?paged=%#%',
	                        'mid_size'  => 8,
	                        'current' => max( 1, get_query_var('paged') ),
	                        'total' => $the_query->max_num_pages,                        
						    'prev_text'    => '«',
						    'next_text'    => '»'
	                    ) );?>
	                </div>
	<?php wp_reset_postdata();?>
	<?php else: ?>
		No blog found.
	<?php endif; ?>				
				</div>
			</div>
			<div class="col-xs-12 col-sm-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</div>
<!-- BLOG ARCHIVE SECTION -->
<?php get_footer(); ?>