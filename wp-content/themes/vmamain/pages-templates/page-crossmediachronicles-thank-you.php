<?php
/**
 * Crossmediachronicles thank you template
 *
 * Template Name: Crossmediachronicles thank you
 *
 * @package VMA
 */
get_header('chronicles');
?>
<!-- FB SPLASH SECTION -->
<div class="section section-splash section-thankyou fill fill-top" style="background-image: url(<?php echo get_template_directory_uri(); ?>/library/images/main-bg.jpg)">
    <div class="container">
	    <div class="row">
	        <div class="col-xs-12">
	            <div class="title-block text-center">
	            	<h1 class="h1 text-bold uppercase">Thank You!</h1>
	            	<p class="text-semibold">We're glad you found us. Thanks for signing up for the VMA Cross-Media Chronicles. We hope you enjoy the inspiration.</p>
	            </div>
	            <div class="slogan-block text-center">
	            	<div class="slogan"><p>Please check out all of what VMA has to offer</p></div>
	            	<div class="button-block">
	                    <a href="http://main.vma.bz/" target="_blank" class="button button-primary button-border-white text-bold">Visit VMA <i class="icon icon-angle-right text-bold"></i></a> 
	                </div>
	            </div>
				<div class="row link-group">	        
			        <div class="col-xs-6 col-sm-4">
				        <a href="http://submain.vma.bz/Default.aspx?pageid=220" target="_blank" class="block color-white">
				            <div class="intro-block">	                
				                <div class="image-block">
				                    <img src="<?php echo get_template_directory_uri(); ?>/library/images/networking-icon-w.png" class="img-responsive" alt="Networking"/>
				                </div>
				                <div class="text-block">
					                <h3 class="text-bold color-white">Networking</h3>
				                </div>
				                <div class="button-block">
				                    <span class="button button-normal text-bold capitalize color-white animated-normal" >Networking Events <i class="icon icon-angle-right text-bold"></i></span>
				                </div>
				            </div>
				        </a>
			        </div>
			        <div class="col-xs-6 col-sm-4">
				        <a href="http://submain.vma.bz/Default.aspx?pageid=2" target="_blank" class="block color-white">
				            <div class="intro-block">	                
				                <div class="image-block">
				                    <img src="<?php echo get_template_directory_uri(); ?>/library/images/education-icon-w.png" class="img-responsive" alt="Education"/>
				                </div>
				                <div class="text-block">
					                <h3 class="text-bold color-white">Education</h3>
				                </div>
				                <div class="button-block">
				                    <span class="button button-normal text-bold capitalize color-white animated-normal" >View Our Programs <i class="icon icon-angle-right text-bold"></i></span>
				                </div>
				            </div>
				        </a>
			        </div>
			        <div class="col-xs-6 col-sm-4">
				        <a href="http://submain.vma.bz/Default.aspx?pageid=4" target="_blank" class="block color-white">
				            <div class="intro-block">
				                <div class="image-block">
				                    <img src="<?php echo get_template_directory_uri(); ?>/library/images/insurance-icon-w.png" class="img-responsive" alt="Insurance"/>
				                </div>
				                <div class="text-block">
					                <h3 class="text-bold color-white">Insurance</h3>
				                </div>
				                <div class="button-block">
				                    <span class="button button-normal text-bold capitalize color-white animated-normal" >Get Insured <i class="icon icon-angle-right "></i></span>
				                </div>
				            </div>
				        </a>
			        </div>
			        <div class="col-xs-6 col-sm-4">
				        <a href="http://submain.vma.bz/Default.aspx?pageid=28" target="_blank" class="block color-white">
				            <div class="intro-block">	                
				                <div class="image-block">
				                    <img src="<?php echo get_template_directory_uri(); ?>/library/images/group-purchase-icon-w.png" class="img-responsive" alt="Group Purchase"/>
				                </div>
				                <div class="text-block">
					                <h3 class="text-bold color-white">Group Purchase</h3>
				                </div>
				                <div class="button-block">
				                    <span class="button button-normal text-bold capitalize color-white animated-normal" >Group Rates <i class="icon icon-angle-right text-bold"></i></span>
				                </div>
				            </div>
				        </a>
			        </div>
			        <div class="col-xs-6 col-sm-4">
				        <a href="http://submain.vma.bz/Default.aspx?pageid=29" target="_blank" class="block color-white">
				            <div class="intro-block">	                
				                <div class="image-block">
				                    <img src="<?php echo get_template_directory_uri(); ?>/library/images/sales-support-icon-w.png" class="img-responsive" alt="Sales Support"/>
				                </div>
				                <div class="text-block">
					                <h3 class="text-bold color-white">Sales Support</h3>
				                </div>
				                <div class="button-block">
				                    <span class="button button-normal text-bold capitalize color-white animated-normal" >Find Out How <i class="icon icon-angle-right text-bold"></i></span>
				                </div>
				            </div>
				        </a>
			        </div>
			        <div class="col-xs-6 col-sm-4">
				        <a href="http://submain.vma.bz/Default.aspx?pageid=3" target="_blank" class="block color-white">
				            <div class="intro-block">	                
				                <div class="image-block">
				                    <img src="<?php echo get_template_directory_uri(); ?>/library/images/business-services-icon-w.png" class="img-responsive" alt="Business Services"/>
				                </div>
				                <div class="text-block">
					                <h3 class="text-bold color-white">Business Services</h3>
				                </div>
				                <div class="button-block">
				                    <span class="button button-normal text-bold capitalize color-white animated-normal" >Learn More <i class="icon icon-angle-right "></i></span>
				                </div>
				            </div>
				        </a>
			        </div>
				</div>
	        </div>
	        <div class="col-xs-12">
	        	<div class="footer text-center">
	        		<div class="footer-logo"><a href="http://main.vma.bz/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/vma-insurance-logo.png" class="img-responsive"></a></div>
	        		<div class="address">
	        			<p>665 Third Street, Suite 500, San Francisco, CA 94107</p>
	        			<p>Phone <a href="tel:8006593363">800.659.3363</a> | Fax 800.824.1911 | <br class="visible-xs"><a href="mailto:shannon@vma.bz">info@vma.bz</a></p>
	        		</div>
	        	</div>
	        </div>
	    </div>
    </div>
</div>
<!-- FB SPLASH SECTION -->
<?php get_footer('landingpage'); ?>