<?php
/**
 * About page template
 *
 * Template Name: About
 *
 * @package VMA-Main
 */
get_header();
?>

<!-- HERO SECTION -->
<?php
if (have_rows('hero_section')):
while (have_rows('hero_section')): the_row();
$banner_image = get_sub_field('background_image');
?>
<div class="section section-hero section-join section-parallax" data-image-src="<?php echo $banner_image; ?>" data-parallax="scroll" data-bleed="0" data-position="center" draggable="disable">
    <div class="container">
        <div class="col-xs-12 no-padding">            
			<div class="join-block">
				<h1 class="h1 text-bold"><?php echo get_sub_field('title'); ?></h1>
				<h4><?php echo get_sub_field('text'); ?></h4>
				<div class="button-block"><a class="button button-normal button-orange uppercase animated-normal" href="<?php echo get_sub_field('button_url'); ?>"><?php echo get_sub_field('button_text'); ?> <i class="icon icon-angle-right text-bold"></i></a></div>
			</div>
        </div>
		<div class="clear"></div>
    </div>
</div>
<?php endwhile; endif;?>
<!-- HERO SECTION -->

<!-- Intro SECTION -->
<?php
if (have_rows('intro_section')):
while (have_rows('intro_section')): the_row();
?>
<div class="section section-intro bg-grey">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1">
				<div class="content-block text-center">
					<h2 class="h3 text-bold color-orange text-center"><?php echo get_sub_field('title'); ?></h2>
					<p><?php echo get_sub_field('text'); ?></p>
				</div>	
			</div>
		</div>				
	</div>
</div>
<?php endwhile; endif;?>
<!-- Intro SECTION -->

<!-- CONTENT SECTION -->
<?php
if (have_rows('who_do_we_serve')):
while (have_rows('who_do_we_serve')): the_row();
$image = get_sub_field('image');
?>
<div class="section section-howwedo" >
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<div class="content-block image-block">
					<img src="<?php echo $image; ?>" class="img-responsive" alt="Map"/>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 ">
				<div class="content-block">
					<div class="title-block">
						<h2 class="h3 border-title color-orange"><?php echo get_sub_field('title'); ?></h2>
					</div>
					<div class="text-block">
					<?php echo get_sub_field('description'); ?>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endwhile; endif;?>
<!-- CONTENT SECTION -->

<!-- CONTENT SECTION -->
<?php
if (have_rows('more_bang_less_buck')):
while (have_rows('more_bang_less_buck')): the_row();
$image = get_sub_field('image');
?>
<div class="section section-howwedo bg-grey" >
	<div class="container">
		<div class="row">			
			<div class="col-xs-12 col-sm-7 ">
				<div class="content-block">
					<div class="title-block">
						<h2 class="h3 border-title color-orange"><?php echo get_sub_field('title'); ?></h2>
					</div>
					<div class="text-block">
						<?php echo get_sub_field('description'); ?>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-5">
				<div class="content-block image-block">
					<img src="<?php echo $image; ?>" class="img-responsive" alt="Map"/>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endwhile; endif;?>
<!-- CONTENT SECTION -->

<!-- BETTER SECTION -->
<?php
if (have_rows('better_section')):
while (have_rows('better_section')): the_row();
?>
<div class="section section-better">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="heading-block text-center">
					<h2 class="h3 color-orange border-title-center"><?php echo get_sub_field('title'); ?></h2>
					<?php echo get_sub_field('text'); ?>
				</div>
				<div class="content-block grey-block bg-grey text-center">
					<?php if(get_sub_field('pia_title')) { ?><p><strong><?php echo get_sub_field('pia_title'); ?></strong></p><?php }?>
					<?php echo get_sub_field('pia_text'); ?>
				</div>
				<div class="content-block text-center">
					<div class="row">
						<div class="col-xs-12 col-sm-10 col-sm-offset-1">
						<?php echo get_sub_field('description'); ?>
						</div>
					</div>
				</div>
				<div class="button-block text-center">
					<a href="<?php echo get_sub_field('button_url'); ?>" class="button button-primary button-orange"><?php echo get_sub_field('button_text'); ?> <i class="icon icon-angle-right"></i></a>
				</div>

			</div>
		</div>				
	</div>
</div>
<?php endwhile; endif;?>
<!-- BETTER SECTION -->


<!-- BECOME MEMBER SECTION -->
<?php
if (have_rows('become_member')):
while (have_rows('become_member')): the_row();
$image = get_sub_field('background_image');
?>
<div class="section section-membership relative section-parallax" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri(); ?>/library/images/membership-section-bg.jpg" data-bleed="0" data-position="center">	
    <div class="container">
    	<div class="row">
			<div class="col-xs-12">
			    <div class="membership-block text-center">
		            <h2 class="text-bold uppercase color-white"><?php echo get_sub_field('title'); ?></h2>
					<p class="color-white"><?php echo get_sub_field('text'); ?></p>
					<a href="<?php echo get_sub_field('button_url'); ?>" class="button button-primary button-orange" ><?php echo get_sub_field('button_text'); ?> <i class="icon icon-angle-right"></i></a> 			        
			    </div>
			</div>
		</div>
    </div>
</div>
<?php endwhile; endif;?>
<!-- BECOME MEMBER SECTION -->

<?php get_footer(); ?>