<?php
/**
 * Home page template
 *
 * Template Name: Home
 *
 * @package VMA-Main
 */


 


get_header();
?>
<!-- HERO SECTION -->
<?php


if (have_rows('hero_section')):
while (have_rows('hero_section')): the_row();
$banner_image = get_sub_field('background_image');
?>
<div class="section section-hero section-parallax" data-parallax="scroll" data-image-src="<?php echo $banner_image; ?>" data-bleed="0" data-position="center" draggable="disable">	
    <div class="container hero-container">
    	<div class="row">
	        <div class="col-xs-12">            
				<div class="hero-block text-center fade-scroll">
					<h1 class="h1 text-light"><?php echo get_sub_field('title'); ?></h1>
					<div class="cd-intro">		
						<div class="cd-headline letters rotate-2">
							<span class="cd-words-wrapper h1 text-semibold">
								<?php
								$counter = 1;
								while (have_rows('services')): the_row();
								$status = ($counter == 1) ? " class='is-visible'" : "";
								?>	
								<b<?php echo $status; ?>><?php echo get_sub_field('service_name'); ?></b>
								<?php $counter++; ?>
								<?php endwhile;?>
							</span>
						</div>			
					</div>					
					<div class="slogan"><p><?php echo get_sub_field('text'); ?></p></div>
				</div>
	        </div>
			<div class="clear" ></div>
		</div>
    </div>
</div>
<?php endwhile; endif;?>
<!-- HERO SECTION -->
<!-- SERVICES SECTION -->
<?php

if (have_rows('services_section')):
while (have_rows('services_section')): the_row();
?>
<div class="section section-services" >
    <div class="container services-container">
    	<div class="title-block text-center">
	      	<h2 class="h1 text-light"><?php echo get_sub_field('title'); ?></h2>
	    </div>
	    <div class="row">
<?php
if (have_rows('services')):
while (have_rows('services')): the_row();
?>	        
	        <div class="col-xs-6 col-sm-4">
		        <a href="<?php echo get_sub_field('button_url'); ?>" class="block color-primary">
		            <div class="intro-block">	                
		                <div class="image-block">
		                    <img src="<?php echo get_sub_field('service_icon');?>" class="img-responsive" width="62" height="59" alt="<?php echo get_sub_field('service_name'); ?>"/>
		                </div>
		                <div class="text-block">
			                <h3 class="text-bold color-primary"><?php echo get_sub_field('service_name'); ?></h3>
			                <p><?php echo get_sub_field('short_description'); ?></p>
		                </div>
		                <div class="button-block">
		                    <span class="button button-normal text-bold capitalize animated-normal" ><?php echo get_sub_field('button_name'); ?> <i class="icon icon-angle-right text-bold"></i></span>
		                </div>
		            </div>
		        </a>
	        </div>
<?php endwhile; endif;?>
		</div>
    </div>
</div>
<?php endwhile; endif;?>
<!-- SERVICES SECTION -->

<!-- MEMBERSHIP SECTION -->
<?php
if (have_rows('membership_section')):
while (have_rows('membership_section')): the_row();
?>
<div class="section section-membership relative section-parallax" data-parallax="scroll" data-image-src="<?php echo get_sub_field('background_image');?>" data-bleed="0" data-position="center">	
    <div class="container">
    	<div class="row">
			<div class="col-xs-12">
			    <div class="membership-block text-center">
			    	<img src="<?php echo get_sub_field('membership_icon');?>" class="img-responsive" alt="Membership"/>
		            <h2 class="text-bold uppercase color-white"><?php echo get_sub_field('membership_title');?></h2>
					<p class="color-white"><?php echo get_sub_field('membership_text');?></p>
					<a href="<?php echo get_sub_field('button_url');?>" class="button button-primary button-orange" ><?php echo get_sub_field('button_name');?> <i class="icon icon-angle-right"></i></a> 			        
			    </div>
			</div>
		</div>
    </div>
</div>
<?php endwhile; endif;?>
<!-- MEMBERSHIP SECTION -->

<!-- EVENTS SECTION -->
<?php
$eventProducts = VMA_EVENTS()->queryCommingEvents();
?>
<?php if ($eventProducts): ?>
<div class="section section-events">
    <div class="container">
    	<div class="row">
	        <div class="col-xs-12">
	            <div class="title-block text-center">
	            	<h2 class="h1 page-title">Upcoming Events</h2>
	            </div>
				
				<div class="events-group">
					<table width="100%" cellsapcing="0" cellpadding="0" border="0" class="custom-table">
						<tbody><tr>
							<th>Date</th>
							<th>Events</th>
							<th>Location</th>
							<th>Time</th>
						</tr> 

						<?php


						?>
						<?php foreach ($eventProducts as $eventProduct) : ?>
						<?php
							$timestamp = $eventProduct->get_meta('WooCommerceEventsDateTimeTimestamp');

							$dt = (new DateTimeImmutable())->setTimestamp($timestamp);
							$date = $dt->format('m.d');
							$timeStart = $dt->format('H:i');
							$hoursEnd = $eventProduct->get_meta('WooCommerceEventsHourEnd');
							$minutesEnd = $eventProduct->get_meta('WooCommerceEventsMinutesEnd');
							$timeEnd = "$hoursEnd:$minutesEnd";

							$timeRange = "$timeStart - $timeEnd";
							$location = $eventProduct->get_meta('WooCommerceEventsLocation');
						?>
  						<tr>
 							<td><a href="<?= $eventProduct->get_permalink() ?>" class="color-primary"><?= $date ?></a></td>
 							<td><a href="<?= $eventProduct->get_permalink() ?>" class="color-orange"><?= $eventProduct->get_name(); ?></a></td>
							<td><a href="<?= $eventProduct->get_permalink() ?>" class="color-primary"><?= $location ?></a></td>
						  	<td><a href="<?= $eventProduct->get_permalink() ?>" class="color-primary"><?= $timeRange ?></a></td>

  						</tr>
						<?php endforeach; ?>
					</tbody></table>
				</div>

	            <div class="button-block text-center">
	            	<a href="<?= VMA_EVENTS()->archiveLink() ?>" class="button button-primary button-orange" >View All <i class="icon icon-angle-right"></i></a>
	            </div>
				
	        </div>
	    </div>
    </div>
</div>
<?php endif; ?>
<!-- EVENTS SECTION -->

 
<!-- SPOTLIGHT SECTION -->
<?php $args = array(
    'post_type'=> 'spotlightitems',
    'order'    => 'DESC',
    'post_status' => 'publish',
    'showposts'=> 100
);
query_posts( $args );
?>
<?php if(have_posts()):?>
<div class="section section-spotlight bg-grey">
    <div class="container spotlight-container">
	    <div class="row">
	        <div class="col-xs-12 no-padding">
	            <div class="title-block text-center">
	            	<h2 class="h1 text-light">Spotlight</h2>
	            </div>
				
				<div class="spotlight-container">
					<div id="owl-spotlight" class="owl-carousel">
						<?php while(have_posts()): the_post();?>
							<?php $image = get_field('spotlight_graphic');?>
						<div class="owl_spotlight spotlight-group">
							<div class="spotlight-block">
								<div class="col-xs-12 col-sm-7">
									<div class="content-block">
										<h3 class="text-bold"><?php echo the_title();?></h3>								
										<a href="<?php echo get_field('spotlight_link');?>" target="<?php echo get_field('open_link_in_new_tab');?>"><span class="block color-orange"><?php echo get_field('spotlight_subtitle');?></span></a>
<?php
$spotlight_body_text = wpautop( get_field('spotlight_body_text') );
$spotlight_body_text = substr( $spotlight_body_text, 0, strpos( $spotlight_body_text, '</p>' ) + 4 );
$spotlight_body_text = strip_tags($spotlight_body_text, '<a><strong><em>');
?>
										<p><?php echo $spotlight_body_text;?></p>
										<a href="<?php echo get_field('spotlight_link');?>" target="<?php echo get_field('open_link_in_new_tab');?>" class="button button-normal text-semibold capitalize no-padding animated-normal" ><?php echo get_field('spotlight_link_name');?> <i class="icon icon-angle-right"></i></a>
									</div>
								</div>
								<div class="col-xs-12 col-sm-5">
									<div class="img-block">
										<a href="<?php echo get_field('spotlight_link');?>" target="<?php echo get_field('open_link_in_new_tab');?>"><img src="<?php echo $image;?>" class="img-responsive" width="400" height="180" alt="<?php echo the_title();?>"/></a>
									</div>
								</div>
							</div>
						</div>
						<?php endwhile; wp_reset_postdata();?>
					</div>
				</div>
				
	        </div>
	    </div>
    </div>
</div>
<?php endif;?>
<!-- SPOTLIGHT SECTION -->

<!-- PUBLICATIONS SECTION -->
<?php $args = array(
    'post_type'=> 'publication',
    'order'    => 'DESC',
    'post_status' => 'publish',
    'showposts'=> 100
);
query_posts( $args );
?>
<?php if(have_posts()):?>
<div class="section section-publications ">
    <div class="container">
	    <div class="row">

	        <div class="col-xs-12">
	            <div class="title-block text-center">
	            	<h2 class="h1 text-light">Publications</h2>
	            </div>
				
				<div class="publications-container">
					<div id="owl-publications" class="owl-carousel">
<?php while(have_posts()): the_post();?>
						<div class="owl_publication publication-block">
<?php $image = get_field('publication_thumbnail');?>
							<a href="<?php echo get_field('publication_link_url');?>" class="block" target="_blank">
								<div class="img-block">
									<img src="<?php echo $image['sizes']['large']; ?>" class="img-responsive" width="165" height="213" alt="<?php the_title();?>"/>
								</div>
								<h3 class="color-primary"><?php the_title();?></h3>
								<span class="color-primary"><?php echo get_field('publication_link_label');?></span>
							</a>
						</div>
<?php endwhile; ?>
					</div>
				</div>
				<div class="button-block text-center" style="padding-top: 15px;">
					<a href="<?php echo site_url('publications');?>" class="button button-primary button-orange horizontal-center" >View Publications<i class="icon icon-angle-right"></i></a> 
				</div>				
	        </div>
	    </div>
    </div>
</div>
<?php endif;?>
<!-- PUBLICATIONS SECTION -->


<!-- TESTIMONIALS SECTION -->
<?php $args = array(
    'post_type'=> 'testimonial',
    'order'    => 'DESC',
    'post_status' => 'publish',
    'showposts'=>10
);
query_posts( $args );
?>
<?php if(have_posts()):?>
<div class="section section-testimonials">
    <div class="container">
    	<div class="row">
	        <div class="col-xs-12">
	            <!-- Testimonials -->
	            <div class="title-block text-center">
					<h2 class="h1">Testimonials</h2>
	            </div>
				
				<div class="testimonial-container">
					
					<div id="owl-testimonial" class="owl-carousel">
<?php while(have_posts()): the_post(); ?>
						<div class="owl_testimonial testimonial-block">
							<div class="row">
								<div class="col-xs-12 col-sm-6 author_content">
									<h3 class="h2"><?php the_title(); ?></h3>
									<i><img src="<?php echo get_template_directory_uri(); ?>/library/images/quote.jpg" width="15" height="17"> <?php echo get_field('testimonial_description');?> <img src="<?php echo get_template_directory_uri(); ?>/library/images/quote.jpg" width="15" height="17"></i>
									<div class="author_name"><span class="name text-semibold">- <?php echo get_field('client_name');?>,</span> <?php echo get_field('designation');?> </div>
									<div class="company color-orange"><a href="<?php echo get_field('company_website');?>" target="_blank"><?php echo get_field('company_name');?></a></div>
								</div>
								<div class="col-xs-12 col-sm-6 author_icon">
								<?php $image = get_field('image');?>
									<img src="<?php echo $image['sizes']['large']; ?>" class="img-responsive" alt=""/>
								</div>
							</div>
						</div>
<?php endwhile;?>
					</div>
				</div>
	            <!-- Testimonials -->
				
				<div class="button-block text-center">
					<a href="<?php echo site_url('testimonials');?>" class="button button-primary button-orange horizontal-center" >View Testimonials <i class="icon icon-angle-right"></i></a> 
				</div>
	        </div>
	    </div>
    </div>
</div>
<?php endif;?>
<!-- TESTIMONIALS SECTION -->
<!-- BLOG SECTION -->
<?php $args = array(
    'post_type'=> 'post',
    'order'    => 'DESC',
    'showposts'=>3
    // 'category_name'=>'featured'
);
query_posts( $args );
?>
<?php if(have_posts()):?>
<div class="section section-blog bg-grey">
    <div class="container blog-container">
    	<div class="row">
	        <div class="col-xs-12">
	            <div class="title-block text-center">
	            	<h2 class="h1">Blog</h2>
	            </div>
				
				<div class="blog-group">
					<div class="row">
						<?php while(have_posts()): the_post();?>
						<div class="col-xs-12 col-sm-4">
							<div class="blog-article">
								<div class="img-block fill" style="background-image: url(<?php echo get_field('main_image',get_the_ID());?>);">
									<a href="<?php the_permalink();?>"><img src="<?php echo get_template_directory_uri(); ?>/library/images/blog-pholder.png" class="img-responsive" alt="" width="500" height="225"/></a>
								</div>
								<div class="blog-content">
									<a href="<?php the_permalink();?>" class="animated-normal"><h3 class="text-bold"><?php $title = get_the_title();
										echo (strlen($title) > 70) ? substr(strip_tags($title), 0, 70).'...' : $title?></h3></a>
									<!--<span class="date block"><span>Posted by:</span> <span class="color-orange"><?php echo get_field('displayed_author_name', get_the_ID()) ? get_field('displayed_author_name', get_the_ID()) : get_the_author();?></span> | <?php echo get_the_date('M d, Y', get_the_ID() );?></span>-->
									<a href="<?php the_permalink();?>" class="button button-normal text-semibold capitalize animated-normal" >Read More <i class="icon icon-angle-right"></i></a>
								</div>
							</div>
						</div>
						<?php endwhile;?>
					</div>
				</div>
	            <div class="button-block text-center">
	            	<a href="<?php echo site_url('blog-articles');?>" class="button button-primary button-orange" >View All <i class="icon icon-angle-right"></i></a>
	            </div>
				
	        </div>
	    </div>
    </div>
</div>
<?php endif;?>
<!-- BLOG SECTION -->
<?php get_footer(); ?>