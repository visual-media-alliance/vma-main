<div class="sidebar-right">
    <?php if (! page_has_contact_form()): ?>
	<div class="join-block text-center fill" style="background-image: url(<?php echo get_template_directory_uri(); ?>/library/images/quote-bg.jpg)">
		<h3 class="color-white text-bold">Join VMA and grow your business like never before</h3>
		<a href="<?php echo site_url('join'); ?>" class="button button-primary button-orange text-normal uppercase" >Become a member <i class="icon icon-angle-right text-bold"></i></a>
	</div>
	<?php endif; ?>

	<!-- SPOTLIGHT SECTION -->
	<div class="spotlight-block">
	    <div class="title-block">
	        <h3 class="h3 text-bold">Spotlight</h3>
	    </div>
<?php $args = array(
    'post_type'=> 'spotlightitems',
    'order'    => 'DESC',
    'post_status' => 'publish',
    'showposts'=> 100
);
query_posts( $args );
?>
<?php if(have_posts()):?>
		<div class="spotlight-container">
			<div id="owl-spotlight" class="owl-carousel">
<?php while(have_posts()): the_post();?>
<?php $image = get_field('spotlight_graphic');?>
				<div class="owl_spotlight spotlight-group">
					<div class="content-block">
						<h4 class="text-bold"><?php echo the_title(); ?></h4>
						<a href="<?php echo get_field('spotlight_link');?>" class="block" target="<?php echo get_field('open_link_in_new_tab');?>"><span class="block color-orange"><?php echo get_field('spotlight_subtitle');?></span></a>
						<div class="img-block">
							<a href="<?php echo get_field('spotlight_link');?>" target="<?php echo get_field('open_link_in_new_tab');?>"><img src="<?php echo $image; ?>" class="img-responsive" alt=""/></a>
						</div>	
<?php
$spotlight_body_text = wpautop( get_field('spotlight_body_text') );
$spotlight_body_text = substr( $spotlight_body_text, 0, strpos( $spotlight_body_text, '</p>' ) + 4 );
$spotlight_body_text = strip_tags($spotlight_body_text, '<a><strong><em>');
?>
						<p><?php echo $spotlight_body_text;?></p>
						<a href="<?php echo get_field('spotlight_link');?>" class="button button-normal text-semibold capitalize no-padding animated-normal" target="<?php echo get_field('open_link_in_new_tab');?>"><?php echo get_field('spotlight_link_name');?><i class="icon icon-angle-right"></i></a>
					</div>
				</div>
<?php endwhile; wp_reset_postdata();?>
			</div>
		</div>
<?php endif; ?>
	</div>
<!-- SPOTLIGHT SECTION -->

</div>
<!-- SPOTLIGHT SECTION -->	
