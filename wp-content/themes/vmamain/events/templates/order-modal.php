<?php
/**
 * The template for showing a combination of the add to cart form and mini cart
 * bellow the product summary
 */

defined( 'ABSPATH' ) || exit;

global $product;

?>

<div class="modal fade order-modal" tabindex="-1" role="dialog" aria-labelledby="orderModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7 add-to-cart-col">
                        <div class="event-title"><?= $product->get_name() ?></div>
                        <div class="add-to-cart" >
                            <?php woocommerce_template_single_add_to_cart(); ?>
                        </div>
                    </div>
                    <div class="col-md-5 cart-summary-col">
                        <div class="summary-image-holder">
                            <?php echo woocommerce_get_product_thumbnail() ?>
                        </div>
                        <div class="minicart widget_shopping_cart_content" >
                            <?php woocommerce_mini_cart() ?>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

