<?php

defined( 'ABSPATH' ) || exit;

global $product;

$button_text = $args['button_text'] ?? '';

$expired = VMA_EVENTS()->isEventExpired($product);

?>

<?php if ($expired) : ?>
    This event has concluded
<?php else: ?>
<div class="block">
    <?php if ($product->get_type() === 'external'): ?>
        <a
            href="<?= $product->add_to_cart_url() ?>"
            class="button button-normal button-orange uppercase"
        >
            <?= $button_text ?>  <i class="icon icon-angle-right text-bold"></i>
        </a>
        <span>Hosted by partner organization</span>
            
    <?php else: ?>
        <button
            class="button button-normal button-orange uppercase open-modal-add-to-cart-button" 
            data-toggle="modal"
            data-target=".order-modal"
        >
            <?= $button_text ?>  <i class="icon icon-angle-right text-bold"></i>
        </button>
    <?php endif; ?>
</div>
<?php endif; ?>