<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
global $vmaEvents;

$event = $product;

$eventExpires = $product->get_meta('WooCommerceEventsExpireTimestamp');
$eventExpired = !!$eventExpires && $eventExpires <= current_time('timestamp');



$timestamp = $event->get_meta('WooCommerceEventsDateTimeTimestamp');

$dt = (new DateTimeImmutable())->setTimestamp($timestamp);
$date = $dt->format('M d, Y');
$timeStart = $dt->format('H:i');
$hoursEnd = $event->get_meta('WooCommerceEventsHourEnd');
$minutesEnd = $event->get_meta('WooCommerceEventsMinutesEnd');
$timeEnd = "$hoursEnd:$minutesEnd";

$timeRange = "$timeStart - $timeEnd";
$location = $event->get_meta('WooCommerceEventsLocation');

$address = '';
$city = '';
$virtual = '';

$apiKey = get_option('globalWooCommerceEventsGoogleMapsAPIKey');

$price = '9999.00';
$priceMin = '99999.00';
$priceMax = '99999.00';


if ($product->get_type() === 'variable') {
	$prices = $product->get_variation_prices()['price'];
	
	$priceMin = $product->get_variation_regular_price('min');
	$priceMax = $product->get_variation_regular_price('max');

	$price = "$$priceMin - $$priceMax";

	if ($priceMax === '0.00') {
		$price = '0.00';
	}

}

if ($product->get_type() === 'simple' || $product->get_type() === 'external') {
	$price = $product->get_price();
	if ($price) {
		$price = "$$price";
	}
}

$totalStock = $product->get_stock_quantity();
$outOfStock = $product->get_manage_stock() && ($totalStock <= 0);

$status = 'Open';
$isOpen = true;
if ($eventExpired) {
	$status = 'Closed';
	$isOpen = false;
}
elseif ($outOfStock) {
		$status = 'Sold Out';
		$isOpen = false;
}
?>

<div class="section section-event-details bg-grey">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8">
				<div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'content-block', $product ); ?>>
					<?php		
						if ( post_password_required() ) {
							echo get_the_password_form(); // WPCS: XSS ok.
							return;
						}

						do_action( 'woocommerce_before_single_product' );
					?>
                    <?php echo get_the_post_thumbnail() ?>
                    <?php the_content(); ?>
					<!--<img src="<?php echo get_template_directory_uri(); ?>/library/images/event-details.jpg" class="img-responsive" alt=""/>-->
					<!-- <p></p> -->					
				</div>
			</div>
			<div class="col-xs-12 col-sm-4">
				<div class="sidebar-right">
					<div class="event-info-block">
						<div class="form-header">
							<h3 class="text-bold">Event Details</h3>
						</div>
						<div class="form-body">
					    	<div class="form-row row">
								<div class="col-xs-5 col-sm-4">
									<label>Date:</label>
								</div>
								<div class="col-xs-7 col-sm-8">
									<span><?= $date ?></span>
								</div>
							</div>
							<div class="form-row row">
								<div class="col-xs-5 col-sm-4">
									<label>Time:</label>
								</div>
								<div class="col-xs-7 col-sm-8">
									<span><?= $timeRange ?></span>
								</div>
							</div>
							<?php if ($location): ?> 
							<div class="form-row row">
								<div class="col-xs-5 col-sm-4">
									<label>Location:</label>
								</div>
							</div>
							<div class="form-row row text-center">
								<p><?= $location ?></p>
								<div class="col-xs-7 col-sm-12">
									<img 
										src="<?= "https://maps.googleapis.com/maps/api/staticmap?center=$location&zoom=17&size=303x303&key=$apiKey"?>"
										style="max-width: 100%; max-height: auto;"
									/>
								</div>
							</div>
							<?php endif; ?>
						</div>
						<div class="form-header">
							<h3 class="text-bold">Registration</h3>
						</div>
						<div class="form-body">
						<?php if ($isOpen && $price): ?>
							<div class="form-row row">
								<div class="col-xs-5 col-sm-4">
									<label>Tickets:</label>
								</div>
								<div class="col-xs-7 col-sm-8">
									<span><?= $price ?></span>
								</div>
							</div>
						<?php endif; ?>
							<div class="form-row button-row row">
								<div class="col-xs-12">
									<a 
										<?php if ($product->get_type() === 'external' && $isOpen): ?>
											href="<?= $product->get_product_url() ?>"
										<?php elseif ($isOpen): ?>
											href="<?= $vmaEvents->ticketsLink($post) ?>"
										<?php endif; ?>
										class="btn button button-normal button-orange uppercase animated-normal button-full <?= !$isOpen ? 'disabled' : '' ?>"
										style="<?= !$isOpen ? 'background-color: #c4c4c4; background-image: none' : '' ?>"
									>
											<?php if ($isOpen): ?>
												Register<i class="icon icon-angle-right text-bold"></i>
											<?php else: ?>
												<?= $status ?>
											<?php endif; ?>
						
									</a>
									
								</div>
							</div>
							
							<div class="form-row row">
								<div class="text-center">
									<?php if ($isOpen && $product->get_type() === 'external'): ?>
										off site registration
									<?php elseif ($outOfStock): ?>
										No remaining tickets
									<?php elseif($isOpen): ?>
									<?php else: ?>
										This event has concluded
									<?php endif; ?>
								</div>
							</div>
						</div>
				    </div>
				    <div class="have-a-question">
						<div class="form-header">
							<h3 class="text-bold">Have a Question?</h3>
						</div>
						<div class="form-body">
							<p>Contact VMA at <a href="tel:800.659.3363" data-mce-href="tel:800.659.3363">800.659.3363</a> or <a href="mailto:info@vma.bz" data-mce-href="mailto:info@vma.bz">info@vma.bz</a></p>
						</div>
					</div>
		</div>
	</div>
</div>