jQuery( function( $ ) {

    
	if ( typeof wc_add_to_cart_params === 'undefined' ) {
		return false;
    }
    
    class ModalAddToCart {
        constructor() {
            this.onShowModal = this.onShowModal.bind(this);
            this.onHideModal = this.onHideModal.bind(this);
            this.onAddToCart = this.onAddToCart.bind(this);
            this.onRemoveFromCart = this.onRemoveFromCart.bind(this);
            this.removeOthers = this.removeOthers.bind(this);

            this.modalElement = $('.order-modal');
            this.$button = $('.single_add_to_cart_button');
            this.$form = this.$button.closest('form.cart');
            this.$form.isVariable = this.$form.is('.variations_form');

            this.requestData = { is_vma_event: true };


            this.initialize();

            /** remove woo core handlers and replace with our own */
            $(document.body)
                .off('click', '.add_to_cart_button')
                .off('click', '.remove_from_cart_button')
                .on( 'click', '.add_to_cart_button', this.onAddToCart )
                .on( 'click', '.remove_from_cart_button', this.onRemoveFromCart )

            this.modalElement.on('show.bs.modal', this.onShowModal);
            this.modalElement.on('hide.bs.modal', this.onHideModal);


            this.restoreToggleState();
        }

        initialize() {

            let productId;
            if (this.$form.isVariable) {
                const variationInput =  this.$form.find('input[name="variation_id"]');
                variationInput.on('change', this.onVariationChange);

                productId = this.$form.find('input[name="product_id"]').val();
            }
            else {
                productId = this.$button.attr('value');
            }

            /** Set minimal criteria for add-to-cart.js ajax handler to accept a click */
            this.$button
                /** Set classes bound to woocomerce add_to_cart handlers to enable ajax */
                .addClass('ajax_add_to_cart')
                .addClass('add_to_cart_button')
                /** Add the product_id, converted to ajax POST param in add-to-cart.js */
                .attr('data-product_id', productId)
                /**  Prevent the form from being submitted by the document */
                .click((e) => e.preventDefault());

            
        }

        restoreToggleState() {
            const state = localStorage.getItem('order-modal-state');
            if (state !== 'open') {
                return;
            }

            // $( document.body ).on('added_to_cart', function (e) {
            //     e.target.data
            // });
            this.modalElement.modal();
        }

        

        onAddToCart(e) {
            const $thisbutton = this.$button;

            if ( $thisbutton.is( '.ajax_add_to_cart' ) ) {
                if ( ! $thisbutton.attr( 'data-product_id' ) ) {
                    return true;
                }
    
                e.preventDefault();
    
                $thisbutton.removeClass( 'added' );
                $thisbutton.addClass( 'loading' );
    
                // Allow 3rd parties to validate and quit early.
                if ( false === $( document.body ).triggerHandler( 'should_send_ajax_request.adding_to_cart', [ $thisbutton ] ) ) { 
                    $( document.body ).trigger( 'ajax_request_not_sent.adding_to_cart', [ false, false, $thisbutton ] );
                    return true;
                }
    
                var data = {...this.requestData};


                // Fetch changes that are directly added by calling $thisbutton.data( key, value )
                $.each( $thisbutton.data(), function( key, value ) {
                    data[ key ] = value;
                });
    
                // Fetch data attributes in $thisbutton. Give preference to data-attributes because they can be directly modified by javascript
                // while `.data` are jquery specific memory stores.
                $.each( $thisbutton[0].dataset, function( key, value ) {
                    data[ key ] = value;
                });
    
                if (this.$form.isVariable) {
                    data['quantity'] = this.$form
                        .find('.single_variation_wrap .quantity')
                        .find('input.qty').val();
                    data['product_id'] = this.$form
                        .find('input[name="variation_id"], input.variation_id')
                        .val();
                }
                else {
                    data['quantity'] = this.$form
                        .find('.quantity .input.qty').val();
                }

                // Trigger event.
                $( document.body ).trigger( 'adding_to_cart', [ $thisbutton, data ] );
    
                $.ajax({
                    type: 'POST',
                    url: wc_add_to_cart_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'add_to_cart' ),
                    data: data,
                    success: function( response ) {
                        if ( ! response ) {
                            return;
                        }
    
                        if ( response.error && response.product_url ) {
                            window.location = response.product_url;
                            return;
                        }
    
                        // Redirect to cart option
                        if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
                            window.location = wc_add_to_cart_params.cart_url;
                            return;
                        }
    
                        // Trigger event so themes can refresh other areas.
                        $( document.body ).trigger( 'added_to_cart', [ response.fragments, response.cart_hash, $thisbutton ] );
                    },
                    dataType: 'json'
                });
            }
        };

        onRemoveFromCart(e) {
            var $thisbutton = $(e.target),
                $row        = $thisbutton.closest( '.woocommerce-mini-cart-item' );

            e.preventDefault();

            $row.block({
                message: null,
                overlayCSS: {
                    opacity: 0.6
                }
            });
    
            const data = {
                cart_item_key : $thisbutton.data( 'cart_item_key' ),
                ...this.requestData,
            };

            $.ajax({
                type: 'POST',
                url: wc_add_to_cart_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'remove_from_cart' ),
                data: data,
                success: function( response ) {
                    if ( ! response || ! response.fragments ) {
                        window.location = $thisbutton.attr( 'href' );
                        return;
                    }
    
                    $( document.body ).trigger( 'removed_from_cart', [ response.fragments, response.cart_hash, $thisbutton ] );
                },
                error: function() {
                    window.location = $thisbutton.attr( 'href' );
                    return;
                },
                dataType: 'json'
            });
        };

        onVariationChange(e) {
            /** update the button data attribute */
            () => this.$button.attr('variation_id', e.target.val)();
        }

        onShowModal(e) {
            this.removeOthers();
            localStorage.setItem('order-modal-state', 'open');
        }

        onHideModal($) {
            localStorage.setItem('order-modal-state', 'closed');
        }

        /**
         * Remove products not on the current product page from
         * the cart
         */
        removeOthers() {

            let productId;
            if (this.$form.isVariable) {
                productId = this.$form.find('input[name="product_id"]').val()
            }
            else {
                productId = this.$form
                    .find('button.single_add_to_cart_button')
                    .attr('value');
            }
            const data = {
                product_id: productId,
                ...this.requestData,
            };

            $.ajax({
                type: 'POST',
                url: wc_add_to_cart_params.wc_ajax_url.toString().replace('%%endpoint%%', 'remove_others_cart'),
                success: function(response) {
                    if ( ! response || ! response.fragments ) {
                        return;
                    }
                    $(document.body).trigger('removed_from_cart', [ response.fragments, response.cart_hash ]);
                },
                data: data,
                dataType: 'json',
            });
        }


    }

    new ModalAddToCart();
});
