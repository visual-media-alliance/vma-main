<?php
/**
 *
 * @package VMA-Insurance
 */
?>

<footer>
    <div class="section section-footer section-landing-footer">
        <div class="container">
            <div class="row" >

                <div class="col-xs-12 col-sm-8">
                    <div class="contact-info landing-contact-info">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="footer-logo">
                                    <img src="<?php echo get_template_directory_uri(); ?>/library/images/vma-footer-logo-2.png" alt="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 no-padding">
                                <div class="contact-number">
                                    <p class="text-bold">665 Third Street, Suite 500<br>San Francisco, CA 94107</p>
                                    <p>Phone: <a href="tel:800.659.3363">800.659.3363</a><br>Fax: 800.824.1911<br>
                                    Email: <a href="mailto:info@vma.bz">info@vma.bz</a></p>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="sheld-block">
                        <img src="<?php echo get_template_directory_uri(); ?>/library/images/trusted-by-businesses.png" class="img-responsive">
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="copyright text-center">
                        <span>&copy; 1986-<?php echo date('Y'); ?> <?php echo get_bloginfo('name'); ?> | <a href="<?php echo get_site_url(); ?>/privacy-policy">Privacy Policy</a></span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>


</div>
<!-- Small modal -->
<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main Container Ends-->

<!--[if lt IE 9]></div><![endif]--><?php wp_footer(); ?>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/page-landing.js"></script>

</div>
</body>
</html>