<?php

function vma_hero_section() {
    get_template_part('template-parts/hero-section');
}


/**
 * Event Template functions 
 **************************************************************/

function vma_tickets_add_to_cart() {
    global $product;
    $func = 'vma_tickets_' . $product->get_type() . '_add_to_cart';
    $func();
}

function vma_tickets_variable_add_to_cart() {
    global $product;

    // Enqueue variation scripts.
    wp_enqueue_script( 'wc-add-to-cart-variation' );

    // Get Available variations?
    $get_variations = count( $product->get_children() ) <= apply_filters( 'woocommerce_ajax_variation_threshold', 30, $product );

    // Load the template.
    get_template_part('woocommerce/tickets-variable-add-to-cart', '', [
        'available_variations' => $get_variations ? $product->get_available_variations() : false,
        'attributes'           => $product->get_variation_attributes(),
        'selected_attributes'  => $product->get_default_attributes(),
    ]);
}


function vma_tickets_hide_related_products() {
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
}

function vma_tickets_template_add_to_cart_text() {
    return "Add to Order";
}

function vma_tickets_template_modal_order() {
    get_template_part('events/templates/order', 'modal');
}

function vma_tickets_template_show_cart_button() {
    $args['button_text'] = "Get Tickets";
    get_template_part('events/templates/button', 'show-cart', $args);
}

function vma_tickets_mini_cart( $args = array() ) {

    $defaults = array(
        'list_class' => '',
    );

    $args = wp_parse_args( $args, $defaults );

    get_template_part('events/templates/cart/mini-cart', '', $args);
}

function vma_tickets_cart_item_name($product_name, $cart_item, $cart_item_key) {
    $product = $cart_item['data'];
    $product_type = $product->get_type();
    if ($product_type === 'simple') {
        return 'General Ticket(s)';
    }
    elseif ($product_type === 'variation') {
        $attr_name = $cart_item['variation']['attribute_pa_membership'];
        return ucfirst($attr_name) . ' Ticket';
    }
    return $product_name;
}

function vma_tickets_cart_item_thumbnail($thumb, $cart_item, $cart_item_key) {
    return '';
}