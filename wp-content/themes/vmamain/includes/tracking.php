<?php

function write_contact_page_script() {
    ?>
        
<script type="text/javascript">
(function() {
    function gtag_report_conversion(url) {
        var callback = function () {
            if (typeof(url) != 'undefined') {
            window.location = url;
            }
        };
        gtag('event', 'conversion', {
            'send_to': 'AW-804775729/95F6CKrDwYIBELHO3_8C',
            'event_callback': callback
        });
        return false;
    }

    var wpcf7Elm = document.querySelector( '.wpcf7-form' );
    
    wpcf7Elm.addEventListener( 'wpcf7submit', function( event ) {
        gtag_report_conversion();
    }, false );
    
    
    function setCookie(name, value, days){
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000)); 
        var expires = "; expires=" + date.toGMTString();
        document.cookie = name + "=" + value + expires + ";path=/";
    }
    function getParam(p){
        var match = RegExp('[?&]' + p + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }

    
    function readCookie(name) { 
    var n = name + "="; 
    var cookie = document.cookie.split(';'); 
    for(var i=0;i < cookie.length;i++) {      
        var c = cookie[i];      
        while (c.charAt(0)==' '){c = c.substring(1,c.length);}      
        if (c.indexOf(n) == 0){return c.substring(n.length,c.length);} 
    } 
    return null; 
    } 

    var gclid = getParam('gclid');
    if(gclid){
        var gclsrc = getParam('gclsrc');
        if(!gclsrc || gclsrc.indexOf('aw') !== -1){
            setCookie('gclid', gclid, 90);
        }
    }       
    
    var msclkid = getParam('msclkid');
    if(msclkid){
        setCookie('msclkid', msclkid, 90);
    }

    document.addEventListener("DOMContentLoaded", function(event) {
        wpcf7Elm.elements['gclid'].value = readCookie('gclid');
        wpcf7Elm.elements['msclkid'].value = readCookie('msclkid');
    });
})();
</script>
    
    
<?php
}

function write_adwords_global_script() {

    ?>

    <!-- Google AdWords Global site tag -->
    <script type="text/javascript">
        if(typeof gtag === 'function') {
            gtag('config', 'AW-804775729');
        }

    </script>

    <?php
}




add_action('wp_head', function() {
    write_adwords_global_script();

    // this test has to happen in 'wp_head' hook for some reason
    if(page_has_contact_form()) {
        add_action('wp_footer', function() {
            write_contact_page_script();
        });
    }
});