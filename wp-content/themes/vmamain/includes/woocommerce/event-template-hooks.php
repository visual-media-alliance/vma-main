<?php
/**
 * These hooks are loaded only when the current post in loop is an event product
 */


/** add markup for the order modal */
add_action('woocommerce_single_product_summary', 'vma_tickets_template_modal_order');

/**
 * After Single Products Summary Div.
 *
 * @see woocommerce_output_related_products()
 */
add_action('woocommerce_after_single_product_summary', 'vma_tickets_hide_related_products', 0);

/**
 * Replace add to cart button text
 */
add_action('woocommerce_product_single_add_to_cart_text', 'vma_tickets_template_add_to_cart_text', 0);

/**
 * Remove display of product categories and tags. This also removes the sku
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );


/**
 * Remove Add to cart from product summaery div
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

add_action('woocommerce_single_product_summary', 'vma_tickets_template_show_cart_button');
//add_action('woocommerce_before_single_product', 'vma_tickets_template_modal_order');
//add_action('woocommerce_after_single_product_summary', 'vma_tickets_template_order_bar', 2);
//add_action('woocommerce_after_single_product_summary', 'woocommerce_mini_cart', 2);

/**
 * MiniCart
 */
add_filter('woocommerce_cart_item_name', 'vma_tickets_cart_item_name', 10, 3);
add_filter('woocommerce_cart_item_thumbnail', 'vma_tickets_cart_item_thumbnail', 10, 3);
remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart', 10 );