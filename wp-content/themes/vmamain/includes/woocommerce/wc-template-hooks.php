<?php
/**
 * Overides for WooCommerce Template Hooks
 *
 * Action/filter hooks used for WooCommerce functions/templates.
 *
 * @package WooCommerce\Templates
 * @version 2.1.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Content Wrappers.
 *
 * @see woocommerce_output_content_wrapper()
 * @see woocommerce_output_content_wrapper_end()
 */
// remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
// remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );


/**
 * Breadcrumbs.
 *
 * @see woocommerce_breadcrumb()
 */

remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

/**
 * Hero Section
 * 
 * @see vma_hero_section()
 */
// add_action( 'woocommerce_before_main_content', 'vma_hero_section');

/**
 * sidebar
 */
remove_all_actions('woocommerce_sidebar');