<!-- <div>
<?php if ('open' == $post->comment_status) : ?> 
<div id="respond"> 
<h3 class="text-bold"><?php comment_form_title( 'Leave a Reply', 'Leave a Reply to %s' ); ?></h3> 
<div class="cancel-comment-reply">
    <small><?php cancel_comment_reply_link(); ?></small>
</div> 
<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
<p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>">logged in</a> to post a comment.</p>
<?php else : ?> 
<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform"> 
<?php if ( $user_ID ) : ?> 
<p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Log out &raquo;</a></p>
<?php else : ?> 
<p>
	<label for="author"><small>Name <?php if ($req) echo "*"; ?></small></label>
	<input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" class="form-control" required="required" />
</p> 
<p>
	<label for="email"><small>Email Address (will not be published) <?php if ($req) echo "*"; ?></small></label>
	<input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" class="form-control" required="required" />
</p> 
<p>
	<label for="url"><small>Website</small></label>
	<input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="22" tabindex="3" class="form-control" />
</p> 
<?php endif; ?> 
<p>
	<label><small>Comment</small></label>
	<textarea name="comment" id="comment" cols="100%" rows="10" tabindex="4" class="form-control" ></textarea></p>
 
<p><input name="submit" type="submit" id="submit" tabindex="5" value="Submit Comment" class="button button-primary bg-red" />
<?php comment_id_fields(); ?>
</p>
<?php do_action('comment_form', $post->ID); ?>
 
</form>
 
<?php endif; // If registration required and not logged in ?>
</div>
<?php endif; ?>
</div> -->

<div id="comments" class="comments-area">
	<?php if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php
				printf( _nx( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'twentyfifteen' ),
					number_format_i18n( get_comments_number() ), get_the_title() );
			?>
		</h2>
		<div class="comment-list">
			<?php
				wp_list_comments( array(
					'style'       => 'div',
					'short_ping'  => true,
					'avatar_size' => 32,
				) );
			?>
		</div><!-- .comment-list -->
	<?php endif; // have_comments() ?>

	<?php if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'twentyfifteen' ); ?></p>
	<?php endif; ?>


	<?php if ('open' == $post->comment_status) : ?> 
	<div id="respond"> 
	<h3 class="text-bold"><?php comment_form_title( 'Leave a Reply', 'Leave a Reply to %s' ); ?></h3> 
	<div class="cancel-comment-reply">
	    <small><?php cancel_comment_reply_link(); ?></small>
	</div> 
	<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
	<p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>">logged in</a> to post a comment.</p>
	<?php else : ?> 
	<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform"> 
	<?php if ( $user_ID ) : ?> 
	<p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Log out &raquo;</a></p>
	<?php else : ?> 
	<p>
		<label for="author"><small>Name <?php if ($req) echo "*"; ?></small></label>
		<input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" class="form-control" required="required" />
	</p> 
	<p>
		<label for="email"><small>Email Address (will not be published) <?php if ($req) echo "*"; ?></small></label>
		<input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" class="form-control" required="required" />
	</p> 
	<p>
		<label for="url"><small>Website</small></label>
		<input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="22" tabindex="3" class="form-control" />
	</p> 
	<?php endif; ?> 
	<p>
		<label><small>Comment</small></label>
		<textarea name="comment" id="comment" cols="100%" rows="10" tabindex="4" class="form-control" ></textarea></p>
	 
	<p><input name="submit" type="submit" id="submit" tabindex="5" value="Submit Comment" class="button button-primary bg-red" />
	<?php comment_id_fields(); ?>
	</p>
	<?php do_action('comment_form', $post->ID); ?>
	 
	</form>
	 
	<?php endif; // If registration required and not logged in ?>
	</div>
	<?php endif; ?>
</div><!-- .comments-area -->
