<?php
/**
 *
 * @package VMA-Main
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
<meta name="msapplication-square144x144logo" content="<?php echo get_template_directory_uri(); ?>/win8-tile-icon.png" />
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-icon-touch.png"/>
<link rel="icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
<title><?php
			if( is_home() ) {
				echo get_bloginfo( 'name' ) . ' | ' . get_bloginfo( 'description' );
			} else {
				if( wp_title( '', false ) === '' ) {
					echo get_bloginfo( 'name' );
				} else {
					echo wp_title( '', false ) . ' | ' .  get_bloginfo( 'name' );
				}
			}
		?></title>
<?php wp_head(); ?>
</head>
<body <?php body_class( grunt_wordpress_page_name() ); ?>>
<!--[if lt IE 9]><p class="browse-happy">You are using an <strong>outdated</strong> browser.<br/>Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a>.</p><div class="lt-ie9"><![endif]-->
<div class="animsition">
<header id="header"></header>
<!-- Main Container -->
<div class="main-container no-padding">