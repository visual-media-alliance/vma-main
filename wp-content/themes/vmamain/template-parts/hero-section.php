<?php
/**
 * Displays the Hero Image Title
 */

 ?>
<!-- HERO SECTION -->
<div class="section section-banner section-single-banner section-parallax" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri(); ?>/library/images/events-details-banner.jpg" data-bleed="0" data-position="top">
    <div class="container">
		<div class="row">
	        <div class="col-xs-12">            
				<div class="hero-block text-center event-block">
					<h1 class="h1 text-bold color-white fade-scroll"><?php the_title(); ?></h1>
						<?php if (false) : ?>
					<!--<div class="button-block fade-scroll"><a class="button button-normal button-orange uppercase animated-normal" href="<?php //echo get_sub_field('button_url'); ?>">Register <i class="icon icon-angle-right text-bold"></i></a></div>-->
						<?php endif; ?>
				</div>
	        </div>
			<div class="clear" ></div>
		</div>
    </div>
</div>
<!-- HERO SECTION -->