<?php
/**
 * Page template to display posts
 *
 * @package VMA-Education
 */

get_header(); ?> <?php
    
	switch( get_post_type( $post ) ) {

		case 'courses':
			   get_template_part('posts-templates/post', 'courses');
		break;
		

		// Default posts
		default:
			get_template_part('posts-templates/post', 'blog');
		break;

	}

?> <?php get_footer(); ?>