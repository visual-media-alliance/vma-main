<?php
/**
 * Theme functions file
 *
 * @package  VMA-Main
 * @author   Evan Bangham
 * @link     http://vma.bz
 *
 */

require __DIR__ . '/includes/tracking.php';
require __DIR__ . '/includes/template-functions.php';


function vma_theme_pre_activation_check($old_theme_name, WP_Theme $old_theme_object)
{
    try {
        if (version_compare(PHP_VERSION, '7.4', '<')) {
            throw new Exception(sprintf('Whoops, this theme requires %1$s version %2$s at least. Please upgrade %1$s to the latest version for better perfomance and security!', 'PHP', '7.4'));
        }

        if (version_compare($GLOBALS['wpdb']->db_version(), '5.0', '<')) {
            throw new Exception(sprintf('Whoops, this theme requires %1$s version %2$s at least. Please upgrade %1$s to the latest version for better perfomance and security.', 'MySQL', '5.6'));
        }

        if (version_compare($GLOBALS['wp_version'], '5.5', '<')) {
            throw new Exception(sprintf('Whoops, this theme requires %1$s version %2$s at least. Please upgrade %1$s to the latest version for better perfomance and security!', 'WordPress', '5.5'));
        }

        if (!defined('WP_CONTENT_DIR') || !is_writable(WP_CONTENT_DIR)) {
            throw new Exception('WordPress content directory is not writable. Please correct this directory permissions!');
        }

        // if (!is_child_theme()) {
        //     throw new Exception('This theme cannot be activated directly, please create a child theme');
        // }
    } catch (Exception $e) {
        $die_msg = sprintf('<h1 class="align-center">'.esc_html__('Theme Activation Error', 'anon').'</h1><p class="zoo-active-theme-error" >%s</p><p class="align-center"><a href="%s">'.esc_html__('Return to dashboard', 'anon').'</a></p>', $e->getMessage(), get_admin_url(null, 'index.php'));
        switch_theme($old_theme_object->stylesheet);
        wp_die($die_msg, esc_html__('Theme Activation Error', 'vma'), 500);
    }
}
add_action('after_switch_theme', 'vma_theme_pre_activation_check', 10, 2);


add_action('wp', function() {
    /** backfill acf functions with noops */
    if (!class_exists('ACF')) {
        function have_rows() {}
    }
}, 10000);



function grunt_setup_woocommerce() {

    /** always load the core hook overrides */
    require __DIR__ . '/includes/woocommerce/wc-template-hooks.php';

    /** conditionally include hook overrides when the post displayed is an event */
    if (class_exists('FooEvents') && class_exists('VmaEvents')) {

        /** This is needed for the modal add to cart component */
        add_action('vma_events_doing_event_ajax', function() {
            require __DIR__ . '/includes/woocommerce/event-template-hooks.php';
        });

        
        /**
         * Load up templates for single product view
         * 
         * @todo This is being called multiple times!!!!!
         *       Clearly not working as it's called both from the
         *       single-product template and the upsells template.
         * */

        // add_action('the_post', function($post) {
        //     global $product;
        //     if(!in_the_loop() || !$product) {
        //         return;
        //     }

        //     if(VMA_EVENTS()->isEvent($product)) {
        //         require __DIR__ . '/includes/woocommerce/event-template-hooks.php';

        //         wp_enqueue_script('vma-events-modal-add-to-cart',  get_template_directory_uri() . '/library/js/events/modal-add-to-cart.js?include', array(), '1.01', true );
        //     }
        // }, 15, 1);

    //     add_filter('woocommerce_template_loader_files', function($templates, $default_file) {
    //         if (!is_singular('product')) {
    //             return $templates;
    //         }

    //         $product = wc_get_product();

    //         if($product->get_meta('WooCommerceEventsEvent') === 'Event') {
    //             global $wp;
    //             if($wp->query_vars['vma-tickets'] ?? false) {
    //                 $templates[] =  'woocommerce/tickets.php';
    //             }
    //             else {
    //                 $templates[] =  'woocommerce/single-product-fooevent.php';
    //             }

    //         }

    //         return $templates;

    //     }, 10, 2);
    }
}

/* Get page name (template or post type) */
function grunt_wordpress_page_name() {  

	global $post;

	$pageTemplate = substr( basename( get_page_template() ), 0, -4 );

	if( !$pageTemplate ) {

		if( get_post_type( $post ) === 'post' ) {
			return 'post-blog';
		} else {
			return 'post-' . get_post_type( $post );
		}

	} else {
		return $pageTemplate;
	}

}

function page_has_contact_form(){
  $name = grunt_wordpress_page_name();

  if($name === 'page-application' || $name === 'page-flexible-landing-template') {
      wpcf7_enqueue_scripts();
      return true;
  }
  
  return false;
}

/* Enable RSS feed and navigation menu */
function grunt_wordpress_setup() {

	/* Add navigaiton menus support */
	register_nav_menus(array(
		'primary' => __( 'Primary Menu', 'grunt_wordpress_setup' ),
    ));

    add_theme_support('woocommerce');
    if (class_exists('WooCommerce')) {
        grunt_setup_woocommerce();
    }



} add_action( 'after_setup_theme', 'grunt_wordpress_setup' );

add_action('init', function() {
	
	add_filter('use_block_editor_for_post_type', 'prefix_disable_gutenberg', 10, 2);
	function prefix_disable_gutenberg($current_status, $post_type)
	{
		// Use your post type key instead of 'product'
		if ($post_type === 'post' || $post_type === 'page') return false;
		return $current_status;
	}
	
});


/* Enqueue scripts */
function grunt_wordpress_scripts() {
    $custom_css_theme_relative = 'library/css/custom.css';
    $custom_css_full = get_template_directory() . "/$custom_css_theme_relative";
    $custom_css_mtime = (string) filemtime($custom_css_full);
    
    $user_css_theme_relative = 'library/css/user.css';
    $user_css_full = get_template_directory() . "/$user_css_theme_relative";
    $user_css_mtime = (string) filemtime($user_css_full);
    
	/* CSS */
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/library/css/bootstrap.min.css?include', null, '3.3.6');
    //wp_enqueue_style( 'custom', $template .  '/library/css/custom.css?include', array(), $user_css_modified );
    wp_enqueue_style( 'custom', get_template_directory_uri() . "/$custom_css_theme_relative?include", array(), $custom_css_mtime );
    wp_enqueue_style( 'user', get_template_directory_uri() . "/$user_css_theme_relative?include", array(), $user_css_mtime );
    wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,600,700,800', false );

	/* JS */

	/* Deregister jQuery... */
    if(grunt_wordpress_page_name() != 'post-features')
    {
    	wp_deregister_script( 'jquery' );
    }
	/* ... To load it properly */
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/library/js/jquery.min.js?include', array(), '1.11.1', true );
	wp_enqueue_script( 'main', get_template_directory_uri() . '/library/js/main.min.js?include', array(), '1.01', true );
    wp_enqueue_script( 'slider', get_template_directory_uri() . '/library/js/slider.js?include', array(), '1.01', true );

	/* Scripts for specific pages */
	switch ( grunt_wordpress_page_name() ) {
		
	/* Pages */
	case 'page-home':
		wp_enqueue_script( 'page-home', get_template_directory_uri() . '/library/js/page-home.min.js?include', array(), '1.01', true );  
	break;

    case 'page-upcoming-events':
      wp_enqueue_script( 'page-events', get_template_directory_uri() . '/library/js/page-events.js?include', array(), '1.01', true );  
    break;

    case 'page-land':
      wp_enqueue_script( 'page-events', get_template_directory_uri() . '/library/js/page-events.js?include', array(), '1.01', true );  
    break;
    

    // case 'page-registration':
    //   wp_enqueue_script( 'page-registration', 'https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js', array(), '1.01', true );  
    // break;


	/* Posts types */
	case 'post-courses':
		wp_enqueue_script( 'page-class', get_template_directory_uri() . '/library/js/page-class.min.js', array(), '1.01', true );
    break;
	}

	wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/library/js/jquery-ui-1.10.3.min.js', array(), '1.10.3', true );
	
	if (is_post_type_archive('espresso_events')) {
        wp_enqueue_script( 'page-events', get_template_directory_uri() . '/library/js/page-events.js?include', array(), '1.01', true );
	}

} add_action( 'wp_enqueue_scripts', 'grunt_wordpress_scripts' );


/**
 * Add CSS class to Next/Previous Post link
 */

add_filter('next_post_link', 'post_link_attributes');
add_filter('previous_post_link', 'post_link_attributes');
 
function post_link_attributes($output) {
    $code = 'class="button button-normal color-orange text-semibold capitalize animated-normal"';
    return str_replace('<a href=', '<a '.$code.' href=', $output);
}

/**
 * Add automatic image sizes
 */
if ( function_exists( 'add_image_size' ) ) { 
	
	add_image_size( 'home-slider-img', 400, 265, true ); //(cropped)
}


// Remove Query Strings
function vma_remove_script_version( $src ){
  $parts = explode( '?ver', $src );
  return $parts[0];
}
add_filter( 'script_loader_src', 'vma_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', 'vma_remove_script_version', 15, 1 );
