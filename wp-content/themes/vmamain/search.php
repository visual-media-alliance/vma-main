<?php
/**
 * Template Name: Search List Page
 *
 * @package VMA-Insurance
 */
get_header();
?>
<!-- HERO SECTION -->
<div id="parallax" class="section section-banner section-parallax" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri(); ?>/library/images/photoshop-hero-bg.jpg" data-bleed="0" data-position="center" data-natural-width="1315" data-natural-height="450">
	<div class="container banner-container">
		<div class="col-xs-12">
			<div class="banner-block">
				<div class="col-xs-12 text-center">
					<h1 class="page-title text-bold color-white fade-scroll">Search Results</h1>
				</div>
				<div class="clear" ></div>
			</div>
		</div>
	</div>
</div>
<!-- HERO SECTION -->


<!-- SEARCH RESULTS -->
<div class="section section-search bg-grey" >
	<div class="container search-container">
		
		<div class="col-xs-12 col-sm-8">
			<?php if (have_posts()) : ?>
			<div class="col-xs-12">
				<div class="title-block">
					<h2 style="font-weight: 700;text-transform: capitalize;" class="section-title"><?php echo "Searched: " . get_search_query() ?></h2>
				</div>
			</div>
			<div class="col-xs-12 col-sm-9">
			
				<!-- Search for mobile -->
				<div class="search-block no-padding bg-white hidden-lg hidden-md hidden-sm">
	                <form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
	                    <div class="input-group">

	                        <input type="search" class="search-field form-control" placeholder="<?php echo esc_attr_x('Search ...', 'placeholder') ?>" value="<?php echo get_search_query(array('post_type')) ?>" name="s" title="<?php echo esc_attr_x('Search for:', 'label') ?>" />
	                        <input type="hidden" name="post_type" value="news" />
	                        <span class="input-group-btn">
	                            <button class="btn btn-default search-submit" type="submit"><?php echo esc_attr_x('Search', 'Go!') ?></button>
	                        </span>
	                        <div class="clear"></div>
	                    </div>
	                    <div class="clear"></div>
	                </form>
	                <!-- /input-group -->
	                <div class="clear"></div>
	            </div>
				<!-- Search for mobile -->
			
			<?php while (have_posts()) : the_post(); ?>
			<?php
			if (is_archive() || is_search()) : // Only display Excerpts for Search
			$cover_image = get_field("cover_image");
			?>
					<div class="news-block">
						<div class="news-photo">
							<a href="<?php the_permalink(); ?>">
								<img src="<?php echo $cover_image['url']; ?>" class="img-responsive" />
							</a>
						</div>
						<a href="<?php the_permalink(); ?>" class="title"><h2 class="section-title color-brown"><?php the_title(); ?></h2></a>

						<div class="clear"></div>

						<h3 class="color-brown"><span><?php echo get_the_date(); ?></span> - <span>BY <?php the_author(); ?></span></h3>
						<p><?php 
						$summary = get_field('news_description');
						$str = strip_tags($summary);
						echo substr($str, 0, 250);?>...</p>

						<!--<div class="social-share-button">
							<p class="uppercase">Share this on:</p>
							<div class="social-share-icons">
								<a href="http://www.facebook.com/share.php?u=<?php print(urlencode(get_permalink())); ?>&title=<?php print(urlencode(the_title())); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="facebook">Facebook</a>
								<a href="http://twitter.com/intent/tweet?status=<?php print(urlencode(the_title())); ?>+<?php print(urlencode(get_permalink())); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="twitter">Twitter</a>
								<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php print(urlencode(get_permalink())); ?>&title=<?php print(urlencode(the_title())); ?>&source=<?php echo $cover_image['url'] ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="linkedin">Linkedin</a>
								<a href="https://plus.google.com/share?url=<?php print(urlencode(get_permalink())); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="gplus">Google plus</a>
							</div>
						</div>-->
					</div>

					<p><?php the_excerpt(); ?></p>
				<?php endif; ?>

				<?php endwhile; ?>
			<div class="clear"></div>
			</div>

			<?php else:?>
			<div class="col-xs-12 col-sm-9">
		
			<!-- Search for mobile -->
			<div class="search-block no-padding bg-white hidden-lg hidden-md hidden-sm">
                <form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
                    <div class="input-group">

                        <input type="search" class="search-field form-control" placeholder="<?php echo esc_attr_x('Search ...', 'placeholder') ?>" value="<?php echo get_search_query(array('post_type')) ?>" name="s" title="<?php echo esc_attr_x('Search for:', 'label') ?>" />
                        <input type="hidden" name="post_type" value="news" />
                        <span class="input-group-btn">
                            <button class="btn btn-default search-submit" type="submit"><?php echo esc_attr_x('Search', 'Go!') ?></button>
                        </span>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </form>
                <!-- /input-group -->
                <div class="clear"></div>
            </div>
			<!-- Search for mobile -->
			
			<h1 class="entry-title"><?php _e( 'Not Found'); ?></h1>
			<div class="entry-content">
				<p><?php _e( 'Apologies, but no results were found for the requested keyword. Please try with different keywords.'); ?></p>

			</div><!-- .entry-content -->
			</div><!-- #post-0 -->
        	<?php endif;?> 
    	</div>

		<div class="col-xs-12 col-sm-4">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<!-- SEARCH RESULTS -->

<?php get_footer(); ?> 