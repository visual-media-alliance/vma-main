<?php get_header();?>

<!-- HERO SECTION -->
<?php
if (have_rows('hero_section', 564)):
while (have_rows('hero_section', 564)): the_row();
$banner_image = get_sub_field('background_image');
?>
<div id="parallax" class="section section-banner section-parallax" data-parallax="scroll" data-image-src="<?php echo $banner_image['url']; ?>" data-bleed="0" data-position="center">
	<div class="container banner-container">
		<div class="col-xs-12">
			<div class="banner-block">
				<div class="col-xs-12 text-center">
					<h1 class="page-title text-bold color-white fade-scroll"><?php echo get_sub_field('title'); ?>: <?php echo single_cat_title();?></h1>
				</div>
				<div class="clear" ></div>
			</div>
		</div>
	</div>
</div>
<?php endwhile; endif;?>
<!-- HERO SECTION -->

<!-- BLOG ARCHIVE SECTION -->
<div class="section section-blog-list" >
	<div class="container blog-container">
		<div class="col-xs-12 col-sm-8">
			<div class="blog-group">
				<!-- <div class="blog-category-title">
					<h3><?php echo single_cat_title();?></h3>
				</div> -->
<?php
$paged= get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$query=new WP_Query($query_string.'&post_type=blog');

if ( $query->have_posts() ) {
while ($query->have_posts() ) {
$query->the_post();

$featured_image = get_field("featured_image");
?>
				<div class="blog-block">
					<div class="img-block fill" style="background-image: url(<?php echo $featured_image['url']; ?>);">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/library/images/blog-pholder.png" class="img-responsive" alt=""/></a>
					</div>
					<div class="blog-content">
						<a href="<?php the_permalink(); ?>" class="animated-normal"><h3 class="text-bold"><?php the_title(); ?></h3></a>
						<span class="date block"><?php echo get_the_date('M d, Y', $post_id); ?></span>
						<p><?php $summary = get_field('paragraph_text');
						$str = strip_tags($summary);
						echo substr($str, 0, 172);?>...</p>
						<a href="<?php the_permalink(); ?>" class="button button-normal color-red text-semibold capitalize animated-normal" >Read More <i class="icon icon-angle-right"></i></a>
					</div>
				</div>
<?php }?>
				 <div class="pagination">
                    <?php
                    $big = 999999999; // need an unlikely integer
                    echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $the_query->max_num_pages,                        
					    'prev_text'    => '«',
					    'next_text'    => '»'
                    ) );?>
                </div>
<?php } ?>			
			</div>
		</div>

		<div class="col-xs-12 col-sm-4">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<!-- BLOG ARCHIVE SECTION -->

<?php get_footer(); ?>