<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package VMA-Main
 */

get_header(); ?>

	<div class="not-found-container">
		<div class="content-box">
			<h3>Sorry, page not found...</h3>
			<p>We apologize that the content you are looking for can not be found.</p>
			<a href="<?php echo get_site_url(); ?>"><button class="button button-primary button-orange horizontal-center"><i class="icon icon-angle-left text-bold"></i> Back to Home</button>
			</a>
		</div>
	</div>


</div>
<!-- Main Container Ends-->

<!--[if lt IE 9]></div><![endif]--><?php wp_footer(); ?>
</div>
</body>
</html>