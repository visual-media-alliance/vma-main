<?php
/**
 *
 * @package VMA-Main
 */
?>
<!-- PARTNERS & SPONSORS SECTION -->
<div class="section section-partner">
    <div class="container partner-container">
        <div class="row">
            <div class="col-xs-12">    
            <?php  if (have_rows('vma_sponsors', 4)):?>         
                <div class="partner-group">
                    <div class="partner-title-block inline-block">
                        <h4 class="text-bold">VMA Sponsors:</h4>
                    </div>
                    <?php while (have_rows('vma_sponsors', 4)): the_row();?>
                    <div class="partner-block inline-block v-center">
                        <a target="_blank" href="<?php the_sub_field('link_url');?>"><img src="<?php the_sub_field('image');?>" class="img-responsive" alt=""/></a>                    
                    </div>
                    <?php endwhile;?>
                </div>
            <?php endif;?>
            <?php  if (have_rows('vma_partners', 4)):?>   
                <div class="partner-group">
                    <div class="partner-title-block inline-block">
                        <h4 class="text-bold">VMA Partners:</h4>
                    </div>
                    <?php while (have_rows('vma_partners', 4)): the_row();?>
                    <div class="partner-block inline-block v-center">
                        <a target="_blank" href="<?php the_sub_field('link');?>"><img src="<?php the_sub_field('image');?>" class="img-responsive" alt=""/></a>                    
                    </div>
                    <?php endwhile;?>
                </div>
                 <?php endif;?>
            </div>
        </div>
    </div>
</div>
<!-- PARTNERS & SPONSORS SECTION -->
<!-- CTA SECTION -->
<div class="section section-cta bg-orange">
    <div class="container cta-container">
        <div class="col-xs-12 col-sm-8 text-right">
            <h2 class="color-white">Learn how we support your business</h2>
        </div>
        <div class="col-xs-12 col-sm-4 text-left">
            <a href="/who-we-are" class="button button-primary button-white" >Find out How <i class="icon icon-angle-right"></i></a>
        </div>
        <div class="clear" ></div>
    </div>
</div>
<!-- CTA SECTION -->
<footer>
    <div class="section section-footer">
        <div class="container">
            <div class="row" >
                <div class="col-xs-3 col-sm-3">
                    <h3 class="text-bold">Our Programs</h3>
                    <?php wp_nav_menu( array('menu' => 'Footer Menu 1', 'menu_class' => 'flinks')); ?>
                </div>
                <div class="col-xs-3 col-sm-3">
                   <h3 class="text-bold">About VMA</h3>
                    <?php wp_nav_menu( array('menu' => 'Footer Menu 2', 'menu_class' => 'flinks')); ?>
                </div>
                <div class="col-xs-6 col-sm-6">
                    <div class="contact-info">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="footer-logo">
                                    <img src="<?php echo get_template_directory_uri(); ?>/library/images/vma-footer-logo.png" width="436" height="129" alt="Visual Media Alliance"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <p class="text-bold">665 Third Street, Suite 500, <br />San Francisco, CA 94107</p>
                                <div class="social-icons hidden-xs">                        
                                    <a class="icon icon-facebook" href="https://www.facebook.com/VisualMediaAlliance" target="_blank"></a>
                                    <a class="icon icon-twitter" href="https://twitter.com/visualmediaall" target="_blank"></a>
                                    <a class="icon icon-linkedin" href="https://www.linkedin.com/in/visualmediaalliance" target="_blank"></a>   
                                  
                                    <a class="icon icon-youtube" href="https://www.youtube.com/channel/UCHXmmPqwdDpvKzmoN0NvLew/feed" target="_blank"></a>
                                    <a class="icon icon-flickr" href="https://www.flickr.com/photos/visualmediaalliance/albums" target="_blank"></a>   
                                    <!--<a class="icon icon-pinterest" href="https://www.pinterest.com/visualmediaall" target="_blank"></a>-->                             
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="contact-number">
                                    <p>Phone: <a href="tel:800.659.3363">800.659.3363</a></p>
                                    <p>Fax: 415.520.1126</p>
                                    <p>Email: <a href="mailto:info@vma.bz">info@vma.bz</a></p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 hidden-lg hidden-md hidden-sm">
                                <div class="social-icons">
                                    <a class="icon icon-facebook" href="https://www.facebook.com/VisualMediaAlliance" target="_blank"></a>
                                    <a class="icon icon-twitter" href="https://twitter.com/visualmediaall" target="_blank"></a>
                                    <a class="icon icon-linkedin" href="https://www.linkedin.com/in/visualmediaalliance" target="_blank"></a>     
                                    <a class="icon icon-youtube" href="https://www.youtube.com/channel/UCHXmmPqwdDpvKzmoN0NvLew/feed" target="_blank"></a>
                                    <a class="icon icon-flickr" href="https://www.flickr.com/photos/visualmediaalliance/albums" target="_blank"></a>   
                                    <!--<a class="icon icon-pinterest" href="https://www.pinterest.com/visualmediaall" target="_blank"></a>-->
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="copyright text-center">
                    <span>&copy; 1986-<?php echo date('Y'); ?> <?php echo get_bloginfo('name'); ?><!-- | <a href="<?php echo get_site_url(); ?>/privacy-policy">Privacy Policy</a>--></span>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<!-- Main Container Ends-->
<!--[if lt IE 9]></div><![endif]--><?php wp_footer(); ?>
</div>
</body>
</html>