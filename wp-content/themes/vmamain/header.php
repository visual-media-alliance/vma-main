<?php
/**
 *
 * @package VMA-Main
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MFJ4NLC');</script>
<!-- End Google Tag Manager -->
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
<meta name="msapplication-square144x144logo" content="<?php echo get_template_directory_uri(); ?>/win8-tile-icon.png" />
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-icon-touch.png"/>
<link rel="icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
<title><?php
			if( is_home() ) {
				echo get_bloginfo( 'name' ) . ' | ' . get_bloginfo( 'description' );
			} else {
				if( wp_title( '', false ) === '' ) {
					echo get_bloginfo( 'name' );
				} else {
					echo wp_title( '', false ) . ' | ' .  get_bloginfo( 'name' );
				}
			}
		?></title>
<?php wp_head(); ?>
</head>
<body <?php body_class( grunt_wordpress_page_name() ); ?>>
<?php wp_body_open(); ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MFJ4NLC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--[if lt IE 9]><p class="browse-happy">You are using an <strong>outdated</strong> browser.<br/>Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a>.</p><div class="lt-ie9"></div><![endif]-->
<!-- Header -->
<header id="header">
<!-- Top -->
	<div class="top hidden-xs">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-10 no-padding-right">
					<div class="top-left-block">
						<?php wp_nav_menu( array('menu' => 'Top Left Menu', 'menu_class' => 'top-left-menu')); ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-2 no-padding-left"></div>
			</div>
		</div>
	</div>
<!-- Top -->
<!-- Logo & Nav Banner -->
	<div class="logonav">
		<div class="container relative">
			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<a href="<?php echo get_site_url(); ?>/" class="logo-link">
						<div class="logo"></div>
					</a>
					<div class="clear"></div>
				</div>			
				<div class="col-xs-12 col-sm-8 no-padding" >
					<div role="navigation" class="navbar navbar-default desktop-nav">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle animated-normal" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only animated-normal">Toggle navigation</span> <span class="icon-bar animated-normal"></span> <span class="icon-bar animated-normal"></span> <span class="icon-bar animated-normal"></span>
							</button>
						</div>
						<div class="navbar-collapse collapse hidden-xs">
							<?php wp_nav_menu( array('container' => 'navbar-collapse collapse', 'menu' => 'Main Menu', 'menu_class' => 'nav navbar-nav')); ?>
							<div class="search">
								<a href="#toggle-search" class="animated-normal"><i class="icon icon-search"></i></a>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="navbar navbar-default mobile-nav hidden-lg hidden-md hidden-sm" >
						<div class="navbar-collapse collapse" >						
							<div class="scroller relative">							
								<div class="top-close-block">
									<button type="button" class="close-btn" data-toggle="collapse" data-target=".navbar-collapse">
										<span class="icon-bar top-bar animated-normal"></span>
										<span class="icon-bar bottom-bar animated-normal"></span>
									</button>
									<div class="clear"></div>
								</div>
								<div class="relative">
									<!-- Search Section -->
									<div class="vma-search">
										<div class="container">
											<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
												<div class="input-group">
													<input class="form-control" type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="Search" />
													<span class="input-group-btn">
														<button class="btn btn-danger bg-orange" type="submit"><i class="icon icon-search"></i></button>
													</span>
												</div>
											</form>
										</div>
									</div>
									<!-- Search Section -->
								</div>
								<?php wp_nav_menu( array('menu' => 'Main Menu', 'menu_class' => 'nav navbar-nav')); ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>				
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<!-- Logo & Nav Banner -->
	<!-- Search Section -->
	<div class="vma-search hidden-xs">
		<div class="container">
			<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<div class="input-group">
					<input class="form-control" type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="Search for services and hit enter" />
					<span class="input-group-btn close-group-btn">
							<button class="btn btn-danger bg-orange" type="reset"><i class="icon icon-close"></i><!--<img src="<?php echo get_template_directory_uri(); ?>/library/images/close-btn.png" class="img-responsive">--></button>
					</span>
				</div>
			</form>
		</div>
	</div>
	<!-- Search Section -->
</header>
<!-- Header -->
<!-- Main Container -->
<div class="main-container no-padding">