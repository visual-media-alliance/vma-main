<?php
/**
 * Plugin Name: WP local development env setup
 * Description: Adds dev features when wp env is set to local 
 */

/**
 * Bail out if this is not a test or production env
 */
if (!in_array(wp_get_environment_type(), ['development', 'local'])) {
    return;
}


/**
 * Hook wp_mail() to Use mailhog in local dev environment 
 * 
 * Setup the local environment to use the mailhog configuration
 * provided in the vsocde .devcontainer setup
 */
add_filter('wp_mail', function($args) {
    $args['headers'] = ['From: Word Press <wordpress@devenv.local'];
    return $args;
});
add_action('phpmailer_init', function ($phpmailer) {
    /**
     * wp_mail() reconfigures phpmailer on every call,
     * set it back to what we want here in this hook
     * 
     */
    if (wp_get_environment_type() === 'local') {
        $phpmailer->Host = 'localhost';
        $phpmailer->Port = 1025;
        $phpmailer->IsSMTP();
    }

    return $phpmailer;
});


ini_set('display_errors', 1);
ini_set('html_errors', true);
ini_set('log_errors', 'stderr');
ini_set('log_errors_max_len', 0);
ini_set('zend.exception_ignore_args', 0);

if (defined('WP_CLI') && WP_CLI) {
    ini_set('html_errors', false);
    ini_set('display_errors', 'stderr');
    ini_set('log_errors', false);
}

/** Some plugins rely on this */
$_SERVER['SERVER_NAME'] ??= 'localhost';

require __DIR__ . '/wp-local-env/ErrorHandler.php';
$errorhandling = new WpLocal\ErrorHandler();
$errorhandling->register();

// Disable WordPress auto updates
if( ! defined('WP_AUTO_UPDATE_CORE')) {
    define( 'WP_AUTO_UPDATE_CORE', false );
}

/**
 * If this is a pantheon site, disable core updates as this
 * will interfere with their upstream build system.
 */
if (
    file_exists(ABSPATH . '/pantheon.upstream.yml') &&
    file_exists(ABSPATH . '/wp-config-pantheon.php')
) {
    remove_action( 'wp_maybe_auto_update', 'wp_maybe_auto_update' );
    // Remove the default WordPress core update nag
    add_action('admin_menu', function() {
        remove_action( 'admin_notices', 'update_nag', 3 );
    });
    
    remove_action('admin_init', '_maybe_update_core');
    
    add_filter('pre_site_transient_update_core', function() {
        include ABSPATH . WPINC . '/version.php';
        return (object) array(
            'updates' => array(),
            'version_checked' => $wp_version,
            'last_checked' => time(),
        );
    });
}

add_action('wp_enqueue_scripts', function() {
    wp_enqueue_script( 'vma-dev-tools', WPMU_PLUGIN_URL . '/wp-local-env/devtools.js');
});