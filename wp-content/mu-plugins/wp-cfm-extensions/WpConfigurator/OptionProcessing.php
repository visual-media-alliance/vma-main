<?php
namespace etherealite\WpConfigurator;

class OptionProcessing {

    // private array $materializedTecDbOptionsSplit;
    // private array $materializedTecDbOptionsLitteral;
    private const SEPARATOR = '/';

    private array $projectedSplits;


    public function __construct()
    {
        add_filter('wpcfm_configuration_items', [$this, 'wpcfm_configuration_items_filter']);
        add_filter('wpcfm_pull_callback', [$this, 'pull_callback'], 10, 2);
    }


    private function specifiedSplits()
    {
        return [
            'tribe_events_calendar_options' => [
                'prefix' => 'tec',
                'keyMask' => [
                    'schema-version',
                    'previous_ecp_versions',
                    'latest_ecp_version',
                    'pro-schema-version',
                    'event-tickets-schema-version',
                    'previous_event_tickets_plus_versions',
                    'latest_event_tickets_plus_version',
                    'previous_event_tickets_versions',
                    'latest_event_tickets_version',
                    'last-update-message-event-tickets',
                    'event-tickets-plus-schema-version',
                    'earliest_date',
                    'earliest_date_markers',
                    'latest_date',
                    'latest_date_markers',    
                ],
            ],
            'disable_comments_options' => [
                'prefix' => 'dsc',
                'keyMask' => [
                    'db_version',
                ],
            ],
        ];
    }


    public function wpcfm_configuration_items_filter($items)
    {
        return $this->projectSplits($items);
    }

    
    public function pull_callback($callback, $callback_params) 
    {
        $keyspace = substr($callback_params['name'], 0, 4);

        if (in_array($keyspace, ['tec/'])) {
            return [$this, 'saveSplitValue'];
        }

        return $callback;
    }


    /**
     * Destructure serialized Wordpress option values into 
     * their own separate settings.
     */
    private function projectSplits($items)
    {

        $foundSubjects = [];
        foreach ($this->subjectOptionNames() as $optionName) {
            if (in_array($optionName, array_keys($items))) {
                $foundSubjects[] = $optionName;
            }
        }

        $projectedSplits = [];
        foreach ($foundSubjects as $foundSubject) {
            $prefix = $this->splitKeyPrefix($foundSubject);
            $keyMask = $this->splitKeyMask($foundSubject);
            $item = $items[$foundSubject];
            $value = maybe_unserialize($item['value']);

            if ($value === false) {
                $item['error'] = $this->decorateMsg("couldn't unserialze option value");
                return $item;
            }
    
            $projectedSplits = [];
            foreach ($value as $key => $val) {
                
                if (!in_array($key, $keyMask)) {
                    $projection = [
                        'value' => json_encode($val),
                        'label' => $key,
                        'group' => 'The Events Calendar Options',
                    ];
                    
                    $prefixedKey = $prefix . self::SEPARATOR . $key;
                    $items[$prefixedKey] = $projection;
                    $projectedSplits[$foundSubject][$prefixedKey] = $projection;
                }
    
            }
        }

        $this->projectedSplits = $projectedSplits;

        return $items;
    }


    private function subjectOptionNames()
    {
        return array_keys($this->specifiedSplits());
    }


    private function splitKeyMask($optionName)
    {
        return $this->specifiedSplits()[$optionName]['keyMask'];
    }


    private function splitKeyPrefix($optionName)
    {
        return $this->specifiedSplits()[$optionName]['prefix'];
    }


    /**
     * Idempotent function Will prefetech other split values and
     * materialize them. Furthers calls to this method will not be saved
     * unless the materiazed version would be changed.
     * @param string $params['name']
     * @param string $params['group']
     * @param string $params['old_value'] The old settings (DB)
     * @param string $params['new_value'] The new settings (file)
     */
    private function saveSplitValue($params) 
    {
        $splitOptionKey = $params['name'];
        $splitOptionValue = $params['new_value'];

        if ($this->willChangeMaterialzed()) {

        }
    }


    private function prefetchSplitOption($splitName)
    {
        $optionValue = get_option($foundName);
    }


    private function prefixes()
    {
        foreach ($this->splits() as $optionName => $split) {
            if ($split['prefix'] === $splitName) {
                $foundName = $split['splitName'];
                
            }
        }
    }


    private function decorateMsg($msg)
    {
        $class = get_class($this);

        $path = __FILE__;

        $decorated =  "The class $class in $path: $msg";

        throw new \Exception($decorated);
        return $decorated;
    }

}