<?php
require __DIR__ . '/vendor/autoload.php';
use WP_CLI\Formatter;

/**
 * List all database options keys not under WP-CFM file tracking.
 *
 * ## OPTIONS
 * 
 * [--reverse]
 * : Invert the process to check for WP-CFM file tracked options not in database.
 * 
 * [--delete]
 * : Delete all the orphaned options from the database
 *
 * [--format=<format>]
 * : Render output in a particular format.
 * ---
 * default: table
 * options:
 *   - table
 *   - csv
 *   - json
 *   - count
 *   - yaml
 * ---
 * 
 * ## EXAMPLES
 *
 * wp config orphans
 *
 * @synopsis
 *
 */
function wpcfm_cli_orphans($args, $assoc_args) {
    
    /** Get all safe settings table keys currently in DB, filter out taxonomies */
    $db_items = array_filter(WPCFM()->registry->get_configuration_items(), function($item) {
        $group = $item['group'] ?? null;
        return $group !== 'Taxonomy Terms';
    });


    /** Get all bundle file settings keys, filter taxonomies */
    $bundle_configs = [];
    foreach (WPCFM()->helper->get_file_bundles() as $bundle) {
        if ($bundle['label'] === 'Taxonomies') {
            continue;
        }
        $bundle_configs[] = $bundle['config'];
    }

    $file_items = array_merge(...$bundle_configs);

    $dbKeys = array_keys($db_items);
    $fileKeys = array_keys($file_items);


    $assoc_args['reverse'] ??= false;
    $assoc_args['delete'] ??= false;

    if ($assoc_args['reverse'] && $assoc_args['delete']) {
        WP_CLI::error("You cannot combine [--delete] with other flags");
        return;
    }

    $diff = $assoc_args['reverse'] ? array_diff($fileKeys, $dbKeys) : 
        array_diff($dbKeys, $fileKeys);

    if($assoc_args['delete']) {
        foreach($diff as $dbKey) {
            delete_option($dbKey);
        }
        
        WP_CLI::success("Orphan keys have been deleted");
        return;
    }



    $diff = array_map(function($item) use($db_items, $file_items) {
        return [
            'option_name' => $item,
            'db_value' => $db_items[$item]['value'] ?? '',
            'file_value' => $file_items[$item]['value'] ?? '',
        ];
    }, $diff);

    if ($diff) {
        $formatter = new Formatter($assoc_args, [
            'option_name', $assoc_args['reverse'] ? 'file_value' : 'db_value',
        ]);
        $formatter->display_items($diff);
    }
}


/**
 * Manage config bundles
 *
 * ## OPTIONS
 *
 * <bundle_name>
 * : The bundle name to oporate on (or use "all")
 *
 * [--delete]
 * : Remove the bundle from the database and the filesystem.
 * 
 * [--show]
 * : List bundle(s) and their key/values
 * 
 * ## EXAMPLES
 *
 * wp config bundle bundle_name --delete
 *
 * @synopsis <bundle_name> [--delete] [--show]
 *
 */
function wpcfm_cli_bundle($args, $assoc_args) {

    $target = $args[0];

    $all = stripos($target, 'all') !== false;

    if (
        isset($assoc_args['delete']) &&
        isset($assoc_args['show'])
    ) {
        WP_CLI::error("the [--delete] and other flags are mutally exclusive");
    }

    if (!$assoc_args || $assoc_args['show'] ?? null === 1) {
        if ($all) {
            (new WPCFM_CLI_Command())->bundles();
        }
        else {
            (new WPCFM_CLI_Command())->show_bundle([$target], []);
        }
    }
}


function wpcfm_cli_diff( $args, $assoc_args ) {
    if ($args[0] ?? null === null) {
        $args[0] = 'all';
    }


    $compare = WPCFM()->readwrite->compare_bundle( $args[0] );

    if ($compare['error'] !== '') {
        WP_CLI::warning( $compare['error'] );
    }
    else {
        # Sort these things into stuff that's only in one place,
        # or where there's actually a diff.
        $only_db_rows = array();
        $only_file_rows = array();
        $diff_rows = array();
        foreach( $compare['db'] as $key => $value ) {
            if ( !isset( $compare['file'][$key] ) ) {
                $only_db_rows[] = array($key, $value);
            }
            elseif ( $value !== $compare['file'][$key] ) {
                $diff_rows[$key] = array( $key, $compare['file'][$key], $value );
            }
        }
        foreach( $compare['file'] as $key => $value ) {
            if ( !isset( $compare['db'][$key] ) ) {
                $only_file_rows[] = array( $key, $value );
            }
            elseif ( $value !== $compare['db'][$key] ) {
                $diff_rows[$key] = array( $key, $compare['db'][$key], $value );
            }
        }
        if ( count( $only_file_rows) > 0 ) {
            $file = new \cli\Table( array( 'Option', 'Value' ), $only_file_rows);
            WP_CLI::line( 'Options that are only in files (pull to load)' );
            $file->display();
        }
        if ( count( $only_db_rows) > 0 ) {
            $db = new \cli\Table( array( 'Option', 'Value' ), $only_db_rows);
            WP_CLI::line( 'Options that are only in DB (push to write to file)' );
            $db->display();
        }
        if ( count( $diff_rows ) > 0 ) {

            $realDiffs = array_map(function($item) {

                
                $dbVal = maybe_unserialize($item[1]) ?? $item[1];
                $fileVal = maybe_unserialize($item[2]) ?? $item[2];

                if (is_array($dbVal) && is_array($fileVal)) {
                    $dbVal = json_encode($dbVal, JSON_PRETTY_PRINT);
                    $fileVal =json_encode($fileVal, JSON_PRETTY_PRINT);
                }


                $differ = new SebastianBergmann\Diff\Differ;
                $diff =  $differ->diff($dbVal, $fileVal);
                return [
                    $item[0],
                    $diff,
                ];
            }, $diff_rows);

            
            // $diff = new \cli\Table( array( 'Option', 'DB value', 'File value' ), $diff_rows);
            WP_CLI::line( 'Options in both the database and in files.' );
            // $diff->display();
            // $realDiffTable = new \cli\Table(['Option', 'Diff From DB'], $realDiffs);
            // $realDiffTable->display();
            print_r($realDiffs);
        }
    }
}


/**
 * Initialize config bundles from json
 *
 * ## OPTIONS
 *
 * <bundle_name>
 * : The bundle name to init on (or use "all")
 *
 * ## EXAMPLES
 *
 * wp config init bundle_name
 *
 * @synopsis <bundle_name> [--delete] [--show]
 *
 */
// function wpcfm_cli_init($args, $assoc_args) {
//     $wpcfm_option = json_decode(get_option('wpcfm_settings'), true);
//     $file_bundles = WPCFM()->helper->get_file_bundles();

//     $initialized = [];
//     foreach($wpcfm_option['bundles'] ?? [] as $bundle) {
//         $initialized[$bundle['name']] = $bundle;
//     }

//     foreach ($args as $name) {
//         if (isset($initialized[$name])) {
//             WP_CLI::error("The bundle $name is already initialized");
//         }
//         $file = $file_bundles[$name] ?? [];
//         if ($file) {
//             $initialized[$name] = (object)[];
//         }
//     }

//     $wpcfm_option['bundles'] = $initialized;
// }