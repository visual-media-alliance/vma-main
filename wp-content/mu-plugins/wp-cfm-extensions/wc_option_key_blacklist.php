<?php
/**
 * This list created based on analysis of:
 *  - woocommerce version: 4.6.0
 *  - woocommerce_db_version 4.6.0
 *  - woocommerce_schema_version: 430
 * 
 * @info PackagesEffected
 *  This notation includes packages that are effected with a high level direction.
 *  Indrect effects by these configuration options are not considered.
 */

return [
    /** 
     * State of the action schedular table migrations
     * 
     * packagesEffected:
     *  - Action_Scheduler
     */
    'action_scheduler_migration_status',

    /**
     * store separator for hybrid stores
     * 
     * packagesEffected:
     *  - Action_Scheduler
     */

    'action_scheduler_hybrid_store_demarkation',
    /** 
     * Locks adding new job execution by process 
     * 
     * packagesEffected:
     *  - Action_Scheduler
     */
    'action_scheduler_lock_async-request-runner',

    /**
     * Default product category id
     * 
     * This defaults to 'Uncategorized', which the installer
     * provides by default.
     */
    'default_product_cat',

    /**
     * Cached copy of product category table relationships
     */
    'product_cat_children',

    /**
     * Action schedular log table version
     * 
     * packagesEffected:
     *  - Action_Scheduler\Migration
     */
    'schema-ActionScheduler_LoggerSchema',

    /**
     * Action schedular store tables version
     * 
     * packagesEffected:
     *  - Action_Scheduler\Migration
     */
    'schema-ActionScheduler_StoreSchema',
    
    /**
     * timer for basing when to show a feedback form in admin
     * 
     * packagesEffected
     *  - WooCommerce\Admin
     */
    'wc_admin_note_home_screen_feedback_homescreen_accessed',

    /**
     * Current the Wocommerce installed blocks version
     * 
     * packagesEffected
     *  - WooCommerce/Blocks
     */
    'wc_blocks_db_schema_version',

    /**
     * remotely fetched instructions for remote admin notices
     * 
     * packagesEffected
     *  - WooCommerce\Admin
     */
    'wc_remote_inbox_notifications_specs',

    /**
     * Tracks state changes in woomerce for remote admin notices
     * 
     * packagesEffected
     *  - WooCommerce\Admin
     */
    'wc_remote_inbox_notifications_stored_state',

    /** 
     * Records time that current woocommerce admin package was installed.
     * 
     * packagesEffected:
     *  - WooCommerce\Admin
     */
    'woocommerce_admin_install_timestamp',

    /**
     * Counts orders made for admin notice generation.
     * 
     * packagesEffected:
     *  - WooCommerce\Admin
     */
    'woocommerce_admin_last_orders_milestone',

    /** 
     * Collection of active admin notices 
     * 
     * packagesEffected:
     *  - WooCommerce\Admin
     */
    'woocommerce_admin_notices',
    
    /**
     * Records woocommerce admin package version
     * 
     * packagesEffected:
     *  - WooCommerce\Admin
     */
    'woocommerce_admin_version',

    /**
     * Version of the Active woocommerce db schema version
     * 
     * This version is set by migrations scripts that mix both
     * DML and DDL database mutations.
     */
    'woocommerce_db_version',

    /**
     * Stores form submission errors
     * 
     * packagesEffected:
     *  - WooCommerce\Admin\Meta Boxes
     *  - WooCommerce\Classes
     */
    'woocommerce_meta_box_errors',

    /**
     * Should woocommerce flush rewrite rules (during or after request?)
     * 
     * packagesEffected:
     *  - WooCommerce\Classes\Products
     *  - WooCommerce\Admin
     */
    'woocommerce_queue_flush_rewrite_rules',

    /**
     * packagesEffected:
     *  - WooCommerce\Classes
     */
    'woocommerce_maybe_regenerate_images_hash',

    /**
     * Admin note for sales record sales ammount
     * 
     * packagesEffected:
     *  - WooCommerce\Admin
     */
    'woocommerce_sales_record_amount',

    /**
     * Admin note for sales record, record date
     * 
     * packagesEffected:
     *  - WooCommerce\Admin
     */
    'woocommerce_sales_record_date',

    /**
     * Version of the Active woocommerce base tables version
     * 
     * This is set by migrations that seem to perform only DDL type migrations
     * and probably no DML types
     * 
     * packagesEffected:
     *  - WooCommerce\Functions
     *  - WooCommerce\Classes
     */
    'woocommerce_schema_version',

    /**
     * Version of the installed (activated?) version of woocommerce
     * 
     * packagesEffected:
     *  - WooCommerce
     *  - WooCommerce\Admin
     */
    'woocommerce_version',
];