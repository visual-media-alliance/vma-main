<?php
/**
 * options table keys for various plugins
 */

$keys = [];

if (function_exists('Pantheon_Sessions')) {
    $keys = [
        /** 
         * Plugin: wp-native-php-sessions
         * Version: 1.2.1
         * 
         * Schema migration version for the session table
         */
        'pantheon_session_version',

        ...$keys,
    ];

}
if (class_exists('EE_Addon')){
    $keys = [

        ...$keys,
    ];
}
if (false) {
    $keys = [
        /**
         * The Events Calendar by Tribe
         */
        'tribe_last_generate_rewrite_rules',
        'tribe_last_save_post',
        'tribe_last_updated_option',
        'external_updates-event-tickets-plus',
        'external_updates-events-calendar-pro',
        'tribe_pue_key_notices',
        'tribe_last_event_tickets_after_create_ticket',

        ...$keys,
    ];
}
if (false) {
    $keys = [
        /**
         * Plugin: 
         * Plugin URI: http://www.woocommerce.com/products/authorize-net-cim/
         * Version: 3.2.7
         */
        'wc_authorize_net_cim_version',
        'wc_authorize_net_cim_lifecycle_events',

        ...$keys,
    ];
}
return [


    /**
     * Freemius shared lib
     * 
     * This is not a plugin but a libraray used by many plugins.
     */
    'fs_accounts',
    'fs_active_plugins',
    'fs_debug_mode',
    'fs_gdpr',

    /**
     * Plugin: wp-cfm
     * Plugin URI: https://github.com/forumone/wp-cfm
     * Version: 1.6-evanpatched
     */
    'wpcfm_settings',

    ...$keys,
];