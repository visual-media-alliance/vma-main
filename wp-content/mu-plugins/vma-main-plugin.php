<?php
/*
  Plugin Name: VMA Main
  Plugin URI: http://vma.gov
  Description: VMA Main Core Plugin, required by VMA Main Theme.
  Version: 0.1
  Author: Evan Bangham
  Author URI: https://github.com/etherealite
*/

CONST ADMIN_MAINTENANCE = false;

/**
 * register custom post types
 */
add_action( 'init', function() {
  register_post_type( 'testimonial',
    array(
      'labels' => array(
        'name' => __( 'Testimonials' ),
        'singular_name' => __( 'Testimonial' ),
        'menu_name'          => _x( 'Testimonials', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Testimonials', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New Testimonial', 'portfolio', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Testimonial', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Testimonial', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Testimonial', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Testimonial', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Testimonials', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Testimonials', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Testimonials:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No Testimonial found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No Testimonial found in Trash.', 'your-plugin-textdomain' )
      ),
      'taxonomies' => array('category'),
      'has_archive' => true,
      'public' => true,
      'has_archive' => true,
      'supports' => array('title','editor','author','comments','revisions'),
      'rewrite' => array('slug' => 'testimonial'),
    )
  ); 


  register_post_type( 'publication',
    array(
      'labels' => array(
        'name' => __( 'Publications' ),
        'singular_name' => __( 'Publication' ),
        'menu_name'          => _x( 'Publications', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Publications', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New Publication', 'portfolio', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Publication', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Publication', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Publication', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Publication', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Publication', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Publication', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Publication:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No Publication found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No Publication found in Trash.', 'your-plugin-textdomain' )
      ),
      'taxonomies' => array('category'),
      'has_archive' => true,
      'public' => true,
      'has_archive' => true,
      'supports' => array('title','editor','author','comments','revisions'),
      'rewrite' => array('slug' => 'publication'),
    )
  );

  register_post_type( 'spotlightitems',
    array(
      'labels' => array(
        'name' => __( 'Spotlights' ),
        'singular_name' => __( 'Spotlight' ),
        'menu_name'          => _x( 'Spotlights', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Spotlights', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New Spotlight', 'portfolio', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Spotlight', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Spotlight', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Spotlight', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Spotlight', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Spotlight', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Spotlight', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Spotlight:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No Spotlight found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No Spotlight found in Trash.', 'your-plugin-textdomain' )
      ),
      'taxonomies' => array('category'),
      'has_archive' => true,
      'public' => true,
      'has_archive' => true,
      'supports' => array('title','editor','author','comments','revisions'),
      'rewrite' => array('slug' => 'spotlightitems'),
    )
  );

  /** prevent Camplaign URL builder plugin from polluting admin screens */
  if (class_exists('reatlat_cub')) {

    add_action('current_screen', function($screen) {
      if (
         $screen->base === 'post' &&
         $screen->post_type === 'page' ||
         $screen->post_type === 'post' ||
         $screen->post_type === 'product'
      ) {

        /** collapse metaboxes by default */
        add_action('admin_print_scripts', function() {
          ?>
          <script>
            jQuery(($) => {
              $('#reatlat_cub-metabox--create-link').addClass('closed');
              $('#reatlat_cub-metabox--links-list').addClass('closed');
            });
          </script>
          <?php
        }, 500);
      }
      else {

        add_action('add_meta_boxes', function() {
          remove_meta_box('reatlat_cub-metabox--links-list', null, 'advanced');
          remove_meta_box('reatlat_cub-metabox--create-link', null, 'advanced');
        }, 500);
      }
   });
  }
  else {

    // $src = 'in ' . __FILE__ . 'on line ' . __LINE__ . ': ';
    // error_log($src . 'Campaign URL Builder Plugin not found');
  }

  if (ADMIN_MAINTENANCE) {
    function admin_maintenace_mode() {
      global $current_user;
      get_currentuserinfo();
      if($current_user->user_login != 'ADMIN_NAME') { ?>
              <style> .updated{margin:30px !important;} </style><?
              wp_die('<div id="message" class="updated"><p><b>Maintenance mode:</b> We are currently making updates. Everything will be online shortly.</p></div>');
          }
    }
    add_action('admin_head', 'admin_maintenace_mode');
  }

});

 /* Allow email address as username to log in */
 function grunt_wordpress_authenticate( $username ) {

  $user = get_user_by( 'email', $username );

  if ( !empty( $user->user_login ) ) {
      $username = $user->user_login;
  }

  return $username;

} add_action( 'wp_authenticate', 'grunt_wordpress_authenticate' );


/**
* Get rid of the Plugin and Theme editors
*/
define( 'DISALLOW_FILE_EDIT', true );



/**
* Possibly speedup loading times by allowing browser
* to load page files before javascript is downloaded
* and run.
*/
// Defer Parsing of JavaScript
// function defer_parsing_of_js ( $url ) {
// if ( FALSE === strpos( $url, '.js' ) ) return $url;
// if ( strpos( $url, 'jquery.js' ) ) return $url;
// return "$url' defer ";
// }
// add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );


/**
* Remove Promotional text footer from the admin area
*/
add_action('admin_init', function() {
  add_filter( 'admin_footer_text',    '__return_false', 11 );
  add_filter( 'update_footer',        '__return_false', 11 );
});