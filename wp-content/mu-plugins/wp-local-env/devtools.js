const vmaDev = {};

(() => {

    const checkout = {};
    vmaDev.checkout = checkout;

    const saved = {
        "billing_first_name": "joe",
        "billing_last_name": "shmow",
        "billing_company": "big co",
        "billing_country": "US",
        "billing_address_1": "323 fake street",
        "billing_address_2": "",
        "billing_city": "san francisco",
        "billing_state": "CA",
        "billing_postcode": "96451",
        "billing_phone": "415 598 28983",
        "billing_email": "customer@local.test",
        "order_comments": "some random notes go here",
    };

    const blackList = [
        'payment_method',
        '_wp_http_referer',
        'woocommerce-process-checkout-nonce',
    ];

    class Generator {

        construct() {
            this.fields = [];  
        }

        matchFieldId(elementName) {
            const matches = elementName.match(
                /^(\d+?)_(\w+?)_1__(\d+?)$/
            );

            if (matches === null){
                return this.matchCustomId(elementName);
            }

            return {
                eventId: matches[1],
                name: matches[2],
                attendee: matches[3],
            };
        }

        matchCustomId(elementName) {
            const matches = elementName.match(
                /^fooevents_custom_(\w+?)_1__(\d+?)$/
            );

            if (matches) {
                return {
                    eventId: null,
                    name: 'fooevents_custom',
                    customId: matches[1],
                    attendee: matches[2],
                };
            }
        }

        generate(elementName) {
            const info = this.matchFieldId(elementName)
            if (info) {
                let value = `${info.name} ${info.attendee}`;
                switch(info.name) {
                    case 'attendee':
                        value = `attendeefirstname ${info.attendee}`;
                        break;
                    case 'attendeeemail':
                        value = `attendee_${info.attendee}@local.test`;
                        break;
                    case 'fooevents_custom':
                        value = `custom ${info.customId} attendee ${info.attendee}`
                        break;
                }
                return value;
            }
            
        }

    }


    checkout.serialize = function () {
        const form = document.querySelectorAll('form.checkout')[0];
        const formData = new FormData(form);
        var object = {};
        formData.forEach((value, key) => {
            if (blackList.includes(key)) {
                return;
            }

            // Reflect.has in favor of: object.hasOwnProperty(key)
            if(!Reflect.has(object, key)){
                object[key] = value;
                return;
            }
            if(!Array.isArray(object[key])){
                object[key] = [object[key]];    
            }
            object[key].push(value);
        });
        var json = JSON.stringify(object, null, 2);
        console.log(json);
    }

    checkout.fill = function () {
        const generator = new Generator();
        const form = document.querySelectorAll('form.checkout')[0];
        const inputs = [...form.getElementsByTagName('input')];
        const filled = [];

        const setValue = (input) => {

            const value = saved[input.name] || generator.generate(input.name);

            if (value) {
                switch (input.type) {
                    case 'checkbox':
                        input.checked = !!value;
                        break;
                    case null || undefined:
                        break;
                    default: 
                        input.value = value;
                        break;
                }
            }
            
            return value;
        };

        inputs.forEach((element) => {
            const value = setValue(element);
            if (value) {
                filled.push(element);
            }
        });

        return filled;
    }


    window.addEventListener('DOMContentLoaded', () => {
        if (
                !document
                    .querySelectorAll('form.woocommerce-checkout')
                    .length
        ) {
            return;
        }
        const html = 'fill form';
        const button = document.createElement('button');
        button.id = 'vmaFillForm';
        button.style.width = '200px';
        button.style.top = '300px';
        button.style.left = '0';
        button.style.position = 'fixed';
        button.innerHTML = html;
        button.addEventListener('click', () => checkout.fill());
        document.querySelector('body').appendChild(button);
    })

})();

