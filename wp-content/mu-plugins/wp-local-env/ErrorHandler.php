<?php

namespace WpLocal;

class ErrorHandler {
    private const WP_STD_MASK = E_CORE_ERROR | 
        E_CORE_WARNING |
        E_COMPILE_ERROR |
        E_ERROR |
        E_WARNING |
        E_PARSE |
        E_USER_ERROR |
        E_USER_WARNING |
        E_RECOVERABLE_ERROR;

    private const ERROR_CODES = [
        E_ERROR => 'E_ERROR',
        E_WARNING => 'E_WARNING',
        E_PARSE => 'E_PARSE',
        E_NOTICE => 'E_NOTICE',
        E_CORE_ERROR => 'E_CORE_ERROR',
        E_CORE_WARNING => 'E_CORE_WARNING',
        E_COMPILE_ERROR => 'E_COMPILE_ERROR',
        E_COMPILE_WARNING => 'E_COMPILE_WARNING',
        E_USER_ERROR => 'E_USER_ERROR',
        E_USER_WARNING => 'E_USER_WARNING',
        E_USER_NOTICE => 'E_USER_NOTICE',
        E_STRICT => 'E_STRICT',
        E_RECOVERABLE_ERROR => 'E_RECOVERABLE_ERROR',
        E_DEPRECATED => 'E_DEPRECATED',
        E_USER_DEPRECATED => 'E_USER_DEPRECATED',
    ];

    private $thrownErrors = 0x1FFF; // E_ALL - E_DEPRECATED - E_USER_DEPRECATED
    private $loggedErrors = 1;

    private $prevExHandler;

    public function register()
    {
        if (!defined('WPCLI')) {
            ob_start();
        }

        set_error_handler([$this, 'errorHandler']);
        $this->prevExHandler = set_exception_handler([$this, 'thrownHandler']);
        // $this->registerHooks();
        //$this->checkHijacks();
    }


    public function errorHandler($errno, $errstr, $errfile, $errline)
    {

        $error =  new \ErrorException($errstr, $errno, 0, $errfile, $errline);
        $this->log($error);

        if (
            $this->wpStdMaskHandler($error) ||
            $this->fileExceptionHandler($error) ||
            $this->wpCliHandler($error)
        ) {
            return;
        }

        function_exists('xdebug_break') && xdebug_break();

        $throws = $errno & $this->thrownErrors;
        if ($throws) {
            throw $error;
        }
        else {
            $this->thrownHandler($error);
        }
    }


    public function log(\Throwable $error)
    {
        if ($this->isWpCli()) {
            return;
        }
        $formatted = $this->formatError($error);
        error_log($formatted);
    }


    public function wpStdMaskHandler(\Throwable $error)
    {
        $absPath = ABSPATH;
        $absPatt = "|$absPath|";
        $errfile = preg_replace($absPatt, '', $error->getFile());

        $pluginDirs = implode("|", [
            "event-espresso-core",
            "contact-form-7",
            "wordpress-importer",
            "media-cleaner-pro",
            "wordpress-importer-v2-master",
            "woocommerce-gateway-authorize-net-cim",
            "fooevents",
        ]);


        $re = '/^(?:(?:wp-includes|wp-admin)|(?:wp-content\/plugins\/(?:' . $pluginDirs . ')))/s';
        $isHandled = fn() => $error->getCode() & static::WP_STD_MASK;
        $isMatch = fn() => preg_match($re, $errfile);

        if(!$isHandled() && $isMatch()) {
            return true;
        }
        return false;
    }

    public function fileExceptionHandler(\Throwable $error)
    {
        $fileExemptions = [
            "wp-content/plugins/event-espresso-core" . 
                "/core/helpers/EEH_Sideloader.helper.php" => 
                    static::WP_STD_MASK ^ E_WARNING,

            "wp-content/plugins/event-espresso-core" . 
                "/core/third_party_libs/pue/pue-client.php" => 
                    static::WP_STD_MASK ^ E_WARNING,

            "wp-admin/includes/image.php" => static::WP_STD_MASK ^ E_WARNING,

            "wp-includes/class-wp-image-editor.php" => static::WP_STD_MASK ^ E_WARNING,

            "wp-content/uploads/fooevents/themes/default_2020/ticket.php" => static::WP_STD_MASK ^ E_WARNING,
        ];

        $relpath = str_replace(ABSPATH, '', $error->getFile());

        $mask = $fileExemptions[$relpath] ?? null;

        if ($mask && !($error->getCode() & $mask)) {
            return true;
        }
        return false;
    }


    public function wpCliHandler(\Throwable $error) {
        if (!$this->isWpCli()) {
            return false;
        }
        $mask = E_ALL & ~E_DEPRECATED & ~E_NOTICE;
        if (!($error->getCode() & $mask)) {
            return true;
        }
        return false;
    }


    public function thrownHandler(\Throwable $e) 
    {
        $this->cleanOutputBuffer();

        if (is_callable($this->prevExHandler))
        {
            call_user_func($this->prevExHandler, $e);
            return;
        }
        $msg = $this->formatError($e);
        $wpError = new \WP_ERROR('uncaught_error', $msg);
        add_filter('wp_die_handler', [$this, 'wp_die_handler_filter'], 10, 1);
        register_shutdown_function(fn() => exit(255));
        wp_die($wpError, 'Internal Server Error', ['code' => 500]);
    }


    protected function isWpCli()
    {
        return defined('WP_CLI') && WP_CLI;
    }


    protected function cleanOutputBuffer()
    {
        while (ob_get_level() !== 0) {
            ob_end_clean();
        }
    }


    protected function formatError(\Throwable $error)
    {
        $code = static::ERROR_CODES[$error->getCode()] ?? 'UNKNOWN';
        $trace = $error->__toString();
        $report = "thrown error $code \n $trace";
        return defined('WP_CLI') ? $report : "<pre>$report</pre>";
    }

    public function wp_die_handler_filter()
    {
        return function(\WP_Error $wpError) {
            if (!headers_sent() ) {
                header("Content-Type: text/html; charset=utf-8");
                status_header(500);
                nocache_headers();
            }
            echo $wpError->get_error_message();
            die();
        };
    }

}
