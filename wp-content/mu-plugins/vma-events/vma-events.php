<?php
/**
 * Plugin Name: Fooevents Extensions
 * Description: Basic tweaks to Fooevents plugin
 * Version: 0.1
 * Author: Evan Bangham
 */


require __DIR__ . '/src/VmaEvents.php';

global $vmaEvents;
$vmaEvents = new VmaEvents();

function VMA_EVENTS() {
    global $vmaEvents;
    return $vmaEvents;
}