<?php
namespace VmaEvents;

class System {
    private EventPermalinks $permalinks;

    private bool $doingEventAjax;
    private bool $isSingleEvent;


    public function __construct($permalinks)
    {
        $this->permalinks = $permalinks;
        add_action('woocommerce_init', function() {

            $this->detectEventAjax();
            add_action('wp', [$this, 'wp']);
            add_action('pre_get_posts', [$this, 'pre_get_posts']);
            $this->addAjaxActions();
            $this->redirectShop();
        });
        
        add_action(
            'woocommerce_product_query', 
            [$this, 'woocommerce_product_query'],
            10,
            2
        );
    }


    public function pre_get_posts($query)
    {
        if(!$query->is_main_query()) {
            return;
        }

        if ($this->isEventQuery($query)) {
            FooEvents::exclusiveFooEvents($query);
            add_filter('template_include', [$this, 'template_include']);
        }
    }


    /**
     * Removes Events from main product query
     */
    public static function woocommerce_product_query($query)
    {
        FooEvents::occlusiveFooEvents($query);
    }


    private function isEventQuery($query)
    {
        return (
            ($query->query_vars['vma-event-single'] ?? null) ||
            ($query->query_vars['vma-event-archive'] ?? null)
        );
    }


    public function template_include($template)
    {
        if (is_product()) {
            add_filter('is_wcopc_checkout', '__return_true');
            add_action('woocommerce_after_main_content', function() {
                echo do_shortcode('[woocommerce_one_page_checkout template="product-single" product_ids="20041"]');
            });
            return '/workspace/wp-content/themes/vmamain/template-parts/woo-page.php';

        }
        return $template;
    }


    public function wp($wp)
    {
        global $wp_query;
    }


    public function addAjaxActions()
    {
        $wc_ajax_empty_cart = function() {
            ob_start();

            if (WC()->cart->empty_cart() !== false) {
                WC_AJAX::get_refreshed_fragments();
            } else {
                wp_send_json_error();
            }
        };
        add_action('wc_ajax_empty_cart', $wc_ajax_empty_cart);

        $wc_ajax_remove_others_cart = function() {
            ob_start();

            $product_id = absint($_POST['product_id']);

            $toRemove = [];
            foreach(WC()->cart->get_cart() as $key => $cart_item) {
                $product = $cart_item['data'];
                if ($product->get_type() === 'variation') {
                    $product = wc_get_product($product->get_parent_id());
                }
                if($product->get_id() !== $product_id) {
                    $toRemove[$key] = $product;
                }
            }

            if(empty($toRemove)) {
                wp_send_json_error();
            }
            else {
                foreach($toRemove as $key => $product) {
                    WC()->cart->remove_cart_item($key);
                }
                WC_AJAX::get_refreshed_fragments();
            }
        };
        add_action('wc_ajax_remove_others_cart', $wc_ajax_remove_others_cart);

        $prevent_cart_redirect = function($params, $handle) {
            if (
                $handle = 'wc-add-to-cart-variation' ||
                $handle = 'wc-add-to-car'
            ) {
                $params['cart_redirect_after_add'] = 'no';
            }
            return $params;
        };
        add_filter('woocommerce_get_script_data', $prevent_cart_redirect, 10, 2);
    }


    public function redirectShop()
    {
        add_action('template_redirect', function() {
            if(is_shop()) {
                wp_redirect(site_url('/events/'), 301);
            }
        });
    }


    public function detectEventAjax()
    {
        add_action('init', function() {
            $this->doingEventAjax = (
                defined('WC_DOING_AJAX') && ($_POST['is_vma_event'] ?? false)
            );
            if ($this->doingEventAjax) {
                do_action('vma_events_doing_event_ajax');
            }
        });
        
    }


    public function isEventAjax() {
        return $this->doingEventAjax;
    }
}