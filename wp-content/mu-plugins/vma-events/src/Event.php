<?php
namespace VmaEvents;

use \WC_Product;

class Event {

    private WC_Product $product;

    public function __construct(WC_Product $product)
    {

        $this->product = $product;
    }


    public function get_permalink()
    {
        return $this->product->get_permalink();
    }


    public function get_meta(...$args)
    {
        return $this->product->get_meta(...$args);
    }


    public function isExpired()
    {
        return FooEvents::isProductExpired($this->product);
    }


    public static function isEvent(object $object)
    {
        return !!static::createFromGenericPost($object);
    }


    public static function createFromGenericPost(object $object)
    {
        if ($object instanceof static) {
            return $object;
        }

        if(FooEvents::isFooEventProduct($object)) {
            return new static($object);
        }



    }


    public static function currentEventsQuery()
    {
        return FooEvents::currentEventsQuery();
    }
}