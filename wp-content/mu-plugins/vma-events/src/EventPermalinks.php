<?php
namespace VmaEvents;

class EventPermaLinks {

    private bool $useModifiedPermalink = true;
    private bool $matched;
    private bool $matchedSingle;
    private bool $matchedArchive;

    public function __construct()
    {
        add_action('init', [$this, 'init'], 0);
        add_filter('query_vars', [$this, 'query_vars'], 0);
        add_action('parse_request', [$this, 'parse_request' ], 999);
        add_filter('post_type_link', [$this, 'post_type_link'], 10, 4);
    }


    public function init()
    {
        if (!$this->useModifiedPermalink) {
            return;
        }

        $this->eventRule = 'event/([^/]+)/?$';

        /** Single Event */
        add_rewrite_rule(
            $this->eventRule,
            'index.php?vma-event-single=1&name=$matches[1]',
            'top'
        );

        /** Event archive */
        add_rewrite_rule(
            "events/?$",
            "index.php?vma-event-archive=1",
            'top'
        );

        //flush_rewrite_rules();
    }


    function query_vars($vars)
    {
        $vars[] = 'vma-event-single';
        $vars[] = 'vma-event-archive';
        return $vars;
    }


    public function parse_request($wp)
    {
        $this->matched = false;
        $this->matchedSingle = false;
        $this->matchedArchive = false;

        $single = $wp->query_vars['vma-event-single'] ?? null;
        $archive = $wp->query_vars['vma-event-archive'] ?? null;

        if ($single) {
            $wp->query_vars['post_type'] = 'product';
            $this->matchedSingle = true;
        }

        if ($archive) {
            $wp->query_vars['post_type'] = 'product';
            $this->matchedArchive = true;
        }

        $this->matched = (
            $this->matchedArchive || $this->matchedSingle
        );
    }

    // public function alterWooFrontendScripts($params, $handle)
    // {
    //     $targets = [
    //         'wc-add-to-cart-variation',
    //         'wc-add-to-cart',
    //     ];
    //     if($this->eventInLoopIteration() && in_array($handle, $targets)) {
    //         $params['cart_redirect_after_add'] = 'yes';
    //         $params['cart_url'] = wc_get_checkout_url();
    //     }
    //     return $params;
    // }


    // public function EventAddToCartFormAction($actionUrl)
    // {
    //     $eventProduct = $this->eventInLoopIteration();
    //     if ($eventProduct) {
    //         $this->useModifiedPermalink = false;
    //         $actionUrl = $eventProduct->get_permalink();
    //         $this->useModifiedPermalink = true;
    //     }
    //     return $actionUrl;
    // }


    public function post_type_link($permalink, $post, $leavename, $sample)
    {
        if (WC()->is_rest_api_request() || !$this->useModifiedPermalink) {
            return $permalink;
        }

        $product = null;
        if (isset( $post->ID, $post->post_type ) && $post->post_type === 'product') {
            $product = wc_get_product($post);
        }

        $draft_or_pending = get_post_status( $post ) && 
            in_array(
                get_post_status( $post ),
                array( 'draft', 'pending', 'auto-draft', 'future' ),
                true
            );

        $path = null;
        if ($product && VMA_EVENTS()->isEvent($product)) {
            if(( ! $draft_or_pending || $sample )) {
                $slug = $post->post_name;

                $permalink = home_url("/event/" . ($leavename ? '%postname%' : $slug) . '/');
            };
        }

        return $permalink;
    }


    // public function wp($wp) {

    //     if (is_product() && !($wp->query_vars['vma-event'] ?? null)) {
    //         $product = wc_get_product();
    //        // wp_redirect($product->get_permalink(), 301);

    //     }
    // }


    /**
     * Redirect single product route to event route
     * 
     * Only redirects the single product route when the queried product
     * is an event prodcut. 
     */
    public function redirectSingleEventProduct()
    {
        /** 
         * can't determine if a product is an event product until after
         * the query has run.
         */
        if ($wp->query_vars['vma-event'] ?? null) {
            add_action('template_redirect', function() {
                $product = wc_get_product();
                if (VMA_EVENTS()->isEventProduct($product)) {
                    wp_redirect($product->get_permalink(), 301);
                }
                
            }, 10);
        }
    }


    public function archiveLink()
    {
        return site_url() . '/events';
    }


    public function matched()
    {
        return $this->matched;
    }


    public function matchedSingle()
    {
        return $this->matchedSingle;
    }


    public function matchedArchive()
    {
        return $this->matchedArchive;
    }


    // public function eventInLoopIteration()
    // {
    //     $product = wc_get_product();
    //     return $this->isEventProduct($product) ? $product : null;
    // }

}