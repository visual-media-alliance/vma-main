<?php
namespace VmaEvents;

use \WC_Product;

class FooEvents {

    public function construct() {
        /**
         * Overide field labels system wide
         */
        add_filter('gettext_woocommerce-events', function ($translation, $text, $domain) {
            switch ($text) {
                case "Directions:":
                    return "Address:";
                case "Directions":
                    return "Address";
                case "Directions to the venue.":
                    return "Address of the venue.";
                default:
                    return $translation;
            }
        }, 10, 3);
    }


    public static function isFooEventProduct(object $product) {

        if ($product instanceof WP_Post) {
            $product = wc_get_product($product->ID);
        }

        if ($product) {
            $indicator = $product->get_meta('WooCommerceEventsEvent');
            if ($indicator === 'Event') {
                return true;
            }
        }
        return false;
    }


    public static function isProductExpired(WC_Product $product) {
        $expires = $product->get_meta('WooCommerceEventsExpireTimestamp');
        if ($expires) {
            return $expires <= current_time('timestamp');
        }
        return false;
    }


    /**
     * Add predicates to meta_query causing it to return
     * exclusivly FooEvent products
     */
    public static function exclusiveFooEvents($query)
    {
        $existing_meta = $query->get('meta_query');
        $meta_query = $existing_meta ? $existing_meta : [];

        $meta_query[] = [
            'key' => 'WooCommerceEventsEvent',
            'compare' => '=',
            'value' => 'Event',
        ];
        $query->set('meta_query', $meta_query);

        return $query;
    }


    /**
     * Add predicates to meta_query causing it to exclude
     * FooEvent products
     */
    public static function occlusiveFooEvents($query)
    {
        $existing_meta = $query->get('meta_query');
        $meta_query = $existing_meta ? $existing_meta : [];

        $meta_query[] = [
            'key' => 'WooCommerceEventsEvent',
            'compare' => '!=',
            'value' => 'Event',
        ];
        $query->set('meta_query', $meta_query);

        return $query;
    }


    public static function currentEventsQuery()
    {
        $queryfunc = function($wp_query_args) {
            /* exclude expired events */
            $wp_query_args['meta_query'][] = [
                'relation' => 'OR',
                array(
                    'key' => 'WooCommerceEventsExpireTimestamp',
                    'compare' => 'NOT EXISTS',
                    'value' => '',
                ),
                array(
                    'key' => 'WooCommerceEventsExpireTimestamp',
                    'compare' => '=',
                    'value' => '',
                ),
                array(
                        'key' => 'WooCommerceEventsExpireTimestamp',
                        'value' => current_time('timestamp'),
                        'type' => 'numeric',
                        'compare' => '>='
                ),
            ];
            /* filter event type products */
            $wp_query_args['meta_query'][] = [
                array(
                    'key' => 'WooCommerceEventsEvent',
                    'compare' => '=',
                    'value' => 'Event',
                ),
            ];
        
            return $wp_query_args;
        };
        
        add_filter(
            'woocommerce_product_data_store_cpt_get_products_query',
            $queryfunc, 10
        );

        $query = new \WC_Product_Query([
            'limit' => 10,
        ]);
        $events = $query->get_products();
        
        remove_filter(
            'woocommerce_product_data_store_cpt_get_products_query',
            $queryfunc, 10
        );

        return $events;
    }



}