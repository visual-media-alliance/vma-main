<?php

class VmaEvents {
    
    private VmaEvents\EventPermalinks $permalinks;
    private VmaEvents\Events $events;

    public function __construct()
    {
        require __DIR__ . '/EventsException.php';
        require __DIR__ . '/FooEvents.php';
        require __DIR__ . '/Event.php';
        require __DIR__ . '/System.php';
        require __DIR__ . '/EventPermalinks.php';
        $this->permalinks = new VmaEvents\EventPermalinks();
        $this->system = new VmaEvents\System($this->permalinks);
    }


    public function archiveLink()
    {
        return $this->permalinks->archiveLink();
    }


    public function isEvent(object $object)
    {
        return VmaEvents\Event::isEvent($object);
    }


    public function isEventExpired(object $product)
    {
        return VmaEvents\Event::createFromGenericPost($product)
            ->isExpired();
    }


    public function queryCommingEvents()
    {
        return VmaEvents\Event::currentEventsQuery();
    }


    public function isEventAjax() {
        return $this->system->isEventAjax();
    }
}