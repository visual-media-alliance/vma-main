<?php
/**
 * PLugin Name: Hooks to cleanup importing data from vma-main live
 * Description: Provides wp-cli commands and some other features.
 */

class VMA_IMPORT_PLUGIN {


    private array $importPostTypes = [
        'acf',
        'acf-field',
        'acf-field-group',
        //'attachment',
        'customize_changeset',
        'page',
        'publication',
        'post',
        'nav_menu_item',
        'mc4wp-form',
        'spotlightitems',
        'testimonial',
        'wpcf7_contact_form',
    ];

    private array $parsedTypes = [];

    private array $failedTypes = [];


    public function __construct()
    {
        add_action('import_start', [$this, 'import_start']);
    }


    public function import_start()
    {
        // add_filter('wxr_importer.pre_process.post', [$this, 'preprocessPost'], 10, 4);
        // add_filter('wp_import_post_comments', [$this, 'preprocessComments'], 10, 3);
        add_action('wxr_importer.parsed.post', [$this, 'parsedPost'], 10, 3);
        add_action('wxr_importer.process_failed.post',
            [$this, 'processFailedPost'],
        10, 5);
    }


    public function parsedPost($post_id, $parsed, $node) {

        $postType ??= $parsed['data']['post_type'];

        $this->parsedTypes[$postType]['count'] = ($this->parsedTypes[$postType]['count'] ?? 0) + 1;
    }


    public function preprocessPost($data, $meta, $comments, $terms)
    {
        // $type ??= $data['post_type'];
        // if ($type === 'page') {
        //     xdebug_break();
        // }
        // /**
        //  * Only import allowed post types
        //  */
        // if (!in_array($type, $this->importPostTypes)) {
        //     echo "skipping post type: $type on post {$data['post_title']} \n";
        //     return [];
        // }
        // return $data;
    }

    public function processFailedPost($post_id, $data, $meta, $comments, $terms)
    {
        $type = $data['post_type'];
        $count = $this->failedTypes[$type]['count'] ?? 0;
        $this->failedTypes[$type]['count'] += 1;
    }

    public function preprocessComments($comments, $post_id, $post)
    {
        /** prevent importing any comments */
        return [];
    }

    /**
     * Command to import from vma main live
     * 
     * 
     * Cleans database, removes uploads then runs the importer
     */
    public function command($args, $assoc_args)
    {

        $sqlfile = '/dockershare/vma-main-dev-10-27-20.sql';
        $xmlfile = '/dockershare/visualmediaalliance.wordpress.2020-10-27.000.xml';

        $default_author_id = 1;
        $fetch_attachments = false;


        WP_CLI::log("importing database dump");

        $options = array(
            //'return'     => true,   // Return 'STDOUT'; use 'all' for full object.
            //'parse'      => 'json', // Parse captured STDOUT to JSON array.
            'launch'     => false,  // Reuse the current process.
            'exit_error' => true,   // Halt script execution on error.
        );

        $cmd = "db import $sqlfile";
        WP_CLI::runcommand( $cmd, $options );


        WP_CLI::log('deleting uploads');
        $cmd = 'find %s -maxdepth 1 -type d  | grep \'[[:digit:]]$\' | xargs rm -rf';
        WP_CLI::launch(WP_CLI\Utils\esc_cmd($cmd, wp_upload_dir()));
        WP_CLI::success('uploads deleted');

        if (isset($assoc_args['reset-only'])) {
            return;
        }

        $assoc_args['verbose'] = 'debug';
        $logger = new WP_Importer_Logger_CLI();
        if ( ! empty( $assoc_args['verbose'] ) ) {
            if ( $assoc_args['verbose'] === true ) {
                $logger->min_level = 'info';
            } else {
                $valid = $logger->level_to_numeric( $assoc_args['verbose'] );
                if ( ! $valid ) {
                    WP_CLI::error( 'Invalid verbosity level' );
                    return;
                }

                $logger->min_level = $assoc_args['verbose'];
            }
        }

        $path = realpath( $args[0] ?? $xmlfile);
        if ( ! $path ) {
            WP_CLI::error( sprintf( 'Specified file %s does not exist', $args[0] ) );
        }

        $options = array(
            'fetch_attachments' => $fetch_attachments,
            'default_author' => $default_author_id,
        );
        if ( isset( $assoc_args['default-author'] ) ) {
            $options['default_author'] = absint( $assoc_args['default-author'] );

            if ( ! get_user_by( 'ID', $options['default_author'] ) ) {
                WP_CLI::error( 'Invalid default author ID specified.' );
            }
        }

        $importer = new WXR_Importer( $options );
        $importer->set_logger( $logger );

        // $data = $importer->get_preliminary_information( $path );

        // var_dump($data);
        // exit();


        WP_CLI::log("importing:");
        WP_CLI::log("{$data->post_count} posts");
        WP_CLI::log("{$data->media_count} media items");


        $result = $importer->import( $path );
        if ( is_wp_error( $result ) ) {
            WP_CLI::error( $result->get_error_message() );
        }

        WP_CLI::log("Parsed types:\n");
        print_r($this->parsedTypes);

        WP_CLI::log("Failed types:\n");
        print_r($this->failedTypes);
    }
}


add_action('init', function() {
    if ( ! class_exists( 'WP_Importer' ) ) {
        return;
    }

    $plugin = new VMA_IMPORT_PLUGIN();

    if (defined('WP_CLI') && WP_CLI) {
        WP_CLI::add_command('vma-import', [$plugin, 'command']);
    }

});
