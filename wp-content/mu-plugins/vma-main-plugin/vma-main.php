<?php

/**
 * Plugin main file.
 *
 * @package   vma\main-wp-plugin
 * @copyright 2019 Visual Media Alliance
 * @license   https://www.apache.org/licenses/LICENSE-2.0 Apache License 2.0
 * @link      https://main.vma.bz
 *
 * @wordpress-plugin
 * Plugin Name: VMA Main Wordpress Plugin
 * Plugin URI:  https://main.vma.bz
 * Description: Core plugin containing customizations for the VMA Main site
 * Version:     0.0.1
 * Author:      Evan Bangham
 * Author URI:  https://ebangham.com
 * License:     Apache License 2.0
 * License URI: https://www.apache.org/licenses/LICENSE-2.0
 * Text Domain: vma-main
 */

// phpcs:disable PSR1.Files.SideEffects
// Some global constants defined
define('VMA_MAIN_VERSION', '0.0.1');
define('VMA_MAIN_PLUGIN_MAIN_FILE', __FILE__);
define('VMA_MAIN_PHP_MINIMUM', '7.2.0');


/**
 * Handles plugin activation.
 *
 * Throws an error if the plugin is activated on an older version than PHP 7.2.
 *
 * @since 0.0.1
 * @access private
 *
 */
function vmamain_activate_plugin()
{
    do_action('vmamain_activate_plugin');
}

register_activation_hook(__FILE__, 'vmamain_activate_plugin');

/**
 * Handles plugin deactivation.
 *
 * @since 0.0.1
 * @access private
 *
 * @param bool $network_wide Whether to deactivate network-wide.
 */
function vmamain_deactivate_plugin()
{
    do_action('vmamain_deactivate_plugin');
}

register_deactivation_hook(__FILE__, 'vmamain_deactivate_plugin');

/**
 * Resets opcache if possible.
 *
 * @since 0.0.1
 * @access private
 */
function vmamain_opcache_reset()
{
    if (version_compare(PHP_VERSION, VMA_MAIN_PHP_MINIMUM, '<')) {
        return;
    }

    if (! function_exists('opcache_reset')) {
        return;
    }

    if (! empty(ini_get('opcache.restrict_api')) && strpos(__FILE__, ini_get('opcache.restrict_api')) !== 0) {
        return;
    }

    opcache_reset();
}
add_action('upgrader_process_complete', 'vmamain_opcache_reset');

require_once __DIR__ . '/includes/bootstrap.php';

vmamain_bootstrap(__DIR__);
// phpcs:enable PSR1.Files.SideEffects.FoundWithSymbols