<?php

namespace VmaMain\WpAdmin;

use Lib\CollectionInterface;

class AdminNoticeBag  implements IteratorAggregate, CollectionInterface {
    private array $notices;

    public function __construct(AdminNotice ...$notices)
    {
        $this->notices = $notices;
    }
}