<?php

namespace VmaMain\WpAdmin;


class AdminNotice {
    private string $message;

    public function __construct(string $message) {
        $this->message = $message;
    }
}