<?php

namespace VmaMain;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

interface BootableProviderInterface extends ServiceProviderInterface {
    public function boot(Container $container): void;
}
