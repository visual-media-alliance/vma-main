<?php

namespace VmaMain;

use Pimple\Container;
use Pimple\ServiceProviderInterface as Provider;
use WpAdmin\AdminNotice;

class Plugin extends Container
{

    /** @var boolean */
    protected $booted = false;

    /** @var string|null */
    protected $pluginPath;

    /** @var array[] */
    protected $pluginDependencies = [];

    /** @var callable[] */
    protected $beforeDeactivateCallbacks = [];

    /** @var callable[] */
    protected $initCallbacks = [];

    /** @var AdminMessage[] */
    protected AdminNotices $adminNotices = [];

    public function __construct(string $pluginPath)
    {
        $this->pluginPath = $pluginPath;
    }

    /**
     * Bootstraps the Plugin and connects its services
     *
     * This is used to overlay the creational and initialization semantics
     * of this Plugin within those of the Wordpress Internals.
     *
     * @param Provider $provider    Pimple Provider instance to register services
     *
     * @return void
     */
    public function bootWith(Provider $provider)
    {
        $this->register($provider);
        add_action('vmamain_activate_plugin', [$this, 'activate']);
        add_action('vmamain_deactivate_plugin', [$this, 'deactivate']);
        add_action('init', [$this, 'handleWpInit']);
        add_action('plugins_loaded', [$this, 'handleWpPluginsLoaded']);
    }

    /**
     * Add Dependencies on other Wordpress plugins.
     *
     * @param string    $serviceName    The name of the service declaring the dependency.
     * @param array     $depends        Description of the plugins required.
     *
     * @return void
     */
    public function addPluginDependencies($serviceName, array $depends)
    {
        $this->pluginDependencies[$serviceName] = $depends;
    }

    /**
     * Add Callbacks to be fired when the Wordpress 'init' action is invoked.
     *
     * @param callable  $callback
     *
     * @return void
     */
    public function addInitCallback($callback)
    {
        $this->initCallbacks[] = $callback;
    }

    /**
     * Centralized handler for the Wordpress internal 'init' action
     *
     * Dispatches to internal Plugin Services as necessary.
     *
     * @return void
     */
    public function handleWpInit()
    {
        if (is_admin()) {
            add_action('admin-notices', [$this, 'handleWpAdminNotices']);
        }

        $this->fireCallbacks($this->initCallbacks);
    }

    /**
     * Centralized handler for the Wordpress internal 'init' action
     * 
     * Dispatches to internal Plugin Services as necessary.
     * @return void
     */
    public function handleWpPluginsLoaded(): void
    {
        $this->checkPluginDependencies();
    }

    public function beforeDeactivate(\Closure $callback): void
    {
        $this->beforeDeactivateCallbacks[] = $callback;
    }


    /**
     * Handles activation/deactivation and version changes
     *
     * @return void
     */
    public function update()
    {

        // do any updates here, db migrations etc...
        flush_rewrite_rules();
    }

    public function activate(): void
    {
        $this->handleWpInit();
        $this->update();
    }

    public function deactivate(): void
    {
        $this->fireCallbacks($this->beforeDeactivateCallbacks);
        $this->update();
    }

    /**
     * fire any callbacks registered in service using this function
     * 
     * @param callable[]    $callbacks
     */
    public function fireCallbacks(array $callbacks): void
    {
        foreach ($callbacks as $callback) {
            $callback($this);
        }
    }

    public function checkPluginDependencies(): void
    {

    }

    private function handleWpAdminNotices(): void
    {

    }

    public function addAdminNotice(): void
    {

    }
}
