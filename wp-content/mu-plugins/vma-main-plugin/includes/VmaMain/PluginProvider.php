<?php

namespace VmaMain;

use Pimple\Container;
use VmaMain\Plugin;
use Pimple\ServiceProviderInterface;
use VmaMain\MarketingEvents\Events;
use VmaMain\Subsections\Government;

class PluginProvider implements BootableProviderInterface
{

    /**
     * @return void
     */
    public function register(Container $plugin): void
    {
        // $plugin['marketing-events'] = function ($ioc) {
        //     return (new Events())->setup();
        // };
        
        $plugin[Government::class] = function (Plugin $plugin): Government {
            return new Government($plugin);
        };
    }

    public function boot(Container $plugin): void
    {
        assert($plugin instanceof Plugin);
        // $government = $plugin[Government::class];
        // assert($government instanceof Government);
        // $plugin->addPluginDependencies(
        //     Government::class,
        //     $government->pluginDependencies()
        // );
        // $plugin->addInitCallback(function() use($government) {
        //     $government->init();
        // });
    }
}
