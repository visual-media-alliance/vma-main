<?php

namespace VmaMain\MarketingEvents;

class Events
{

    /** @var string */
    private $slug = 'event';

    public static function make(): self
    {
        return new static();
    }

    public function setup(): void
    {
        add_action('init', array($this, 'registerPostType'));
    }

    public function teardown(): void
    {
        unregister_post_type($this->slug);
    }

    public function registerPostType(): void
    {
        register_extended_post_type($this->slug, [
            'description' => 'Events that members and non-members can attend',
            'has_archive' => true,
            'pages' => false,
            'plural' => 'Events',
            'slug' => 'events',
            'show_in_feed' => true,
            'show_in_rest' => true,
            'supports' => ['editor', 'title', 'revisions', 'thumbnail']
        ]);
    }
}
