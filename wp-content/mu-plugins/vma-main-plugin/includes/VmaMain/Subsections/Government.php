<?php

namespace VmaMain\Subsections;

use VmaMain\Plugin;

class Government
{

    /** @var Plugin */
    protected $plugin;


    /** @var boolean */
    protected $booted = false;
    
    /** @var string */
    protected $slug = 'government';

    public function boot(): void
    {
    }

    public function __construct(Plugin $plugin)
    {
        $this->plugin = $plugin;
    }

    public function pluginDependencies(): array
    {
        return ['acf' => ['>=', '8.5.3']];
    }

    public function init(): void
    {
        $this->registerPostType();
        $this->plugin->beforeDeactivate(function () {
            unregister_post_type($this->slug);
        });
    }

    public function registerPostType(): void
    {
        register_extended_post_type('government', [
            'description' => 'Helpful information about legal compliance',
            'has_archive' => false,
            'hierarchical' => true,
            'pages' => false,
            'plural' => 'government',
            'slug' => 'events',
            'show_in_feed' => true,
            'show_in_rest' => true,
            'supports' => ['editor', 'title', 'revisions', 'thumbnail', 'page-attributes']
        ]);
    }

    public function registerFields(): void
    {
    }
}
