<?php

// phpcs:disable PSR1.Files.SideEffects
require_once ABSPATH . '/vendor/autoload.php';

function vmamain_bootstrap($pluginPath)
{
    $plugin = new \VmaMain\Plugin($pluginPath);
    $provider = new \VmaMain\PluginProvider();
    $plugin->bootWith($provider);
}
// phpcs:enable
