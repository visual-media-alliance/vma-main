<?php
/*
Plugin Name: WP-CFM Multi-environment 
Description: Enables configuration management for multiple environments with WP-CFM.
*/



// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Patch in a Patheon Environment if one doesn't already exist
 * This needs to run before the Pantheon mu-plugin.
 */
if ( !defined('PANTHEON_ENVIRONMENT')) {
	define('PANTHEON_ENVIRONMENT', (wp_get_environment_type()));
	
	/**
	 * Set the current env to local when PANTHEON_ENVIRONMENT is set to
	 * it.
	 */
	add_filter('wpcfm_current_env', function(){
		return wp_get_environment_type();
	});
}


/**
 * Hook in the option processing 
 */
// add_action('init', function() {
// 	if (!class_exists('WPCFM_Core')) {
// 		return;
// 	}

// 	require __DIR__ . '/wp-cfm-extensions/WpConfigurator/OptionProcessing.php';
// 	$processing = new etherealite\WpConfigurator\OptionProcessing();
// });


/**
 * Add some additional subcommands commands to the WP-CLI command
 * provided by the WP-CFM plugin.
 */
if ( defined('WP_CLI') && WP_CLI ) {
    add_action('init', function() {

		if (!class_exists('WPCFM_Core')) {
			return;
		}

		/**
		 * Disable unwanted commands
		 */
		$runner = WP_CLI::get_runner();
		$runner->config['disabled_commands'] = array_merge(
			$runner->config['disabled_commands'],
			[
				'config edit',
				'config create',
				'config delete',
				'config get',
				'config has',
				'config list',
				'config path',
				'config set',
				'config shuffle-salts',
			]
		);

		require __DIR__ . '/wp-cfm-extensions/eb_wpcfm_cli_command.php';
		WP_CLI::add_command('config orphans', 'wpcfm_cli_orphans');
		WP_CLI::add_command('config bundle', 'wpcfm_cli_bundle');
		//WP_CLI::add_command('config diff', 'wpcfm_cli_diff');
    });
}


/**
 * Add the local environment to our multi-env list
 */
add_filter('wpcfm_multi_env', function($environments) {
	return ['local', ...$environments];
});



/**
 * Add in our own disallowed items for things we don't want tracked
 */
add_filter( 'wpcfm_configuration_items', function($items) {

	$wooItems = class_exists( 'WooCommerce' ) ? 
		require(
			__DIR__ . '/wp-cfm-extensions/wc_option_key_blacklist.php'
		) : [];

	$pluginItems = require(
		__DIR__ . '/wp-cfm-extensions/plugins_option_key_blacklist.php'
	);
	
	$disallowed_items = [

		/**
		 * Wordpress Builtins
		 */

		 /** List of plugins that have been activated */
		'active_plugins',
		
		/** Version of option_tables data and wp table schema */
		'db_version',
		
		/** Holds the slug (stylesheet controlled) name of the active theme */
		'current_theme',
		
		/** Version of the database when wordpress was first installed */
		'initial_db_version',

		/** Stores new email address before sending confirmation email */
		'new_admin_email',
		
		/** Keys emailed to admin accounts in recovery mode in WP 5.2+ */
		'recovery_keys',

		/** Timestamp of the last time a recovery mode email was sent */
		'recovery_mode_email_last_sent',

		/** List of recently activated plugins (is assumed deprecated) */
		'recently_activated',

		/** Stores the stylesheet of the old theme when switching to a new one */
		'theme_switched',

		/** List of Plugins to be uninstalled by uninstaller */
		'uninstall_plugins',


		/**
		 * Woocommerce
		 */
		//...$wooItems,

		/**
		 * Various Plugins
		 */
		...$pluginItems,
	];

	foreach ( $disallowed_items as $row ) {
		unset( $items[ $row ] );
	}

	return $items;
}, 500, 1);