<?php

class FooEvents_PDF_helper {
    
    private $Config;
    
    public function __construct($Config) {
    
        $this->Config = $Config;
        
    }
    
    /**
     * Includes email template and parses PHP
     * 
     * @param string $template
     * @param array $customerDetails
     * @param array $ticket
     * @return string
     */
    public function parse_email_template($template, $customerDetails = array(), $ticket) {
        
        ob_start();
	    $themePacksURL = $this->Config->themePacksURL;
        $barcodeURL =  $this->Config->barcodeURL;
        
        $globalFooEventsPDFTicketsFont = get_option('globalFooEventsPDFTicketsFont');
        $font_family = $this->_get_font_family($globalFooEventsPDFTicketsFont);
        $font_face = $this->_get_font_face($globalFooEventsPDFTicketsFont);
        $font_face .= $this->_get_font_face_bold($globalFooEventsPDFTicketsFont);
        
        include($template); 
        
        return ob_get_clean();
        
    }
    
    /**
     * Includes the ticket template and parses PHP.
     * 
     * @param array $ticket
     * @param string $template_name
     */
    public function parse_ticket_template($ticket, $template) {
        
        ob_start();
        
        $plugins_url = plugins_url();
        $themePacksPath = $this->Config->themePacksPath;
        $themeDetails = explode('/', $template);
        $themeDetails = array_reverse($themeDetails);
        $themeName = $themeDetails[1];

        $barcodePath = $this->Config->barcodePath;
        $barcodeURL =  'data:image/png;base64,'.base64_encode(file_get_contents($barcodePath.$ticket['barcodeFileName'].'.jpg'));
 
        if (!empty($ticket['WooCommerceEventsTicketLogoPath'])) {

            $ticket['WooCommerceEventsTicketLogo'] = 'data:image/png;base64,'.base64_encode(file_get_contents($ticket['WooCommerceEventsTicketLogoPath']));

        }

        if (!empty($ticket['WooCommerceEventsTicketHeaderImagePath'])) {
        
            $ticket['WooCommerceEventsTicketHeaderImage'] = 'data:image/png;base64,'.base64_encode(file_get_contents($ticket['WooCommerceEventsTicketHeaderImagePath']));

        }

        if (file_exists(($themePacksPath.$themeName."/images/location.jpg"))) {

            $locationIcon = 'data:image/png;base64,'.base64_encode(file_get_contents($themePacksPath.$themeName."/images/location.jpg"));

        }

        if (file_exists(($themePacksPath.$themeName."/images/time.jpg"))) {

            $timeIcon = 'data:image/png;base64,'.base64_encode(file_get_contents($themePacksPath.$themeName."/images/time.jpg"));

        }
        
        if (file_exists(($themePacksPath.$themeName."/images/ticket.jpg"))) {

            $ticketIcon = 'data:image/png;base64,'.base64_encode(file_get_contents($themePacksPath.$themeName."/images/ticket.jpg"));

        }

        if (file_exists(($themePacksPath.$themeName."/images/cut1.jpg"))) {

            $cut1 = 'data:image/png;base64,'.base64_encode(file_get_contents($themePacksPath.$themeName."/images/cut1.jpg"));

        }
        
        if (file_exists(($themePacksPath.$themeName."/images/cut2.jpg"))) {

            $cut2 = 'data:image/png;base64,'.base64_encode(file_get_contents($themePacksPath.$themeName."/images/cut2.jpg"));

        }

        if (file_exists(($themePacksPath.$themeName."/images/divider.jpg"))) {

            $divider = 'data:image/png;base64,'.base64_encode(file_get_contents($themePacksPath.$themeName."/images/divider.jpg"));

        }

        $globalFooEventsPDFTicketsFont = get_option('globalFooEventsPDFTicketsFont');
        $font_family = $this->_get_font_family($globalFooEventsPDFTicketsFont);
        $font_face = $this->_get_font_face($globalFooEventsPDFTicketsFont);
        
        //Check theme directory for template first
        if(file_exists($this->Config->templatePathTheme.$template) ) {

             include($this->Config->templatePathTheme.$template);

        } else {

            include($template); 

        }

        return ob_get_clean();
        
    }
    
    /**
     * Includes the ticket template and parses PHP.
     * 
     * @param array $tickets
     * @param string $template_name
     */
    public function parse_multiple_ticket_template($tickets, $template, $eventPluginURL) {
        
        ob_start();
        
        $plugins_url = plugins_url();
        
        //Check theme directory for template first
        if(file_exists($this->Config->templatePathTheme.$template) ) {

             include($this->Config->templatePathTheme.$template);

        }else {

            include($this->Config->templatePath.$template); 

        }

        return ob_get_clean();
        
    }

    /**
     * Generates font family based on selected global font
     * 
     * @param string $globalFooEventsPDFTicketsFont
     * @return string 
     */
    private function _get_font_family($globalFooEventsPDFTicketsFont) {
        
        switch($globalFooEventsPDFTicketsFont) {
            
            case 'DejaVu Sans':
            return "'DejaVu Sans','Helvetica'";
            break;
        
            case 'Firefly Sung':
            return "'Firefly Sung', 'DejaVu Sans','Helvetica'";
            break;
        
            default:
            return "'DejaVu Sans','Helvetica'";
            
        }
        
    }
    
    /**
     * Generates font face based on selected global font
     * 
     * @param string $globalFooEventsPDFTicketsFont
     * @return string 
     */
    private function _get_font_face($globalFooEventsPDFTicketsFont) {
        
        switch($globalFooEventsPDFTicketsFont) {
            
            case 'DejaVu Sans':
            return "";
            break;
        
            case 'Firefly Sung':
            return "@font-face {
                    font-family: 'Firefly Sung';
                    font-style: normal;
                    font-weight: 400;
                    src: url(https://www.fooevents.com/fonts/fireflysung.ttf) format('truetype');
                  }";
            break;
        
            default:
            return "";
            
        }
        
    }
    
    /**
     * Generates font face based on selected global font
     * 
     * @param string $globalFooEventsPDFTicketsFont
     * @return string 
     */
    private function _get_font_face_bold($globalFooEventsPDFTicketsFont) {
        
        switch($globalFooEventsPDFTicketsFont) {
            
            case 'DejaVu Sans':
            return "";
            break;
        
            case 'Firefly Sung':
            return "@font-face {
                    font-family: 'Firefly Sung';
                    font-style: bold;
                    font-weight: bold;
                    src: url(https://www.fooevents.com/fonts/fireflysung.ttf) format('truetype');
                  }";
            break;
        
            default:
            return "";
            
        }
        
    }
    
}
