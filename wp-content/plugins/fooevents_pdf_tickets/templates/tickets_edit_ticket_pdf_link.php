<div class="clear"></div>
<div class="ticket-details-row">
	<a href="<?php echo esc_attr($urlPath); ?>" target="_BLANK" class="button"><?php _e('Download PDF Ticket', 'fooevents-pdf-tickets'); ?></a>
</div>