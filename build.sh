#!/usr/bin/env bash
set -euo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR


DB_VERSION=48748
CORE_BUNDLES=(
    "core"
    "settings_discussion"
    "settings_general"
    "settings_media"
    "settings_permalinks"
    "settings_privacy"
    "settings_reading"
    "settings_writing"
    "theme"
    "widgets"
)
PLUGIN_BUNDLES=(
    "plugin_acf_pro"
    "plugin_campaign_url_builder"
    "plugin_disable_comments"
    "plugin_duplicate_post"
    #"plugin_event_espresso"
    "plugin_mc4wp"
    "plugin_wpcf7"
)
ACTIVATE_PLUGINS=(
    "advanced-custom-fields-pro"
    "campaign-url-builder"
    "contact-form-7"
    "contact-form-7-success-page-redirects"
    "disable-comments"
    "event-espresso-core"
    "mailchimp-for-wp"
    "wp-native-php-sessions"
    "pantheon-advanced-page-cache"
    "wordpress-importer"
    "duplicate-post"
)



progname=$(basename $0)
SCRIPT_PATH=$(dirname "$(realpath -s "$BASH_SOURCE")")

cd "$SCRIPT_PATH"

if ! command -v wp &> /dev/null
then
    printf "WP-CLI command not available"
fi


function clean() {
    local dump_path="private/build/empty-version-${DB_VERSION}.sql"
    if wp core is-installed;
    then
        printf "There might already be a build you're working on.\n\n"
        read -p "Possibly wipe an existing build? " -n 1 -r
        echo
        if [[ ! $REPLY =~ ^[Yy]$ ]]
        then
            echo
            exit 1
        fi
    fi

    rm -rf "wp-content/uploads/*"

    if wp db check &> /dev/null;
    then
        wp db drop --yes
    fi

    wp db create

    wp db import "${dump_path}"

    wp user create localadmin localadmin@local.test \
        --role=administrator \
        --user_pass="password"


}


# Template for sub command functions
#
# function name must start with sub underscore and then
# name of sub command as typed by the user in the form 
# sub_{command name}
function sub_example() {
    # option parser copied from:
    # https://medium.com/@Drew_Stokes/bash-argument-parsing-54f3b81a6a8f
    PARAMS=""
    while (( "$#" )); do
    case "$1" in
        -a|--my-boolean-flag)
        MY_FLAG=0
        shift
        ;;
        -b|--my-flag-with-argument)
        if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
            MY_FLAG_ARG=$2
            shift 2
        else
            echo "Error: Argument for $1 is missing" >&2
            exit 1
        fi
        ;;
        -*|--*=) # unsupported flags
        echo "Error: Unsupported flag $1" >&2
        exit 1
        ;;
        *) # preserve positional arguments
        PARAMS="$PARAMS $1"
        shift
        ;;
    esac
    done
    # set positional arguments in their proper place
    eval set -- "$PARAMS"
}


function sub_default() {
    echo "Usage: $progname <subcommand> [options]"
    echo "Subcommands:"
    echo "    local   Build a local dev environment"
    echo "    info    Get info about the current build"
    echo ""
    echo "For help with each subcommand run:"
    echo "$progname <subcommand> -h|--help"
    echo ""
}


function sub_local() {

    local config_file="wp-config-local.php"

    if [ ! -r "$SCRIPT_PATH/$config_file" ]
    then
        {
            echo "The local wp configuration file $config_file"
            echo "wasn't found in directory $SCRIPT_PATH"
        } >&2
        exit
    fi

    clean

    wp plugin activate wp-cfm

    for i in "${!CORE_BUNDLES[@]}";
    do
        echo "pulling bundle ${CORE_BUNDLES[$i]}"
        wp config pull "${CORE_BUNDLES[$i]}"
    done
    
    wp core update-db

    if [[ "$(wp option get db_version)" != "${DB_VERSION}" ]]
    then
        {
            echo "WP-CFM core settings are for db_version ${DB_VERSION}"
            echo "Settings file bundles for core may need an update"
            echo "List of Core bundles effected:"
            for i in "${CORE_BUNDLES[@]}"
            do 
                echo " -${CORE_BUNDLES[$i]}"
            done
        } >&2
        exit 100
    fi

    wp wp rewrite flush
    
    for i in "${!PLUGIN_BUNDLES[@]}"
    do
        echo "pulling bundle ${PLUGIN_BUNDLES[$i]}"
        wp config pull "${PLUGIN_BUNDLES[$i]}"
    done

    wp plugin activate "${ACTIVATE_PLUGINS[@]}"
}


subcommand=${1---help}
case $subcommand in
    "" | "-h" | "--help")
        sub_default
        ;;
    *)
        shift
        if [ "$(type -t sub_${subcommand})" != 'function' ]; then
            echo "Error: '$subcommand' is not a known subcommand." >&2
            echo "       Run  --help' for a list of known subcommands." >&2
            exit 1
        fi
        sub_${subcommand} $@
        ;;
esac



